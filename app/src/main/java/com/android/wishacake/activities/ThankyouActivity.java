package com.android.wishacake.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.wishacake.R;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.SharedPrefSingleton;

public class ThankyouActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextView mTextViewUserName, mTextViewMessage;
    private Button mButtonDone;

    private static final String LOG_TAG = ThankyouActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            setTheme(R.style.AppTheme_NoActionBar_White);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(ThankyouActivity.this, R.drawable.ic_close));

        String userName = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_FIRST_NAME)
                + " " + SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_LAST_NAME);
        mTextViewUserName.setText(userName);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String bakerName = extras.getString(Constants.EXTRA_KEY_BAKER_NAME);
            mTextViewMessage.setText("Your order request has been sent to " + bakerName + " and is awaiting approval. You will receive a notification from us once your order request will be accepted or rejected.");
        }

        mButtonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickBackOrDone();
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        onClickBackOrDone();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onClickBackOrDone();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTextViewMessage = (TextView) findViewById(R.id.tv_message);
        mTextViewUserName = (TextView) findViewById(R.id.tv_user_name);
        mButtonDone = (Button) findViewById(R.id.btn_done);
    }

    private void onClickBackOrDone() {
        ActivityCompat.finishAffinity(ThankyouActivity.this);
        startActivity(new Intent(ThankyouActivity.this, MainActivity.class));
    }
}

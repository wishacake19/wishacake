package com.android.wishacake.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.adapters.CakeImageAdapter;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.ErrorTextWatcher;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.Utils;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.weiwangcn.betterspinner.library.BetterSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheckoutActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private RecyclerView mRecyclerViewOrderImagesItems;
    private BetterSpinner mBetterSpinnerQuantity, mBetterSpinnerPounds, mBetterSpinnerDate, mBetterSpinnerTime;
    private MaterialEditText mMaterialEditTextDescription, mMaterialEditTextDescriptionJellyBean, mMaterialEditTextName,
            mMaterialEditTextEmail, mMaterialEditTextMobileNumber, mMaterialEditTextHomeAddress;
    private FrameLayout mFrameLayoutDescription;
    private TextView mTextViewMobileNumberHelper, mTextViewAddressHelper, mTextViewCash, mTextViewCard, mTextViewTermsOfService;
    private LinearLayout mLinearLayoutCash, mLinearLayoutCard;
    private ImageView mImageViewCard;
    private Button mButtonPlaceOrder;

    private CakeImageAdapter mCakeImageAdapter;
    private List<String> mImageUrls;
    private String[] mImagesBase64;
    private String mDescription, mMobileNumber, mLocation, mAddress, mUserCardNo, mSelectedPaymentMethod, mBakerId, mBakerName;
    private String[] mQuantityAndPoundsEntries, mDateEntries;
    private List<String> mTimeEntries;
    private int mQuantitySelectedItemIndex = 0, mPoundsSelectedItemIndex = 0, mDateSelectedItemIndex = 0,
            mTimeSelectedItemIndex = 0;
    private Date[] mDates = new Date[3];

    private ProgressDialog mProgressDialog;

    private static final int ADD_PAYMENT_METHOD_REQUEST_CODE = 97;
    private static final String LOG_TAG = CheckoutActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);

        mImagesBase64 = new String[Constants.ORDER_IMAGES_SIZE];

        mBakerId = getIntent().getExtras().getString(Constants.EXTRA_KEY_BAKER_ID);
        mBakerName = getIntent().getExtras().getString(Constants.EXTRA_KEY_BAKER_NAME);
        mImageUrls = getIntent().getExtras().getStringArrayList(Constants.EXTRA_KEY_ORDER_IMAGES);

        if (Globals.sUserDeliveryLocationMap == null) {
            mLocation = "";
        } else {
            mLocation = Globals.sUserDeliveryLocationMap;
        }

        mCakeImageAdapter = new CakeImageAdapter(this, mImageUrls);
        mRecyclerViewOrderImagesItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewOrderImagesItems.setAdapter(mCakeImageAdapter);

        mMaterialEditTextMobileNumber.addTextChangedListener(new ErrorTextWatcher(mTextViewMobileNumberHelper));
        mMaterialEditTextHomeAddress.addTextChangedListener(new ErrorTextWatcher(mTextViewAddressHelper));

        mMaterialEditTextMobileNumber.setText(Constants.MOBILE_NUMBER_PREFIX);
        Selection.setSelection(mMaterialEditTextMobileNumber.getText(), mMaterialEditTextMobileNumber.getText().length());

        mMaterialEditTextMobileNumber.addTextChangedListener(new TextWatcher() {
            String text;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                text = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().startsWith(Constants.MOBILE_NUMBER_PREFIX)) {
                    mMaterialEditTextMobileNumber.setText(text);
                    Selection.setSelection(mMaterialEditTextMobileNumber.getText(), mMaterialEditTextMobileNumber.getText().length());
                }
            }
        });

        loadContactInfoFromSharedPref();
        initSpinnerArrays();
        setUpSpinners();
        setUpDescriptionEditText();
        toggleCardLayoutView();


        mSelectedPaymentMethod = ApiConstants.PARAM_VALUE_CASH;

        mLinearLayoutCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedPaymentMethod = ApiConstants.PARAM_VALUE_CASH;
                mTextViewCard.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTextViewCash.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_selected_payment_method, 0);
            }
        });

        mLinearLayoutCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPrefSingleton.getInstance(CheckoutActivity.this).isCardAdded()) {
                    mSelectedPaymentMethod = ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD;
                    mTextViewCash.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    mTextViewCard.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_selected_payment_method, 0);
                } else {
                    Intent intent = new Intent();
                    intent.putExtra(Constants.EXTRA_KEY_IS_ADD_CARD_OPENED_FROM_CHECKOUT, true);
                    intent.setClassName(getPackageName(), AddCardActivity.class.getName());
                    startActivityForResult(intent, ADD_PAYMENT_METHOD_REQUEST_CODE);
                }
            }
        });

        mButtonPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isValid()) {
                    showConfirmationDialog();
                }
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == ADD_PAYMENT_METHOD_REQUEST_CODE && resultCode == RESULT_OK) {
            toggleCardLayoutView();
            boolean isCardAdded = data.getBooleanExtra(Constants.EXTRA_KEY_IS_ADD_CARD_ADDED, false);
            if (isCardAdded) {
                mSelectedPaymentMethod = ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD;
                mTextViewCash.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTextViewCard.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_selected_payment_method, 0);
            } else {
                mSelectedPaymentMethod = ApiConstants.PARAM_VALUE_CASH;
                mTextViewCard.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                mTextViewCash.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_selected_payment_method, 0);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRecyclerViewOrderImagesItems = (RecyclerView) findViewById(R.id.rv_order_images_items);
        mBetterSpinnerQuantity = (BetterSpinner) findViewById(R.id.better_spinner_quantity);
        mBetterSpinnerPounds = (BetterSpinner) findViewById(R.id.better_spinner_pounds);
        mBetterSpinnerDate = (BetterSpinner) findViewById(R.id.better_spinner_date);
        mBetterSpinnerTime = (BetterSpinner) findViewById(R.id.better_spinner_time);
        mMaterialEditTextDescriptionJellyBean = (MaterialEditText) findViewById(R.id.et_description_jellybean);
        mMaterialEditTextDescription = (MaterialEditText) findViewById(R.id.et_description);
        mMaterialEditTextName = (MaterialEditText) findViewById(R.id.et_name);
        mMaterialEditTextEmail = (MaterialEditText) findViewById(R.id.et_email);
        mMaterialEditTextMobileNumber = (MaterialEditText) findViewById(R.id.et_mobile_number);
        mMaterialEditTextHomeAddress = (MaterialEditText) findViewById(R.id.et_home_address);
        mFrameLayoutDescription = (FrameLayout) findViewById(R.id.frame_layout_description);
        mTextViewMobileNumberHelper = (TextView) findViewById(R.id.tv_mobile_number_helper);
        mTextViewAddressHelper = (TextView) findViewById(R.id.tv_home_address_helper);
        mLinearLayoutCash = (LinearLayout) findViewById(R.id.linear_layout_cash);
        mLinearLayoutCard = (LinearLayout) findViewById(R.id.linear_layout_card);
        mTextViewCash = (TextView) findViewById(R.id.tv_cash);
        mTextViewCard = (TextView) findViewById(R.id.tv_card);
        mImageViewCard = (ImageView) findViewById(R.id.img_card);
        mTextViewTermsOfService = (TextView) findViewById(R.id.tv_terms_of_service);
        mButtonPlaceOrder = (Button) findViewById(R.id.btn_place_order);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mFrameLayoutDescription.setVisibility(View.GONE);
            mMaterialEditTextDescriptionJellyBean.setVisibility(View.VISIBLE);
        }
        mTextViewTermsOfService.setText(Html.fromHtml("By tapping place order, you agree to " +
                getString(R.string.app_name) + "'s" +
                "<br><font color='#f06292'><a href='http://wishacake.com/terms-of-service'><u>Terms of service</u></font>.<br><br><font color='#212121'><strong>Note: </strong></font><font color='#9b9b9b'>You can cancel your order only within 1 hour from the time of order placed.</font>"));
        mTextViewTermsOfService.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void loadContactInfoFromSharedPref() {
        String firstName = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_FIRST_NAME);
        String lastName = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_LAST_NAME);
        String email = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_EMAIL);
        mMobileNumber = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_MOBILE_NUMBER);

        mMaterialEditTextName.setText(firstName + " " + lastName);
        mMaterialEditTextEmail.setText(email);
        if (!Utils.isStringEmptyOrNull(mMobileNumber)) {
            mMaterialEditTextMobileNumber.setText(mMobileNumber);
        }
    }

    private void initSpinnerArrays() {
        mQuantityAndPoundsEntries = getResources().getStringArray(R.array.array_quantity_and_pounds);
        mDateEntries = getNext3Days();
        initTimes();
    }

    private void setUpSpinners() {
        Typeface customFont = Typeface.createFromAsset(getAssets(), "fonts/montserrat_semibold.ttf");
        mBetterSpinnerQuantity.setTypeface(customFont);
        mBetterSpinnerPounds.setTypeface(customFont);
        mBetterSpinnerDate.setTypeface(customFont);
        mBetterSpinnerTime.setTypeface(customFont);

        mBetterSpinnerQuantity.setAdapter(new ArrayAdapter<String>(this, R.layout.custom_spinner_item, mQuantityAndPoundsEntries));
        mBetterSpinnerQuantity.setText(mQuantityAndPoundsEntries[mQuantitySelectedItemIndex]);
        mBetterSpinnerQuantity.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mQuantitySelectedItemIndex = position;
            }
        });

        mBetterSpinnerPounds.setAdapter(new ArrayAdapter<String>(this, R.layout.custom_spinner_item, mQuantityAndPoundsEntries));
        mBetterSpinnerPounds.setText(mQuantityAndPoundsEntries[mPoundsSelectedItemIndex]);
        mBetterSpinnerPounds.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mPoundsSelectedItemIndex = position;
            }
        });

        mBetterSpinnerDate.setAdapter(new ArrayAdapter<String>(this, R.layout.custom_spinner_item, mDateEntries));
        mBetterSpinnerDate.setText(mDateEntries[mDateSelectedItemIndex]);
        mBetterSpinnerDate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mDateSelectedItemIndex = position;
                initTimes();
            }
        });
    }

    private void toggleCardLayoutView() {
        if (SharedPrefSingleton.getInstance(this).isCardAdded()) {
            mTextViewCard.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));

            mUserCardNo = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_CARD_NUMBER);
            mTextViewCard.setText(Utils.getFormattedCardNumber(mUserCardNo));

            if (Utils.getCardType(mUserCardNo).equalsIgnoreCase(Constants.MASTER_CARD)) {
                mImageViewCard.setImageResource(R.drawable.ic_master_card);
            } else if (Utils.getCardType(mUserCardNo).equalsIgnoreCase(Constants.VISA)) {
                mImageViewCard.setImageResource(R.drawable.ic_visa);
            } else if (Utils.getCardType(mUserCardNo).equalsIgnoreCase(Constants.AMERICAN_EXPRESS)) {
                mImageViewCard.setImageResource(R.drawable.ic_american_express);
            } else {
                mImageViewCard.setImageResource(R.drawable.ic_card);
            }
        } else {
            mTextViewCard.setText("Add payment method");
            mTextViewCard.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            mImageViewCard.setImageResource(R.drawable.ic_add);
        }
    }

    private void setUpDescriptionEditText() {
        // For inside scrollbar of input field
        mMaterialEditTextDescription.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_description) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mMaterialEditTextDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    if (hasFocus) {
                        mFrameLayoutDescription.setBackground(ContextCompat.getDrawable(CheckoutActivity.this, R.drawable.outlined_input_border_focused));
                    } else {
                        mFrameLayoutDescription.setBackground(ContextCompat.getDrawable(CheckoutActivity.this, R.drawable.outlined_input_border_unfocused));
                    }
                }
            }
        });

        mMaterialEditTextDescriptionJellyBean.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_description_jellybean) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
    }

    private String[] getNext3Days() {
        String[] arr = new String[3];
        SimpleDateFormat fullFormat = new SimpleDateFormat("EEEE, dd MMM");
        SimpleDateFormat halfFormat = new SimpleDateFormat("dd MMM");
        for (int i = 0; i < arr.length; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            Date date = calendar.getTime();
            if (DateTimeUtils.isToday(date.getTime())) {
                arr[i] = "Today, " + halfFormat.format(date);
            } else if (DateTimeUtils.isTomorrow(date.getTime())) {
                arr[i] = "Tomorrow, " + halfFormat.format(date);
            } else {
                arr[i] = fullFormat.format(date);
            }
            mDates[i] = date;
        }
        return arr;
    }

    private void initTimes() {
        List<String> list = new ArrayList<>();
        if (mDateSelectedItemIndex == 0) {
            Calendar calendar = Calendar.getInstance();
            int currentHour = Integer.parseInt(new SimpleDateFormat("HH").format(calendar.getTime().getTime()));
            int currentMinute = Integer.parseInt(new SimpleDateFormat("mm").format(calendar.getTime().getTime()));
            int hour = 0;
            if (currentHour >= 9) {
//                if (currentHour >= 21 && currentMinute >= 30) {
//                    list.add("Not available");
//                    setUpTimeSpinner(list);
//                    return;
//                }
                hour += currentHour + 3;
            } else {
                hour = 12;
            }
            while (hour < 24) {
                for (int minute = 0; minute < 60; minute += 30) {
                    String time = String.format("%02d:%02d %s", ((hour == 0 || hour == 12) ? 12 : hour % 12), minute,
                            (hour >= 12 ? "PM" : "AM"));
                    list.add(time);
                }
                hour++;
            }
        } else {
            for (int hour = 12; hour < 24; hour++) {
                for (int minute = 0; minute < 60; minute += 30) {
                    String time = String.format("%02d:%02d %s", ((hour == 0 || hour == 12) ? 12 : hour % 12), minute,
                            (hour >= 12 ? "PM" : "AM"));
                    list.add(time);
                }
            }
        }
        list.add("12:00 AM");
        setUpTimeSpinner(list);
    }

    private void setUpTimeSpinner(List<String> list) {
        mTimeEntries = list;

        mBetterSpinnerTime.setAdapter(new ArrayAdapter<String>(this, R.layout.custom_spinner_item, mTimeEntries));
        mBetterSpinnerTime.setText(mTimeEntries.get(mTimeSelectedItemIndex));
        mBetterSpinnerTime.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mTimeSelectedItemIndex = position;
            }
        });
    }

    @SuppressLint("StringFormatInvalid")
    private boolean isValid() {
        boolean flag = false;

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mDescription = mMaterialEditTextDescriptionJellyBean.getText().toString().trim();
        } else {
            mDescription = mMaterialEditTextDescription.getText().toString().trim();
        }
        mMobileNumber = mMaterialEditTextMobileNumber.getText().toString().trim();
        mAddress = mMaterialEditTextHomeAddress.getText().toString().trim();

        if (mMobileNumber.isEmpty()) {
            Utils.showError(mMaterialEditTextMobileNumber, getString(R.string.error_required, "Mobile number"), mTextViewMobileNumberHelper);
        } else if (Utils.isMobileNumberValid(mMobileNumber) == false) {
            Utils.showError(mMaterialEditTextMobileNumber, getString(R.string.error_badly_formatted, "Mobile number"), mTextViewMobileNumberHelper);
        } else if (mAddress.isEmpty()) {
            Utils.showError(mMaterialEditTextHomeAddress, getString(R.string.error_required, mMaterialEditTextHomeAddress.getHint().toString()), mTextViewAddressHelper);
        } else {
            flag = true;
        }
        return flag;
    }

    private void prepareImagesAsBase64() {
        try {
            String[] temp = new String[Constants.ORDER_IMAGES_SIZE];
            for (int i = 0; i < mImageUrls.size(); i++) {
                temp[i] = mImageUrls.get(i);
            }
            if (mImageUrls.size() < Constants.ORDER_IMAGES_SIZE) {
                for (int i = mImageUrls.size(); i < temp.length; i++) {
                    temp[i] = "null";
                }
            }

            // Now URI's to Base64
            for (int i = 0; i < temp.length; i++) {
                if (!temp[i].equalsIgnoreCase("null")) {
                    Bitmap imageBitmap = null;
                    try {
                        imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.parse(temp[i]));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    mImagesBase64[i] = Utils.getBase64EncodedFromBitmap(imageBitmap, 50);
                } else {
                    mImagesBase64[i] = "null";
                }
            }
        } catch (Exception e) {
            if (e != null) {
                Log.d(LOG_TAG, e.getMessage());
            }
        }
    }

    private void onClickPlaceOrder() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }

        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.show();

        prepareImagesAsBase64();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                placeOrder();
                return null;
            }
        }.execute();
    }

    private void placeOrder() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_PLACE_ORDER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        Intent intent = new Intent(CheckoutActivity.this, ThankyouActivity.class);
                        intent.putExtra(Constants.EXTRA_KEY_BAKER_NAME, mBakerName);
                        startActivity(intent);
                    } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                        Toast.makeText(CheckoutActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(CheckoutActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_USER_ID, SharedPrefSingleton.getInstance(CheckoutActivity.this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID));
                params.put(ApiConstants.PARAM_BAKER_ID, mBakerId);
                params.put(ApiConstants.PARAM_IMAGE1, mImagesBase64[0]);
                params.put(ApiConstants.PARAM_IMAGE2, mImagesBase64[1]);
                params.put(ApiConstants.PARAM_IMAGE3, mImagesBase64[2]);
                params.put(ApiConstants.PARAM_IMAGE4, mImagesBase64[3]);
                params.put(ApiConstants.PARAM_IMAGE5, mImagesBase64[4]);
                params.put(ApiConstants.PARAM_IMAGE6, mImagesBase64[5]);
                params.put(ApiConstants.PARAM_IMAGE7, mImagesBase64[6]);
                params.put(ApiConstants.PARAM_IMAGE8, mImagesBase64[7]);
                params.put(ApiConstants.PARAM_IMAGE9, mImagesBase64[8]);
                params.put(ApiConstants.PARAM_IMAGE10, mImagesBase64[9]);
                params.put(ApiConstants.PARAM_QUANTITY, mQuantityAndPoundsEntries[mQuantitySelectedItemIndex]);
                params.put(ApiConstants.PARAM_POUNDS, mQuantityAndPoundsEntries[mPoundsSelectedItemIndex]);
                params.put(ApiConstants.PARAM_DESCRIPTION, mDescription);
                params.put(ApiConstants.PARAM_CONTACT_NUMBER, mMobileNumber);
                params.put(ApiConstants.PARAM_DELIVERY_LOCATION, mLocation);
                params.put(ApiConstants.PARAM_DELIVERY_ADDRESS, mAddress);
                params.put(ApiConstants.PARAM_DELIVERY_DATE, new SimpleDateFormat("yyyy-MM-dd").format(mDates[mDateSelectedItemIndex]));
                params.put(ApiConstants.PARAM_DELIVERY_TIME, DateTimeUtils.getFormattedDateTimeString("hh:mm a", "HH:mm:ss", mTimeEntries.get(mTimeSelectedItemIndex)));
                params.put(ApiConstants.PARAM_PAYMENT_METHOD, mSelectedPaymentMethod);
                if (mSelectedPaymentMethod.equals(ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD)) {
                    params.put(ApiConstants.PARAM_CARD_NUMBER, SharedPrefSingleton.getInstance(CheckoutActivity.this)
                            .getLoggedInUserData().get((Constants.SHARED_PREF_KEY_USER_CARD_NUMBER)));
                    params.put(ApiConstants.PARAM_EXPIRATION_DATE, SharedPrefSingleton.getInstance(CheckoutActivity.this)
                            .getLoggedInUserData().get((Constants.SHARED_PREF_KEY_USER_CARD_EXP_DATE)));
                    params.put(ApiConstants.PARAM_CVV, SharedPrefSingleton.getInstance(CheckoutActivity.this)
                            .getLoggedInUserData().get((Constants.SHARED_PREF_KEY_USER_CARD_SECURITY_CODE)));
                    params.put(ApiConstants.PARAM_CARDHOLDER_NAME, SharedPrefSingleton.getInstance(CheckoutActivity.this)
                            .getLoggedInUserData().get((Constants.SHARED_PREF_KEY_USER_CARDHOLDER_NAME)));
                    params.put(ApiConstants.PARAM_COUNTRY, SharedPrefSingleton.getInstance(CheckoutActivity.this)
                            .getLoggedInUserData().get((Constants.SHARED_PREF_KEY_USER_CARD_COUNTRY)));
                }
                if (mSelectedPaymentMethod.equals(ApiConstants.PARAM_VALUE_CASH)) {
                    params.put(ApiConstants.PARAM_CARD_NUMBER, "null");
                    params.put(ApiConstants.PARAM_EXPIRATION_DATE, "null");
                    params.put(ApiConstants.PARAM_CVV, "null");
                    params.put(ApiConstants.PARAM_CARDHOLDER_NAME, "null");
                    params.put(ApiConstants.PARAM_COUNTRY, "null");
                }
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void showConfirmationDialog() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.your_orders_btn_confirm), null);

        final TextView textViewTitle = (TextView) dialogView.findViewById(R.id.tv_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText("Confirmation");

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        textViewMsg.setText("Are you sure you want to place the order? Once you tap the confirm button below than you won't be allowed to edit your order information. However, you can cancel the order if you want to by navigating to Your orders.");

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNetworkAvailable(getApplicationContext())) {
                    Toast.makeText(CheckoutActivity.this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
                    // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
                    return;
                }
                alertDialog.dismiss();
                onClickPlaceOrder();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }
}
package com.android.wishacake.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.adapters.HelpAdapter;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.HelpItem;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.UriRealPathUtils;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.utilities.PermissionUtils;
import com.android.wishacake.utilities.RealPathUtils;
import com.android.wishacake.utilities.Utils;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HelpActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private LinearLayout mLinearLayoutHeaderViews;
    private MaterialEditText mMaterialEditTextDescription, mMaterialEditTextDescriptionJellyBean;
    private TextView mTextViewAttachAnImage;
    private Button mButtonSubmit;
    private List<HelpItem> mHelpItems;
    private HelpAdapter mHelpAdapter;
    private FrameLayout mFrameLayoutDescription, mFrameLayoutSubmit;

    private ProgressDialog mProgressDialog;

    private String mBakerId, mDescription, mImage1Base64, mImage2Base64, mImage3Base64;
    private boolean mIsHelpOpenForReporting = false;

    public static FrameLayout sFrameLayoutUploadImage;
    public static Button sButtonUploadAnImage;
    public static RecyclerView sRecyclerViewHelpItems;

    private static String sAppName;

    // For SDK >= 24
    private static String sCurrentPhotoPath;
    private static File sStorageDirectory;

    // For SDK < 24
    private Uri mPhotoUri;
    private static String sCurrentPhotoName;

    public static int sHelpImageCount = 0;
    private static final int REQUEST_CODE_GALLERY = 01;
    private static final int REQUEST_CODE_CAMERA = 02;
    private static final String LOG_TAG = HelpActivity.class.getSimpleName();

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        initViews();

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Submitting...");

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(HelpActivity.this, R.drawable.ic_arrow_back_white));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.EXTRA_KEY_IS_HELP_OPENED_FOR_REPORTING)) {
                if (extras.getBoolean(Constants.EXTRA_KEY_IS_HELP_OPENED_FOR_REPORTING)) {
                    mIsHelpOpenForReporting = true;
                    mBakerId = extras.getString(Constants.EXTRA_KEY_BAKER_ID);
                    getSupportActionBar().setTitle(getString(R.string.order_details_report_a_problem));
                } else {
                    String firstName = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_FIRST_NAME);
                    mMaterialEditTextDescription.setHint("Hi " + firstName + ", How can we help you?");
                    mMaterialEditTextDescriptionJellyBean.setHint("Hi " + firstName + ", How can we help you?");
                }
            } else {
                String firstName = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_FIRST_NAME);
                mMaterialEditTextDescription.setHint("Hi " + firstName + ", How can we help you?");
                mMaterialEditTextDescriptionJellyBean.setHint("Hi " + firstName + ", How can we help you?");
            }
        }

        mHelpItems = new ArrayList<>();
        mHelpAdapter = new HelpAdapter(HelpActivity.this, mHelpItems);
        sRecyclerViewHelpItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        sRecyclerViewHelpItems.setAdapter(mHelpAdapter);

        sAppName = getString(R.string.app_name);

        sStorageDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        // For inside scrollbar of input field
        mMaterialEditTextDescription.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_description) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mMaterialEditTextDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    if (hasFocus) {
                        mFrameLayoutDescription.setBackground(ContextCompat.getDrawable(HelpActivity.this,
                                R.drawable.outlined_input_border_focused));
                    } else {
                        mFrameLayoutDescription.setBackground(ContextCompat.getDrawable(HelpActivity.this,
                                R.drawable.outlined_input_border_unfocused));
                    }
                }
            }
        });

        mMaterialEditTextDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mMaterialEditTextDescription.getText().toString().trim().isEmpty()) {
                    mFrameLayoutSubmit.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                    mButtonSubmit.setEnabled(false);
                } else {
                    mFrameLayoutSubmit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    mButtonSubmit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mMaterialEditTextDescriptionJellyBean.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.et_description_jellybean) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        mMaterialEditTextDescriptionJellyBean.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mMaterialEditTextDescriptionJellyBean.getText().toString().trim().isEmpty()) {
                    mFrameLayoutSubmit.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                    mButtonSubmit.setEnabled(false);
                } else {
                    mFrameLayoutSubmit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    mButtonSubmit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        sButtonUploadAnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View dialogView = LayoutInflater.from(HelpActivity.this).inflate(R.layout.custom_alert_dialog, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(HelpActivity.this, R.style.AlertDialogTheme);
                builder.setView(dialogView);
                builder.setNeutralButton(getString(R.string.help_gallery_alert_dialog_button), null);
                builder.setPositiveButton(getString(R.string.help_camera_alert_dialog_button), null);

                final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
                textViewMsg.setText(getString(R.string.help_upload_image_dialog_msg));

                final AlertDialog alertDialog = builder.create();
                alertDialog.setCancelable(true);

                alertDialog.show();

                /*
                 *
                 * Following code must be called after show() method of alert dialog
                 */

                alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (PermissionUtils.checkGalleryPermission(HelpActivity.this)) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            alertDialog.dismiss();
                            startActivityForResult(Intent.createChooser(intent, "Select picture"), REQUEST_CODE_GALLERY);
                        }
                        alertDialog.dismiss();
                    }
                });

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (PermissionUtils.checkCameraPermission(HelpActivity.this)) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                // Create the File where the photo should go
                                File photoFile = null;
                                try {
                                    photoFile = createImageFileForSDK24AndAbove();
                                } catch (IOException ex) {
                                    // Error occurred while creating the File
                                    Log.e(LOG_TAG, "IOException || " + ex.getMessage());
                                }
                                // Continue only if the File was successfully created
                                if (photoFile != null) {
                                    Uri photoURI = FileProvider.getUriForFile(HelpActivity.this,
                                            getPackageName() + ".fileprovider",
                                            photoFile);
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                    alertDialog.dismiss();
                                    startActivityForResult(intent, REQUEST_CODE_CAMERA);
                                }
                            } else {
                                mPhotoUri = Uri.fromFile(createImageFileForSDKBelow24());
                                intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
                                startActivityForResult(intent, REQUEST_CODE_CAMERA);
                            }
                        }
                        alertDialog.dismiss();
                    }
                });

                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(HelpActivity.this, R.color.colorPrimary));
                alertDialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(ContextCompat.getColor(HelpActivity.this, R.color.colorPrimary));
            }
        });

        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsHelpOpenForReporting) {
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        mDescription = mMaterialEditTextDescriptionJellyBean.getText().toString().trim();
                    } else {
                        mDescription = mMaterialEditTextDescription.getText().toString().trim();
                    }
                    onClickSubmit();
                    return;
                }
                Toast.makeText(HelpActivity.this, "Not implemented yet.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        onClickBack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onClickBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtils.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean writeStoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeStoragePermission) {
                    openGallery();
                }
            }
        }
        if (requestCode == PermissionUtils.REQUEST_CODE_CAMERA_OR_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean cameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (cameraPermission) {
                    if (!PermissionUtils.checkGalleryPermission(HelpActivity.this)) {
                        boolean writeStoragePermission = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                        if (writeStoragePermission) {
                            openCamera();
                        }
                    }
                    openCamera();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {
            if (data != null) {
                Uri uri = data.getData();
                String imagePath = "";
                if (Build.VERSION.SDK_INT < 11) {
                    imagePath = RealPathUtils.getRealPathFromURI_BelowSDK11(this, uri);
                } else if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT <= 18) {
                    imagePath = RealPathUtils.getRealPathFromURI_SDK11to18(this, uri);
                } else {
                    imagePath = RealPathUtils.getRealPathFromURI_SDK19(this, uri);
                }
                String fileName;
                if (!imagePath.isEmpty()) {
                    fileName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
                } else {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    fileName = "IMG_" + timeStamp + ".jpg";
                }

                sRecyclerViewHelpItems.setVisibility(View.VISIBLE);
                mHelpItems.add(new HelpItem(uri, fileName));
                mHelpAdapter.notifyDataSetChanged();
                sHelpImageCount++;

                try {
                    Bitmap imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    assignBase64ToImage(imageBitmap);
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        }
        if (requestCode == REQUEST_CODE_CAMERA && resultCode == RESULT_OK) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (data != null) {
                    Bundle extras = data.getExtras();
                    if (extras != null) {
                        Bitmap imageBitmap = (Bitmap) extras.get("data");
                        Uri uri = Uri.parse(Utils.getPathFromBitmap(HelpActivity.this, imageBitmap));
                        String fileName = sCurrentPhotoPath.substring(sCurrentPhotoPath.lastIndexOf("/") + 1);

                        sRecyclerViewHelpItems.setVisibility(View.VISIBLE);
                        mHelpItems.add(new HelpItem(uri, fileName));
                        mHelpAdapter.notifyDataSetChanged();
                        sHelpImageCount++;

                        try {
                            assignBase64ToImage(imageBitmap);
                        } catch (Exception e) {
                            e.getMessage();
                        }

                    } else {
                        Toast.makeText(this, "An unknown error has occurred while saving the image. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
                sRecyclerViewHelpItems.setVisibility(View.VISIBLE);
                mHelpItems.add(new HelpItem(mPhotoUri, sCurrentPhotoName));
                mHelpAdapter.notifyDataSetChanged();
                sHelpImageCount++;

                try {
                    String imagePath = UriRealPathUtils.getUriRealPath(this, mPhotoUri);
                    Bitmap imageBitmap = null;
                    if (!imagePath.isEmpty()) {
                        imageBitmap = BitmapFactory.decodeFile(imagePath);
                    }
                    assignBase64ToImage(imageBitmap);
                } catch (Exception e) {
                    e.getMessage();
                }
            }
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mLinearLayoutHeaderViews = (LinearLayout) findViewById(R.id.linear_layout_header_views);
        mMaterialEditTextDescriptionJellyBean = (MaterialEditText) findViewById(R.id.et_description_jellybean);
        mMaterialEditTextDescription = (MaterialEditText) findViewById(R.id.et_description);
        mTextViewAttachAnImage = (TextView) findViewById(R.id.tv_attach_an_image);
        sFrameLayoutUploadImage = (FrameLayout) findViewById(R.id.frame_layout_upload_image);
        sButtonUploadAnImage = (Button) findViewById(R.id.btn_upload_an_image);
        sRecyclerViewHelpItems = (RecyclerView) findViewById(R.id.rv_help_items);
        mButtonSubmit = (Button) findViewById(R.id.btn_submit);
        mFrameLayoutDescription = (FrameLayout) findViewById(R.id.frame_layout_description);
        mFrameLayoutSubmit = (FrameLayout) findViewById(R.id.frame_layout_submit);

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mFrameLayoutDescription.setVisibility(View.GONE);
            mMaterialEditTextDescriptionJellyBean.setVisibility(View.VISIBLE);
        }
        mTextViewAttachAnImage.setText(Html.fromHtml("<font color='#212121'>Attach an image</font>"
                + " <font color='#9b9b9b'>(optional)</font>"));
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select picture"), REQUEST_CODE_GALLERY);
    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFileForSDK24AndAbove();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.e(LOG_TAG, "IOException || " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(HelpActivity.this,
                        getPackageName() + ".fileprovider",
                        photoFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
            }
        } else {
            mPhotoUri = Uri.fromFile(createImageFileForSDKBelow24());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoUri);
            startActivityForResult(intent, REQUEST_CODE_CAMERA);
        }
    }


    private void assignBase64ToImage(Bitmap imageBitmap) {
        if (sHelpImageCount == 1) {
            mImage1Base64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
        } else if (sHelpImageCount == 2) {
            mImage2Base64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
        } else if (sHelpImageCount == 3) {
            mImage3Base64 = Utils.getBase64EncodedFromBitmap(imageBitmap);
        }
    }

    private void onClickSubmit() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_REPORT_A_PROBLEM, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);
                            String responseMsg = rootJsonObject.getString(ApiConstants.KEY_RESPONSE);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                showSuccessDialog("Your complaint has been received. Our customer support team will look into the situation and take any " +
                                        "necessary action. Thank you for helping to improve the " + getString(R.string.app_name) + " community.");
                            } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                Toast.makeText(HelpActivity.this, responseMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(HelpActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_USER_ID, SharedPrefSingleton.getInstance(HelpActivity.this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID));
                        params.put(ApiConstants.PARAM_BAKER_ID, mBakerId);
                        params.put(ApiConstants.PARAM_DESCRIPTION, mDescription);
                        if (mImage1Base64 == null) {
                            params.put(ApiConstants.PARAM_IMAGE1, "null");
                        } else {
                            params.put(ApiConstants.PARAM_IMAGE1, mImage1Base64);
                        }
                        if (mImage2Base64 == null) {
                            params.put(ApiConstants.PARAM_IMAGE2, "null");
                        } else {
                            params.put(ApiConstants.PARAM_IMAGE2, mImage2Base64);
                        }
                        if (mImage3Base64 == null) {
                            params.put(ApiConstants.PARAM_IMAGE3, "null");
                        } else {
                            params.put(ApiConstants.PARAM_IMAGE3, mImage3Base64);
                        }
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(HelpActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }

    private void showSuccessDialog(String msg) {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setPositiveButton("OK", null);

        String firstName = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_FIRST_NAME);

        final TextView textViewTitle = (TextView) dialogView.findViewById(R.id.tv_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText("Thanks, " + firstName + "!");

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        textViewMsg.setText(msg);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(false);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                ActivityCompat.finishAffinity(HelpActivity.this);
                startActivity(new Intent(HelpActivity.this, MainActivity.class));
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    private static File createImageFileForSDK24AndAbove() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String fileName = "IMG_" + timeStamp;
        File imageFile = File.createTempFile(fileName, ".jpg", sStorageDirectory);
        sCurrentPhotoPath = imageFile.getAbsolutePath();
        return imageFile;
    }

    private static File createImageFileForSDKBelow24() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), sAppName);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        sCurrentPhotoName = "IMG_" + timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator + sCurrentPhotoName);
    }

    private void onClickBack() {
        if (!this.isAnyUnsavedChangesAvailable()) {
            finish();
        } else {
            showUnsavedChangesDialog();
        }
    }

    private boolean isAnyUnsavedChangesAvailable() {
        boolean check = false;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            if (!mMaterialEditTextDescriptionJellyBean.getText().toString().trim().isEmpty()
                    || mHelpAdapter.getItemCount() > 0) {
                check = true;
            }
        } else {
            if (!mMaterialEditTextDescription.getText().toString().trim().isEmpty()
                    || mHelpAdapter.getItemCount() > 0) {
                check = true;
            }
        }
        return check;
    }

    private void showUnsavedChangesDialog() {
        View dialogView = LayoutInflater.from(HelpActivity.this).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(HelpActivity.this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.discard_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.cancel_alert_dialog_button), null);

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setText(getString(R.string.shared_discard_your_changes));

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
    }
}

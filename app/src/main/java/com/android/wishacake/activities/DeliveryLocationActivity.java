package com.android.wishacake.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wishacake.R;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.utilities.PermissionUtils;
import com.android.wishacake.utilities.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.android.wishacake.utilities.Utils.getDoubleValueRoundedOff;

public class DeliveryLocationActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private Toolbar mToolbar;
    private FrameLayout mFrameLayoutFindBakers;
    private Button mButtonFindBakers;

    private EditText mEditTextPlaceAutocompleteFragment;

    private GoogleMap mMap;
    private MarkerOptions mMarkerOptions;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    private static double sCurrentLatitude;
    private static double sCurrentLongitude;
    private static String sCurrentPlaceName;

    private MenuItem mMenuItemLocateMe;

    private boolean mIsBakerOpenedFromNavMenu = false;

    private static final int REQUEST_OPEN_LOCATION_DIALOG = 100;
    private static final String LOG_TAG = DeliveryLocationActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_location);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.EXTRA_KEY_IS_BAKERS_OPENED_FROM_NAVIGATION_MENU)) {
                if (extras.getBoolean(Constants.EXTRA_KEY_IS_BAKERS_OPENED_FROM_NAVIGATION_MENU)) {
                    mIsBakerOpenedFromNavMenu = true;
                }
            }
        }

        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        PlaceAutocompleteFragment placeAutocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        mMarkerOptions = new MarkerOptions();
//        mMarkerOptions.draggable(true);

        setUpPlaceAutocompleteFragmentViews(placeAutocompleteFragment);

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder().setCountry("PK").build();
        placeAutocompleteFragment.setFilter(typeFilter);

        openAutocompleteWidgetInOverlayMode(placeAutocompleteFragment);

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(10 * 1000); // 10 seconds, in milliseconds
        mLocationRequest.setFastestInterval(1 * 1000); // 1 second, in milliseconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        mGoogleApiClient = new GoogleApiClient.Builder(DeliveryLocationActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mButtonFindBakers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEditTextPlaceAutocompleteFragment.getText().toString().trim().equals("")) {
                    Toast.makeText(DeliveryLocationActivity.this, "Delivery location is required.", Toast.LENGTH_SHORT).show();
                } else {
                    Globals.sIsDeliveryLocationObtainedFromUser = true;
                    Globals.sUserCurrentLocation.setLatitude(getDoubleValueRoundedOff(sCurrentLatitude));
                    Globals.sUserCurrentLocation.setLongitude(getDoubleValueRoundedOff(sCurrentLongitude));
                    Globals.sUserDeliveryLocationMap = mEditTextPlaceAutocompleteFragment.getText().toString().trim();
                    Intent intent = new Intent(DeliveryLocationActivity.this, BakersActivity.class);
                    intent.putExtra(Constants.EXTRA_KEY_IS_BAKERS_OPENED_FROM_NAVIGATION_MENU, mIsBakerOpenedFromNavMenu);
                    intent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, getIntent().getExtras().getStringArrayList(Constants.EXTRA_KEY_ORDER_IMAGES));
                    startActivity(intent);
                }
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        if (!mEditTextPlaceAutocompleteFragment.getText().toString().trim().equals("")) {
            Globals.sIsDeliveryLocationObtainedFromUser = true;
            Globals.sUserCurrentLocation.setLatitude(getDoubleValueRoundedOff(sCurrentLatitude));
            Globals.sUserCurrentLocation.setLongitude(getDoubleValueRoundedOff(sCurrentLongitude));
            Globals.sUserDeliveryLocationMap = mEditTextPlaceAutocompleteFragment.getText().toString().trim();
        } else {
            Globals.sIsDeliveryLocationObtainedFromUser = false;
            Globals.sUserDeliveryLocationMap = "";
        }
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_delivery_location, menu);
        mMenuItemLocateMe = menu.findItem(R.id.action_locate_me);
        if (PermissionUtils.checkLocationPermission(this)) {
            loadCurrentLocationInMap();
        } else {
            requestLocationPermission();
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_locate_me:
                if (!Utils.isNetworkAvailable(getApplicationContext())) {
                    Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
                    // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
                    return true;
                }
                loadCurrentLocationInMap();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
        super.onStart();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (!PermissionUtils.checkLocationPermission(this)) {
            requestLocationPermission();
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

        if (location != null) {
            updateLocationVariables(location);
        }
        LatLng latLng = new LatLng(sCurrentLatitude, sCurrentLongitude);

        mMap.clear();
        mMap.addMarker(mMarkerOptions.position(latLng).title(""));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mEditTextPlaceAutocompleteFragment.setText(getPlaceNameByLatLong(latLng.latitude, latLng.longitude));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f));
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG, connectionResult.toString());
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
//            @Override
//            public void onCameraIdle() {
//                LatLng latLng = mMap.getCameraPosition().target;
//                mMap.clear();
//                mMap.addMarker(mMarkerOptions.position(latLng).title(""));
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
//                mEditTextPlaceAutocompleteFragment.setText(getPlaceNameByLatLong(latLng.latitude, latLng.longitude));
////                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18.0f));
//                sCurrentLatitude = latLng.latitude;
//                sCurrentLongitude = latLng.longitude;
//            }
//        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();
                mMap.addMarker(mMarkerOptions.position(latLng).title(""));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mEditTextPlaceAutocompleteFragment.setText(getPlaceNameByLatLong(latLng.latitude, latLng.longitude));
//                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f));
                sCurrentLatitude = latLng.latitude;
                sCurrentLongitude = latLng.longitude;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtils.REQUEST_CODE_LOCATION) {
            if (grantResults.length > 0) {
                boolean locationPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (locationPermission) {
                    if (PermissionUtils.checkLocationPermission(this)) {
                        loadCurrentLocationInMap();
                    }
                    try {
                        LocationServices.FusedLocationApi.requestLocationUpdates(
                                mGoogleApiClient, mLocationRequest, this);
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        if (requestCode == REQUEST_OPEN_LOCATION_DIALOG) {
            if (resultCode == Activity.RESULT_OK) {
                // All required changes were successfully made
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // The user was asked to change settings, but chose not to
                if (!states.isGpsUsable()) {
                    // Degrade gracefully depending on what is available
                }
            }
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mFrameLayoutFindBakers = (FrameLayout) findViewById(R.id.frame_layout_find_bakers);
        mButtonFindBakers = (Button) findViewById(R.id.btn_find_bakers);
    }

    private void setUpPlaceAutocompleteFragmentViews(PlaceAutocompleteFragment placeAutocompleteFragment) {
        mEditTextPlaceAutocompleteFragment = (EditText) placeAutocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input);
        mEditTextPlaceAutocompleteFragment.setTextSize(16f);
        mEditTextPlaceAutocompleteFragment.setHintTextColor(Constants.DEFAULT_EDITTEXT_HINT_COLOR);
        mEditTextPlaceAutocompleteFragment.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));
        mEditTextPlaceAutocompleteFragment.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/montserrat_semibold.ttf"));

        ImageButton imageButtonSearch = (ImageButton) placeAutocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_button);
        imageButtonSearch.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_search_secondary));

        ImageButton imageButtonClear = (ImageButton) placeAutocompleteFragment.getView().findViewById(R.id.place_autocomplete_clear_button);
        imageButtonClear.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_clear_secondary));

        mEditTextPlaceAutocompleteFragment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mEditTextPlaceAutocompleteFragment.getText().toString().trim().isEmpty()) {
                    mFrameLayoutFindBakers.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                    mButtonFindBakers.setEnabled(false);
                } else {
                    mFrameLayoutFindBakers.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    mButtonFindBakers.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void openAutocompleteWidgetInOverlayMode(PlaceAutocompleteFragment placeAutocompleteFragment) {
        placeAutocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                onPlaceItemSelected(place);
            }

            @Override
            public void onError(Status status) {
            }
        });
    }

    private void onPlaceItemSelected(Place place) {
        mMap.clear();
        mMap.addMarker(mMarkerOptions.position(place.getLatLng()).title(place.getName().toString()));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 18.0f));

        sCurrentLatitude = place.getLatLng().latitude;
        sCurrentLongitude = place.getLatLng().longitude;
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private String getPlaceNameByLatLong(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        String addressStr = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                Address address = addresses.get(0);
                String str = address.getAddressLine(0);
                addressStr = str;
            }
        } catch (IOException e) {
            if (e != null) {
                Log.e(LOG_TAG, e.getLocalizedMessage());
            }
        }
        return addressStr;
    }

    private void requestLocationPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION}, PermissionUtils.REQUEST_CODE_LOCATION);
    }

    private void updateLocationVariables(Location location) {
        sCurrentLatitude = location.getLatitude();
        sCurrentLongitude = location.getLongitude();
        sCurrentPlaceName = getPlaceNameByLatLong(sCurrentLatitude, sCurrentLatitude);

        Globals.sUserCurrentLocation.setLatitude(sCurrentLatitude);
        Globals.sUserCurrentLocation.setLongitude(sCurrentLongitude);

        Log.v("Location", "Lat: " + sCurrentLatitude + "\tLong: " + sCurrentLongitude
                + "\tName: " + sCurrentPlaceName);
    }

    private void loadCurrentLocationInMap() {
        if (PermissionUtils.checkLocationPermission(this)) {
            try {
                if (isLocationEnabled()) {
                    if (mGoogleApiClient.isConnected()) {
                        LocationServices.FusedLocationApi
                                .requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
                                    @Override
                                    public void onLocationChanged(Location location) {
                                        if (location != null) {
                                            updateLocationVariables(location);
                                        }
                                    }
                                });
                        LatLng latLng = new LatLng(sCurrentLatitude, sCurrentLongitude);

                        mMap.clear();
                        mMap.addMarker(mMarkerOptions.position(latLng).title(""));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                        mEditTextPlaceAutocompleteFragment.setText(getPlaceNameByLatLong(latLng.latitude, latLng.longitude));
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 18.0f));
                    } else {
                        mGoogleApiClient.connect();
                    }
                } else {
                    enableLocation();
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        } else {
            requestLocationPermission();
        }
    }

    // Old approach
    private void showOpenLocationSettingsDialog() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.open_settings_alert_dialog_button), null);

        final TextView textViewTitle = (TextView) dialogView.findViewById(R.id.tv_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText("Location is off");

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        textViewMsg.setText("Turn it on to help us find your current location.");

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    // New approach
    private void enableLocation() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        builder.setAlwaysShow(false);

        final Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());

        result.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                loadCurrentLocationInMap();
            }
        });

        result.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    // Location settings are not satisfied, but this can be fixed
                    // by showing the user a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        // and check the result in onActivityResult().
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(DeliveryLocationActivity.this,
                                REQUEST_OPEN_LOCATION_DIALOG);
                    } catch (IntentSender.SendIntentException sendEx) {
                        // Ignore the error.
                    }
                }
            }
        });

//        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
//            @Override
//            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
//                try {
//                    LocationSettingsResponse response = task.getResult(ApiException.class);
//                    // loadCurrentLocationInMap();
//                } catch (ApiException exception) {
//                    switch (exception.getStatusCode()) {
//                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                            // Location settings are not satisfied. But could be fixed by showing the
//                            // user a dialog.
//                            try {
//                                // Cast to a resolvable exception.
//                                ResolvableApiException resolvable = (ResolvableApiException) exception;
//                                // Show the dialog by calling startResolutionForResult(),
//                                // and check the result in onActivityResult().
//                                resolvable.startResolutionForResult(DeliveryLocationActivity.this, REQUEST_OPEN_LOCATION_DIALOG);
//                            } catch (IntentSender.SendIntentException e) {
//                                // Ignore the error.
//                            } catch (ClassCastException e) {
//                                // Ignore, should be an impossible error.
//                            }
//                            break;
//                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
//                            // Location settings are not satisfied. However, we have no way to fix the
//                            // settings so we won't show the dialog.
//                            break;
//                    }
//                }
//            }
//        });
    }
}

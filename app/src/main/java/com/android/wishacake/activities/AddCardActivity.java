package com.android.wishacake.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wishacake.R;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.ErrorTextWatcher;
import com.android.wishacake.helpers.ExpirationDateValidator;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.utilities.Utils;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.listeners.OnCountryPickerListener;
import com.rengwuxian.materialedittext.MaterialEditText;

public class AddCardActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private MaterialEditText mMaterialEditTextCardNumber, mMaterialEditTextExpDate, mMaterialEditTextSecurityCode,
            mMaterialEditTextCardholderName, mMaterialEditTextCountry;
    private TextView mTextViewCardNumberHelper, mTextViewExpDateHelper, mTextViewSecurityCodeHelper, mTextViewCardholderNameHelper,
            mTextViewCountryHelper;
    private Button mButtonSave;

    private ProgressDialog mProgressDialog;

    private String mCardNumber, mExpDate, mSecurityCode, mCardholderName, mCountry;
    private boolean mIsAddCardOpenedFromCheckout = false;

    private static final String LOG_TAG = AddCardActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.EXTRA_KEY_IS_ADD_CARD_OPENED_FROM_CHECKOUT)) {
                if (extras.getBoolean(Constants.EXTRA_KEY_IS_ADD_CARD_OPENED_FROM_CHECKOUT)) {
                    mIsAddCardOpenedFromCheckout = true;
                }
            }
        }

        mMaterialEditTextSecurityCode.addTextChangedListener(new ErrorTextWatcher(mTextViewSecurityCodeHelper));
        mMaterialEditTextCardholderName.addTextChangedListener(new ErrorTextWatcher(mTextViewCardholderNameHelper));
        mMaterialEditTextCountry.addTextChangedListener(new ErrorTextWatcher(mTextViewCountryHelper));

        mMaterialEditTextCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @SuppressLint("StringFormatInvalid")
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Hide error state
                Utils.hideError(mTextViewCardNumberHelper);

                String cardNumber = charSequence.toString();
                if (Utils.getCardType(cardNumber).equalsIgnoreCase(Constants.MASTER_CARD)) {
                    toggleCardNumberMaxLengthAndIconWithCvvCidMaxLength(R.drawable.ic_master_card, 16, "CVV", 3);
                } else if (Utils.getCardType(cardNumber).equalsIgnoreCase(Constants.VISA)) {
                    toggleCardNumberMaxLengthAndIconWithCvvCidMaxLength(R.drawable.ic_visa, 16, "CVV", 3);
                }
//                else if (Utils.getCardType(cardNumber).equalsIgnoreCase(Constants.AMERICAN_EXPRESS)) {
//                    toggleCardNumberMaxLengthAndIconWithCvvCidMaxLength(R.drawable.ic_american_express, 15, "CID", 4);
//                }
                else {
                    // Default state
                    toggleCardNumberMaxLengthAndIconWithCvvCidMaxLength(R.drawable.ic_card, 16, "CVV", 3);
                }

//                mSecurityCode = mMaterialEditTextSecurityCode.getText().toString().trim();
//
//                // Hide CVV CID error if input is true according to the card type
//                if ((Utils.getCardType(cardNumber).equalsIgnoreCase(Constants.MASTER_CARD) && mSecurityCode.length() == 3)
//                        || (Utils.getCardType(cardNumber).equalsIgnoreCase(Constants.VISA) && mSecurityCode.length() == 3)
//                        || (Utils.getCardType(cardNumber).equalsIgnoreCase(Constants.AMERICAN_EXPRESS) && mSecurityCode.length() == 4)) {
//                    Utils.hideError(mTextViewSecurityCodeHelper);
//                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mMaterialEditTextExpDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // Hide error state
                mTextViewExpDateHelper.setText("MM/YY");
                mTextViewExpDateHelper.setTextColor(ContextCompat.getColor(AddCardActivity.this, R.color.colorSecondaryText));
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0 && (editable.length() % 3) == 0) {
                    final char c = editable.charAt(editable.length() - 1);
                    if ('/' == c) {
                        editable.delete(editable.length() - 1, editable.length());
                    }
                }
                if (editable.length() > 0 && (editable.length() % 3) == 0) {
                    char c = editable.charAt(editable.length() - 1);
                    if (Character.isDigit(c) && TextUtils.split(editable.toString(), String.valueOf("/")).length <= 2) {
                        editable.insert(editable.length() - 1, String.valueOf("/"));
                    }
                }
            }
        });

        CountryPicker.Builder builder = new CountryPicker.Builder()
                .with(this)
                .canSearch(true)
                .sortBy(CountryPicker.SORT_BY_NAME)
                .style(R.style.CountryPickerStyle)
                .theme(CountryPicker.THEME_OLD)
                .listener(new OnCountryPickerListener() {
                    @Override
                    public void onSelectCountry(Country country) {
                        mMaterialEditTextCountry.setText(country.getName());
                        mMaterialEditTextCountry.setCompoundDrawablesWithIntrinsicBounds(getScaledDrawable(country.getFlag()), null, null, null);
                    }
                });

        final CountryPicker countryPicker = builder.build();
        try {
            mMaterialEditTextCountry.setText(countryPicker.getCountryFromSIM().getName());
            mMaterialEditTextCountry.setCompoundDrawablesWithIntrinsicBounds(getScaledDrawable(countryPicker.getCountryFromSIM().getFlag()), null, null, null);
        } catch (Exception e) {
            if (e != null) {
                Log.e(LOG_TAG, e.getMessage());
                mMaterialEditTextCountry.setText("");
                mMaterialEditTextCountry.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            }
        }
        mMaterialEditTextCountry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countryPicker.showDialog(AddCardActivity.this);
            }
        });

        mButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSave();
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        onClickBack();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_save, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onClickBack();
                return true;
//            case R.id.action_save:
//                onClickSave();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mMaterialEditTextCardNumber = (MaterialEditText) findViewById(R.id.et_card_number);
        mMaterialEditTextExpDate = (MaterialEditText) findViewById(R.id.et_exp_date);
        mMaterialEditTextSecurityCode = (MaterialEditText) findViewById(R.id.et_security_code);
        mMaterialEditTextCardholderName = (MaterialEditText) findViewById(R.id.et_cardholder_name);
        mMaterialEditTextCountry = (MaterialEditText) findViewById(R.id.et_country);
        mTextViewCardNumberHelper = (TextView) findViewById(R.id.tv_card_number_helper);
        mTextViewExpDateHelper = (TextView) findViewById(R.id.tv_exp_date_helper);
        mTextViewSecurityCodeHelper = (TextView) findViewById(R.id.tv_security_code_helper);
        mTextViewCardholderNameHelper = (TextView) findViewById(R.id.tv_cardholder_name_helper);
        mTextViewCountryHelper = (TextView) findViewById(R.id.tv_country_helper);
        mButtonSave = (Button) findViewById(R.id.btn_save);
    }

    private void toggleCardNumberMaxLengthAndIconWithCvvCidMaxLength(int resIdDrawableCard, int maxLengthCardNumber, String securityCodeHint, int maxLengthSecurityCode) {
        mMaterialEditTextCardNumber.setCompoundDrawablesWithIntrinsicBounds(resIdDrawableCard, 0, 0, 0);
        mMaterialEditTextCardNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthCardNumber)});
        mMaterialEditTextSecurityCode.setHint(securityCodeHint);
        mMaterialEditTextSecurityCode.setFilters(new InputFilter[]{new InputFilter.LengthFilter(maxLengthSecurityCode)});
    }

    private Drawable getScaledDrawable(int drawableResId) {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawableResId);
        return new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 48, 48, true));
    }

    private boolean isExpDateFormatValid(String expDate) {
        boolean check = false;
        String regexExp = "^[0-9+]{2}/[0-9+]{2}$";
        if (expDate.matches(regexExp)) {
            check = true;
        }
        return check;
    }

    private boolean isCountryValid(String country) {
        boolean check = false;
        String regexExp = "^[A-Za-z]{1,}$";
        if (country.matches(regexExp)) {
            check = true;
        }
        return check;
    }

    @SuppressLint("StringFormatInvalid")
    private boolean isValid() {
        boolean flag = false;

        mCardNumber = mMaterialEditTextCardNumber.getText().toString().trim();
        mExpDate = mMaterialEditTextExpDate.getText().toString().trim();
        mSecurityCode = mMaterialEditTextSecurityCode.getText().toString().trim();
        mCardholderName = mMaterialEditTextCardholderName.getText().toString().trim();
        mCountry = mMaterialEditTextCountry.getText().toString().trim();

        if (mCardNumber.isEmpty()) {
            Utils.showError(mMaterialEditTextCardNumber, getString(R.string.error_required, mMaterialEditTextCardNumber.getHint().toString()), mTextViewCardNumberHelper);
        } else if (Utils.getCardType(mCardNumber).equals("")
                || (Utils.getCardType(mCardNumber).equalsIgnoreCase(Constants.MASTER_CARD) && mCardNumber.length() != 16)
                || (Utils.getCardType(mCardNumber).equalsIgnoreCase(Constants.VISA) && mCardNumber.length() != 16)
//                || (Utils.getCardType(mCardNumber).equalsIgnoreCase(Constants.VISA) && (mCardNumber.length() != 13 || mCardNumber.length() != 16))
//                || (Utils.getCardType(mCardNumber).equalsIgnoreCase(Constants.AMERICAN_EXPRESS) && mCardNumber.length() != 15)
                ) {
            Utils.showError(mMaterialEditTextCardNumber, getString(R.string.error_invalid_card_number), mTextViewCardNumberHelper);
        } else if (mExpDate.isEmpty()) {
            mTextViewExpDateHelper.setText(getString(R.string.error_required, mMaterialEditTextExpDate.getHint().toString()));
            mTextViewExpDateHelper.setTextColor(ContextCompat.getColor(AddCardActivity.this, R.color.colorRed));
            mMaterialEditTextExpDate.requestFocus();
        } else if (isExpDateFormatValid(mExpDate) == false) {
            mTextViewExpDateHelper.setText(getString(R.string.error_badly_formatted, mMaterialEditTextExpDate.getHint().toString()));
            mTextViewExpDateHelper.setTextColor(ContextCompat.getColor(AddCardActivity.this, R.color.colorRed));
            mMaterialEditTextExpDate.requestFocus();
        } else if (new ExpirationDateValidator().isValid(mExpDate.substring(0, 2), mExpDate.substring(mExpDate.length() - 2)) == false) {
            mTextViewExpDateHelper.setText(mMaterialEditTextExpDate.getHint().toString() + " is invalid.");
            mTextViewExpDateHelper.setTextColor(ContextCompat.getColor(AddCardActivity.this, R.color.colorRed));
            mMaterialEditTextExpDate.requestFocus();
        } else if (mSecurityCode.isEmpty()) {
            Utils.showError(mMaterialEditTextSecurityCode, getString(R.string.error_required, mMaterialEditTextSecurityCode.getHint().toString()), mTextViewSecurityCodeHelper);
        } else if ((Utils.getCardType(mCardNumber).equalsIgnoreCase(Constants.MASTER_CARD) && mSecurityCode.length() != 3)
                || (Utils.getCardType(mCardNumber).equalsIgnoreCase(Constants.VISA) && mSecurityCode.length() != 3)
//                || (Utils.getCardType(mCardNumber).equalsIgnoreCase(Constants.AMERICAN_EXPRESS) && mSecurityCode.length() != 4)
                ) {
            Utils.showError(mMaterialEditTextSecurityCode, getString(R.string.error_badly_formatted, mMaterialEditTextSecurityCode.getHint().toString()), mTextViewSecurityCodeHelper);
        } else if (mCardholderName.isEmpty()) {
            Utils.showError(mMaterialEditTextCardholderName, getString(R.string.error_required, mMaterialEditTextCardholderName.getHint().toString()), mTextViewCardholderNameHelper);
        } else if (Utils.isNameValid(mCardholderName) == false) {
            Utils.showError(mMaterialEditTextCardholderName, getString(R.string.error_badly_formatted, mMaterialEditTextCardholderName.getHint().toString()), mTextViewCardholderNameHelper);
        } else if (mCountry.isEmpty()) {
            Utils.showError(mMaterialEditTextCountry, getString(R.string.error_required, mMaterialEditTextCountry.getHint().toString()), mTextViewCountryHelper);
        } else if (isCountryValid(mCountry) == false) {
            Utils.showError(mMaterialEditTextCountry, getString(R.string.error_badly_formatted, mMaterialEditTextCountry.getHint().toString()), mTextViewCountryHelper);
        } else {
            flag = true;
        }
        return flag;
    }

    private void onClickSave() {
        if (isValid()) {
            SharedPrefSingleton.getInstance(this).saveCardData(mCardNumber, mExpDate, mSecurityCode, mCardholderName, mCountry);
            Toast.makeText(AddCardActivity.this, "Your card has been added successfully.", Toast.LENGTH_SHORT).show();
            if (mIsAddCardOpenedFromCheckout) {
                Intent intent = new Intent();
                intent.putExtra(Constants.EXTRA_KEY_IS_ADD_CARD_ADDED, true);
                setResult(RESULT_OK, intent);
            }
            finish();
        }
    }

    private void onClickBack() {
        if (!this.isAnyUnsavedChangesAvailable()) {
            finish();
        } else {
            showUnsavedChangesDialog();
        }
    }

    private boolean isAnyUnsavedChangesAvailable() {
        boolean check = false;
        if (!mMaterialEditTextCardNumber.getText().toString().trim().isEmpty() ||
                !mMaterialEditTextExpDate.getText().toString().trim().isEmpty() ||
                !mMaterialEditTextSecurityCode.getText().toString().trim().isEmpty() ||
                !mMaterialEditTextCardholderName.getText().toString().trim().isEmpty()) {
            check = true;
        }
        return check;
    }

    private void showUnsavedChangesDialog() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.discard_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.cancel_alert_dialog_button), null);

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setText(getString(R.string.shared_discard_your_changes));

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
    }
}

package com.android.wishacake.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.adapters.YourOrdersAdapter;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.models.Baker;
import com.android.wishacake.models.Order;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YourOrdersActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private RelativeLayout mRelativeLayoutSearch;
    private EditText mEditTextSearch;
    private ImageView mImageViewClear;
    private View mViewDividerSearch;

    private List<Order> mOrders;

    private YourOrdersAdapter mYourOrdersAdapter, mYourOrdersFilteredAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerViewYourOrdersItems;
    private RelativeLayout mRelativeLayoutEmptyView;
    private ProgressBar mProgressBar;
    private ImageView mImageViewEmptyView;
    private TextView mTextViewEmptyView, mTextViewEmptyViewTextButton;

    private MenuItem mSearchMenuItem;

    private boolean mIsSearchCollapsed = true;

    private static final String LOG_TAG = YourOrdersActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_orders);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(YourOrdersActivity.this, R.drawable.ic_arrow_back_white));

        mViewDividerSearch.setVisibility(View.GONE);

        mIsSearchCollapsed = true;
        setUpSearch();

        mOrders = new ArrayList<>();
        mYourOrdersAdapter = new YourOrdersAdapter(this, this, mOrders);
        mYourOrdersAdapter.setItemClickListener(new YourOrdersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Order order = null;
                if (mYourOrdersFilteredAdapter != null) {
                    order = mYourOrdersFilteredAdapter.getItem(position);
                } else {
                    order = mYourOrdersAdapter.getItem(position);
                }
                Intent orderIntent = new Intent(YourOrdersActivity.this, OrderDetailsActivity.class);
                orderIntent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, (ArrayList<String>) order.getImageUrls());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DATE, order.getDate());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, order.getStatus());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_ID, order.getId());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_QUANTITY, order.getQuantity());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_POUNDS, order.getPounds());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DESCRIPTION, order.getDescription());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_CONTACT_NUMBER, order.getContactNumber());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_LOCATION, order.getDeliveryLocation());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_ADDRESS, order.getDeliveryAddress());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_DATE, order.getDeliveryDate());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_TIME, order.getDeliveryTime());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_SUBTOTAL, order.getSubTotal());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_CHARGES, order.getDeliveryCharges());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_AMOUNT, order.getTotalAmount());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_PAYMENT_METHOD, order.getPaymentMethod());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_REJECTED_REASON, order.getRejectedReason());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_ORDER_RATING, order.getOrderRating());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_RATED_STATUS, order.getRatedStatus());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_ID, order.getBaker().getId());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_IMAGE, order.getBaker().getImageUrl());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_NAME, order.getBaker().getFirstName() + " " + order.getBaker().getLastName());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_ADDRESS, order.getBaker().getAddress());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_RATING, order.getBaker().getRating());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_MOBILE_NUMBER, order.getBaker().getMobileNumber());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_EMAIL, order.getBaker().getEmail());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_DISTANCE, order.getBaker().getDistance());
                orderIntent.putStringArrayListExtra(Constants.EXTRA_KEY_BAKER_SLIDER_IMAGES, (ArrayList<String>) order.getBaker().getSliderImageUrls());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_ACTIVE_STATUS, order.getBaker().getActiveStatus());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_FAVORITE_ID, order.getBaker().getFavoriteId());
                orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_IS_FAVORITE, order.getBaker().isFavorite());
                orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DATA_LOAD_FROM_SERVER, false);
                startActivity(orderIntent);
            }
        });
        mRecyclerViewYourOrdersItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerViewYourOrdersItems.setAdapter(mYourOrdersAdapter);

        executeAsyncTaskOrders();

        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorPrimary));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeAsyncTaskOrders();
            }
        });

        mTextViewEmptyViewTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTextViewEmptyViewTextButton.getText().toString().equalsIgnoreCase(getString(R.string.shared_try_again))) {
                    mTextViewEmptyViewTextButton.setVisibility(View.GONE);
                    executeAsyncTaskOrders();
                } else {
                    finish();
                }
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_your_orders, menu);
        mSearchMenuItem = menu.findItem(R.id.action_search);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_search:
                onClickSearch();
                return true;
            case R.id.action_status_info:
                startActivity(new Intent(YourOrdersActivity.this, StatusInformationActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRelativeLayoutSearch = (RelativeLayout) findViewById(R.id.relative_layout_search);
        mEditTextSearch = (EditText) findViewById(R.id.et_search);
        mImageViewClear = (ImageView) findViewById(R.id.img_clear);
        mViewDividerSearch = (View) findViewById(R.id.view_divider_search);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerViewYourOrdersItems = (RecyclerView) findViewById(R.id.rv_your_orders_items);
        mRelativeLayoutEmptyView = (RelativeLayout) findViewById(R.id.relative_layout_empty_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mImageViewEmptyView = (ImageView) findViewById(R.id.img_empty_view);
        mTextViewEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        mTextViewEmptyViewTextButton = (TextView) findViewById(R.id.tv_empty_view_text_button);
    }

    private void onClickSearch() {
        String searchStr = mEditTextSearch.getText().toString().trim();
        if (mIsSearchCollapsed) {
            hideEmptyView();
            showEmptyView(View.GONE, View.GONE, View.GONE, "", getString(R.string.your_orders_get_started));
            if (!Utils.isStringEmptyOrNull(searchStr)) {
                mEditTextSearch.setText("");
            }
            // Expand
            mIsSearchCollapsed = false;
            Utils.expand(mRelativeLayoutSearch, 4);
            mViewDividerSearch.setVisibility(View.VISIBLE);
            mEditTextSearch.requestFocus();
        } else {
            // Collapse
            mIsSearchCollapsed = true;
            Utils.collapse(mRelativeLayoutSearch, 4);
            mViewDividerSearch.setVisibility(View.GONE);
            if (!Utils.isStringEmptyOrNull(searchStr)) {
                mEditTextSearch.setText("");
            }
        }
    }

    private void setUpSearch() {
        mViewDividerSearch.setVisibility(View.GONE);

        Utils.updateSearchRightMargin(YourOrdersActivity.this, mEditTextSearch, 4);
        mEditTextSearch.setHint("Search by date, baker or status");
        mEditTextSearch.setHintTextColor(Constants.DEFAULT_EDITTEXT_HINT_COLOR);

        mEditTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String searchStr = mEditTextSearch.getText().toString().trim();
                if (searchStr.equals("")) {
                    mImageViewClear.setVisibility(View.GONE);
                    Utils.updateSearchRightMargin(YourOrdersActivity.this, mEditTextSearch, 4);
                    executeAsyncTaskOrders();
                } else {
                    Utils.updateSearchRightMargin(YourOrdersActivity.this, mEditTextSearch, 40);
                    mImageViewClear.setVisibility(View.VISIBLE);
                    // Filter orders
                    filterOrders(searchStr);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mImageViewClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditTextSearch.setText("");
            }
        });
    }

    private void showHideMenuItem(MenuItem menuItem) {
        if (mOrders.size() > 0 || mYourOrdersAdapter.getItemCount() > 0) {
            Utils.showMenuItem(menuItem);
        } else {
            Utils.hideMenuItem(menuItem);
        }
    }

    private void filterOrders(String searchQuery) {
        ArrayList<Order> orders = new ArrayList<>();
        for (Order order : mOrders) {
            String orderDate = "";
            if (DateTimeUtils.isToday(order.getDate())) {
                orderDate = "Today".toLowerCase();
            } else if (DateTimeUtils.isYesterday(order.getDate())) {
                orderDate = "Yesterday".toLowerCase();
            } else {
                orderDate = new SimpleDateFormat("dd MMM, yyyy").format(order.getDate());
            }
            orderDate += " • " + new SimpleDateFormat("hh:mm a").format(order.getDate());
            String bakerName = order.getBaker().getFirstName().toLowerCase() + " " + order.getBaker().getLastName().toLowerCase();
            String orderStatus = order.getStatus().toLowerCase();
            if (orderDate.contains(searchQuery.toLowerCase()) || bakerName.contains(searchQuery.toLowerCase())
                    || orderStatus.contains(searchQuery.toLowerCase())) {
                orders.add(order);
            }
        }
        mOrders.clear();
        mOrders.addAll(orders);
//        mRecyclerViewBakerItems.setAdapter(mBakersAdapter);
        mYourOrdersAdapter.notifyDataSetChanged();

        // No results found
        if (mYourOrdersAdapter.getItemCount() == 0 && !searchQuery.equals("")) {
            showEmptyView(View.GONE, View.GONE, View.GONE, "No orders found for '" + searchQuery + "'.", getString(R.string.your_orders_get_started));
        }
        // Some results found
        else if (!searchQuery.equals("") && mYourOrdersAdapter.getItemCount() != 0) {
            mTextViewEmptyView.setText("");
            hideEmptyView();
        }
    }

    private void stopRefreshingSwipeLayout() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void toggleRecyclerView(int recyclerViewVisibility) {
        mRecyclerViewYourOrdersItems.setVisibility(recyclerViewVisibility);
    }

    private void showEmptyView(int imageVisibility, int textButtonVisibility, int progressBarVisibility, String textViewText, String textButtonText) {
        mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        mImageViewEmptyView.setVisibility(imageVisibility);
        mTextViewEmptyViewTextButton.setVisibility(textButtonVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mTextViewEmptyView.setText(textViewText);
        mTextViewEmptyViewTextButton.setText(textButtonText);
    }

    private void hideEmptyView() {
        mRelativeLayoutEmptyView.setVisibility(View.GONE);
    }

    public void executeAsyncTaskOrders() {
        showHideMenuItem(mSearchMenuItem);
        mOrders.clear();
        toggleRecyclerView(View.GONE);
        showEmptyView(View.GONE, View.GONE, View.VISIBLE, "", getString(R.string.your_orders_get_started));

        if (!Utils.isNetworkAvailable(this)) {
            stopRefreshingSwipeLayout();
            showHideMenuItem(mSearchMenuItem);
            showEmptyView(View.GONE, View.VISIBLE, View.GONE, getString(R.string.error_no_internet_connection), getString(R.string.shared_try_again));
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                getOrdersFromServer();
                return null;
            }
        }.execute();
    }

    private void getOrdersFromServer() {
        final List<Order> orders = new ArrayList<>();
        final String loggedInUserId = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_GET_ORDERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideEmptyView();
                mProgressBar.setVisibility(View.GONE);
                stopRefreshingSwipeLayout();

                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String responseStatus = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (responseStatus.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {

                        // Get the json array as a response
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        if (responseArray.length() > 0) {

                            // Iterate through the array
                            for (int i = 0; i < responseArray.length(); i++) {
                                List<String> orderImageUrls = new ArrayList<>();
                                List<String> bakerSliderImageUrls = new ArrayList<>();

                                // Get json object inside the array index wise
                                JSONObject orderJsonObject = responseArray.getJSONObject(i);

                                // Get order details of order json object index wise
                                String id = orderJsonObject.getString(ApiConstants.PARAM_ID);
                                String image1 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE1);
                                String image2 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE2);
                                String image3 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE3);
                                String image4 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE4);
                                String image5 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE5);
                                String image6 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE6);
                                String image7 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE7);
                                String image8 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE8);
                                String image9 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE9);
                                String image10 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE10);
                                long date = DateTimeUtils.convertStringDateToLong("yyyy-MM-dd HH:mm:ss", orderJsonObject.getString(ApiConstants.PARAM_CREATED_AT));
                                String status = Utils.getOrderStatusByOrderStatusCode(orderJsonObject.getString(ApiConstants.PARAM_STATUS));
                                String quantity = orderJsonObject.getString(ApiConstants.PARAM_QUANTITY);
                                String pounds = orderJsonObject.getString(ApiConstants.PARAM_POUNDS);
                                String description = orderJsonObject.getString(ApiConstants.PARAM_DESCRIPTION);
                                String contactNumber = orderJsonObject.getString(ApiConstants.PARAM_CONTACT_NUMBER);
                                String deliveryLocation = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_LOCATION);
                                String deliveryAddress = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_ADDRESS);
                                String deliveryDate = DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd", "dd MMM, yyyy", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_DATE));
                                String deliveryTime = DateTimeUtils.getFormattedDateTimeString("HH:mm:ss", "hh:mm a", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_TIME));
                                double subtotal = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL));
                                double deliveryCharges = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES));
                                double totalAmount = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT));
                                String paymentMethod = (orderJsonObject.getString(ApiConstants.PARAM_PAYMENT_METHOD).equals("1"))
                                        ? ApiConstants.PARAM_VALUE_CASH : ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD;
                                String rejectedReason = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON))
                                        ? "The baker didn't provided the reason for rejecting your order." : orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON);
                                double orderRating = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING));
                                String ratedStatus = orderJsonObject.getString(ApiConstants.PARAM_RATED_STATUS);
                                String bakerId = orderJsonObject.getString(ApiConstants.PARAM_BAKER_ID);
                                String image = orderJsonObject.getString(ApiConstants.PARAM_IMAGE);
                                String fn = orderJsonObject.getString(ApiConstants.PARAM_FIRST_NAME);
                                String ln = orderJsonObject.getString(ApiConstants.PARAM_LAST_NAME);
                                String address = orderJsonObject.getString(ApiConstants.PARAM_LOCATION_NAME) + "\n" + orderJsonObject.getString(ApiConstants.PARAM_LOCATION_ADDRESS);
                                String mobileNumber = orderJsonObject.getString(ApiConstants.PARAM_MOBILE_NUMBER);
                                String email = orderJsonObject.getString(ApiConstants.PARAM_EMAIL);
                                bakerSliderImageUrls.add(orderJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE1));
                                bakerSliderImageUrls.add(orderJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE2));
                                bakerSliderImageUrls.add(orderJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE3));
                                double bakerRating = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_RATING))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_RATING));
                                double bakerLatitude = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE));
                                double bakerLongitude = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE));
                                String activeStatus = orderJsonObject.getString(ApiConstants.PARAM_ACTIVE_STATUS);
                                String favoriteId = orderJsonObject.getString(ApiConstants.PARAM_FAVORITE_ID);
                                String userIds = orderJsonObject.getString(ApiConstants.PARAM_USER_IDS);

                                if (!Utils.isStringEmptyOrNull(image1)) {
                                    orderImageUrls.add(image1);
                                }
                                if (!Utils.isStringEmptyOrNull(image2)) {
                                    orderImageUrls.add(image2);
                                }
                                if (!Utils.isStringEmptyOrNull(image3)) {
                                    orderImageUrls.add(image3);
                                }
                                if (!Utils.isStringEmptyOrNull(image4)) {
                                    orderImageUrls.add(image4);
                                }
                                if (!Utils.isStringEmptyOrNull(image5)) {
                                    orderImageUrls.add(image5);
                                }
                                if (!Utils.isStringEmptyOrNull(image6)) {
                                    orderImageUrls.add(image6);
                                }
                                if (!Utils.isStringEmptyOrNull(image7)) {
                                    orderImageUrls.add(image7);
                                }
                                if (!Utils.isStringEmptyOrNull(image8)) {
                                    orderImageUrls.add(image8);
                                }
                                if (!Utils.isStringEmptyOrNull(image9)) {
                                    orderImageUrls.add(image9);
                                }
                                if (!Utils.isStringEmptyOrNull(image10)) {
                                    orderImageUrls.add(image10);
                                }

                                double distance = 0;
                                if (!(Globals.sUserCurrentLocation.getLatitude() <= 0 || Globals.sUserCurrentLocation.getLongitude() <= 0)) {
                                    // Calculate the circular distance between user current and baker location
                                    distance = Utils.calculateCircularDistanceInKilometers(
                                            Globals.sUserCurrentLocation.getLatitude(),
                                            Globals.sUserCurrentLocation.getLongitude(),
                                            bakerLatitude,
                                            bakerLongitude
                                    );
                                }

                                boolean isFavorite = (userIds.contains(loggedInUserId)) ? true : false;

                                Baker baker = new Baker(bakerId, image, fn, ln, address, mobileNumber, email,
                                        bakerSliderImageUrls, bakerRating, Utils.getDoubleValueRoundedOff(distance), isFavorite,
                                        favoriteId);
                                baker.setActiveStatus(activeStatus);

                                orders.add(new Order(id, orderImageUrls, date, status, quantity, pounds, description, contactNumber,
                                        deliveryLocation, deliveryAddress, deliveryDate, deliveryTime, subtotal, deliveryCharges, totalAmount,
                                        paymentMethod, rejectedReason, orderRating, ratedStatus, baker));

                            }
                            hideEmptyView();
                            toggleRecyclerView(View.VISIBLE);
                            mOrders.addAll(orders);
                            mRecyclerViewYourOrdersItems.setAdapter(mYourOrdersAdapter);
                            mYourOrdersAdapter.notifyDataSetChanged();
                            showHideMenuItem(mSearchMenuItem);
                        } else {
                            showHideMenuItem(mSearchMenuItem);
                            toggleRecyclerView(View.GONE);
                            showEmptyView(View.VISIBLE, View.VISIBLE, View.GONE, getString(R.string.your_orders_order_history_empty), getString(R.string.your_orders_get_started));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showHideMenuItem(mSearchMenuItem);
                stopRefreshingSwipeLayout();
                toggleRecyclerView(View.GONE);
                showEmptyView(View.GONE, View.VISIBLE, View.GONE, "Sorry, something went wrong. Please try again.", getString(R.string.shared_try_again));
                if (error != null) {
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_USER_ID, loggedInUserId);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}

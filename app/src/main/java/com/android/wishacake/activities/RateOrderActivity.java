package com.android.wishacake.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.utilities.Utils;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class RateOrderActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private CircleImageView mCircleImageViewBakerImg;
    private TextView mTextViewOrderDate, mTextViewHowWasYourExperience, mTextViewReviewRatingRemarks;
    private MaterialRatingBar mMaterialRatingBarRating;
    private MaterialEditText mMaterialEditTextReview;
    private Button mButtonSubmit;
    private FrameLayout mFrameLayoutSubmit;

    private ProgressDialog mProgressDialog;

    private String mBakerImageUrl, mOrderDate, mBakerName, mBakerId, mOrderId;

    private static final String LOG_TAG = RateOrderActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            setTheme(R.style.AppTheme_NoActionBar_White);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_order);

        initViews();

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Submitting...");

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mOrderDate = extras.getString(Constants.EXTRA_KEY_ORDER_DATE);
            mBakerImageUrl = extras.getString(Constants.EXTRA_KEY_BAKER_IMAGE);
            mBakerName = extras.getString(Constants.EXTRA_KEY_BAKER_NAME);
            mBakerId = extras.getString(Constants.EXTRA_KEY_BAKER_ID);
            mOrderId = extras.getString(Constants.EXTRA_KEY_ORDER_ID);
        }

        Picasso.get().load(mBakerImageUrl).fit().placeholder(R.drawable.ic_default_profile_pic).into(mCircleImageViewBakerImg);
        mTextViewOrderDate.setText(mOrderDate);
        mTextViewHowWasYourExperience.setText("How was your experience with " + mBakerName + "?");

        mMaterialRatingBarRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                if (rating > 0) {
                    if (mMaterialEditTextReview.getVisibility() == View.GONE) {
                        String reviewStr = mMaterialEditTextReview.getText().toString().trim();
                        if (!reviewStr.equals("") || !reviewStr.isEmpty() || reviewStr != null) {
                            mMaterialEditTextReview.setText("");
                        }
                        Utils.expand(mMaterialEditTextReview, 4);
                        mMaterialEditTextReview.requestFocus();
                    }
                } else {
                    if (mMaterialEditTextReview.getVisibility() == View.VISIBLE) {
                        Utils.collapse(mMaterialEditTextReview, 4);
                        mMaterialEditTextReview.setText("");
                    }
                }
                toggleRatingRemarks(rating);
            }
        });

        mMaterialRatingBarRating.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    float touchPositionX = event.getX();
                    float width = mMaterialRatingBarRating.getWidth();
                    float starsf = (touchPositionX / width) * 5.0f;
                    int stars = (int) starsf + 1;
                    mMaterialRatingBarRating.setRating(stars);
                    v.setPressed(false);
                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setPressed(true);
                }
                if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    v.setPressed(false);
                }
                return true;
            }
        });

        mMaterialEditTextReview.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (mMaterialEditTextReview.getText().toString().trim().isEmpty()) {
                    mFrameLayoutSubmit.setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
                    mButtonSubmit.setEnabled(false);
                } else {
                    mFrameLayoutSubmit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    mButtonSubmit.setEnabled(true);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSubmit();
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        onClickBack();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onClickBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mCircleImageViewBakerImg = (CircleImageView) findViewById(R.id.civ_baker_image);
        mTextViewOrderDate = (TextView) findViewById(R.id.tv_order_date);
        mTextViewReviewRatingRemarks = (TextView) findViewById(R.id.tv_review_rating_remarks);
        mTextViewHowWasYourExperience = (TextView) findViewById(R.id.tv_how_was_your_experience);
        mMaterialRatingBarRating = (MaterialRatingBar) findViewById(R.id.mrb_review_rating);
        mMaterialEditTextReview = (MaterialEditText) findViewById(R.id.et_review);
        mButtonSubmit = (Button) findViewById(R.id.btn_submit);
        mFrameLayoutSubmit = (FrameLayout) findViewById(R.id.frame_layout_submit);
    }

    private void onClickBack() {
        if (!this.isAnyUnsavedChangesAvailable()) {
            finish();
        } else {
            showUnsavedChangesDialog();
        }
    }

    private boolean isAnyUnsavedChangesAvailable() {
        boolean check = false;
        if (!mMaterialEditTextReview.getText().toString().trim().isEmpty()) {
            check = true;
        }
        return check;
    }

    private void showUnsavedChangesDialog() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.discard_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.cancel_alert_dialog_button), null);

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setText(getString(R.string.shared_discard_your_changes));

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
    }

    private void toggleRatingRemarks(float rating) {
        if (rating == 0) {
            mTextViewReviewRatingRemarks.setText("");
            mTextViewReviewRatingRemarks.setVisibility(View.GONE);
        }
        else if (rating == 1) {
            mTextViewReviewRatingRemarks.setText("Poor");
            mTextViewReviewRatingRemarks.setVisibility(View.VISIBLE);
        }
        else if (rating == 2) {
            mTextViewReviewRatingRemarks.setText("Fair");
            mTextViewReviewRatingRemarks.setVisibility(View.VISIBLE);
        }
        else if (rating == 3) {
            mTextViewReviewRatingRemarks.setText("Good");
            mTextViewReviewRatingRemarks.setVisibility(View.VISIBLE);
        }
        else if (rating == 4) {
            mTextViewReviewRatingRemarks.setText("Very good");
            mTextViewReviewRatingRemarks.setVisibility(View.VISIBLE);
        }
        else if (rating == 5) {
            mTextViewReviewRatingRemarks.setText("Excellent");
            mTextViewReviewRatingRemarks.setVisibility(View.VISIBLE);
        }
    }

    private void onClickSubmit() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_CREATE_REVIEW, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);
                            String responseMsg = rootJsonObject.getString(ApiConstants.KEY_RESPONSE);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                ActivityCompat.finishAffinity(RateOrderActivity.this);
                                startActivity(new Intent(RateOrderActivity.this, MainActivity.class));
                            }
                            else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                Toast.makeText(RateOrderActivity.this, responseMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(RateOrderActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_USER_ID, SharedPrefSingleton.getInstance(RateOrderActivity.this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID));
                        params.put(ApiConstants.PARAM_BAKER_ID, mBakerId);
                        params.put(ApiConstants.PARAM_ORDER_ID, mOrderId);
                        params.put(ApiConstants.PARAM_RATING, String.valueOf(mMaterialRatingBarRating.getRating()));
                        params.put(ApiConstants.PARAM_REVIEW, mMaterialEditTextReview.getText().toString().trim());
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(RateOrderActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }
}

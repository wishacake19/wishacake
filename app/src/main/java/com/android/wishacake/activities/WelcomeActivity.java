package com.android.wishacake.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WelcomeActivity extends AppCompatActivity {

    private Button mButtonEmail, mButtonFacebook, mButtonGuest;
    private TextView mTextViewLogin;

    private CallbackManager mCallbackManager;

    private String mFirstName, mLastName, mEmail;

    private ProgressDialog mProgressDialog;

    private static final String LOG_TAG = WelcomeActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            setTheme(R.style.AppTheme_WelcomeTheme);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_welcome);

        if (SharedPrefSingleton.getInstance(this).isUserLoggedIn()) {
            startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
            finish();
            return;
        }

        getSupportActionBar().hide();

        initViews();

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);

        mButtonEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Globals.sIsSignUpOpenedFromLogin = false;
                startActivity(new Intent(WelcomeActivity.this, SignUpActivity.class));
//                finish();
            }
        });

        mCallbackManager = CallbackManager.Factory.create();

        mButtonFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSignUpWithFb();
            }
        });

        mButtonGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Globals.sIsGuestUserLoggedIn = true;
                startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
            }
        });

        mTextViewLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mButtonEmail = (Button) findViewById(R.id.btn_sign_up_with_email);
        mButtonFacebook = (Button) findViewById(R.id.btn_sign_up_with_fb);
        mButtonGuest = (Button) findViewById(R.id.btn_continue_as_guest);
        mTextViewLogin = (TextView) findViewById(R.id.tv_log_in);
    }

    private void printHashKey() {
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo("com.android.wishacake",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : packageInfo.signatures) {
                try {
                    MessageDigest messageDigest = MessageDigest.getInstance("SHA");
                    messageDigest.update(signature.toByteArray());
                    Log.i(LOG_TAG + " HASH KEY :",
                            Base64.encodeToString(messageDigest.digest(), Base64.DEFAULT));
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void onClickSignUpWithFb() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }

        LoginManager.getInstance().logInWithReadPermissions(WelcomeActivity.this,
                Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    mFirstName = object.getString("first_name");
                                    mLastName = object.getString("last_name");
                                    mEmail = object.getString("email");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                // Request Graph API
                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name,last_name,email");
                request.setParameters(parameters);
                request.executeAsync();

                Log.v(LOG_TAG, "First name: " + mFirstName);
                Log.v(LOG_TAG, "Last name: " + mLastName);
                Log.v(LOG_TAG, "Email: " + mEmail);

                if (mFirstName == null || mLastName == null || mEmail == null) {
                    onClickSignUpWithFb();
                } else {
                    mProgressDialog.setMessage("Processing...");
                    mProgressDialog.show();

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            signUpWithFacebook();
                            return null;
                        }
                    }.execute();
                }
            }

            @Override
            public void onCancel() {
                Log.v(LOG_TAG, "onCancel() called of Fb");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(LOG_TAG, "onError() called of Fb: " + error.toString());
            }
        });
    }

    private void signUpWithFacebook() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_SIGN_UP_WTIH_FACEBOOK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);
                            String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS) || (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("-2"))) {
                                JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                                String id = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                                String fn = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                                String ln = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                                String mobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                                String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                                String emailVerifiedStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL_VERIFIED_STATUS);
                                String password = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PASSWORD);
                                String accountType = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_TYPE);
                                String accountStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_STATUS);
                                String modifiedAt = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MODIFIED_AT);

                                // Verified
                                if (emailVerifiedStatus.equals("1")) {
                                    // Active user
                                    if (accountStatus.equals("1")) {
                                        SharedPrefSingleton.getInstance(WelcomeActivity.this).saveLoggedInUserData(id, fn, ln, mobileNumber,
                                                email, password, accountType);
                                        Globals.sIsGuestUserLoggedIn = false;
                                        startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
                                    }
                                    // Blocked user
                                    else if (accountStatus.equals("0")) {
                                        Utils.showCustomOkAlertDialog(WelcomeActivity.this, "Your account has been temporarily blocked", "For further details please visit your nearest wishacake help center or call us at our helpline (021) 111-111-123.");
                                    }
                                }
                                // Unverified
                                else if (emailVerifiedStatus.equals("0")) {
                                    // First time FB login
                                    if (errorCode.equals("")) {
                                        Utils.showCustomOkAlertDialog(WelcomeActivity.this, "Your account has been created successfully",
                                                "To continue using " + getString(R.string.app_name) + ", you'll need to verify your account." +
                                                        " We have sent an email to \"" + mEmail + "\" containing instructions on how to verify your account.");
                                    } else if (errorCode.equals("-2")) {
                                        String dateTimeStr = DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd HH:mm:ss", "dd MMM, yyyy", modifiedAt) + " at " + DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd HH:mm:ss", "hh:mm a", modifiedAt);
                                        Utils.showResendVerificationEmailDialog(WelcomeActivity.this, mEmail, "A verification email was sent to \"" + mEmail + "\" on " + dateTimeStr + ". If you can't find it, request a new one by tapping the resend button below.");
                                    }
                                }
                            } else {
                                if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("0")) {
                                    showDialogIfEmailAlreadyExist(mEmail, rootJsonObject.getString(ApiConstants.KEY_RESPONSE));
                                } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
                                    Toast.makeText(WelcomeActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(WelcomeActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_FIREBASE_TOKEN, Utils.getFirebaseToken());
                params.put(ApiConstants.PARAM_FIRST_NAME, mFirstName);
                params.put(ApiConstants.PARAM_LAST_NAME, mLastName);
                params.put(ApiConstants.PARAM_EMAIL, mEmail);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void showDialogIfEmailAlreadyExist(String title, String msg) {
        View dialogView = LayoutInflater.from(WelcomeActivity.this).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(WelcomeActivity.this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.login_alert_dialog_button), null);

        final TextView textViewTitle = (TextView) dialogView.findViewById(R.id.tv_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(title);

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText)); // Default in XML is PrimaryTextColor
        textViewMsg.setText(msg);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
                finish();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }
}

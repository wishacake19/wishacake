package com.android.wishacake.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.wishacake.R;
import com.android.wishacake.adapters.CakeImageAdapter;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;

import java.util.ArrayList;
import java.util.List;

public class FeaturedCakeDetailsActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private RecyclerView mRecyclerViewFeaturedCakeImagesItems;
    private TextView mTextViewDescription;
    private Button mButtonOrderNow;

    private CakeImageAdapter mCakeImageAdapter;
    private List<String> mImageUrls;
    private String mTitle, mDescription;

    private static final String LOG_TAG = FeaturedCakeDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_featured_cake_details);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(FeaturedCakeDetailsActivity.this, R.drawable.ic_arrow_back_white));

        mImageUrls = new ArrayList<>();

        setUpFeaturedCakeData();

        mCakeImageAdapter = new CakeImageAdapter(this, mImageUrls);
        mRecyclerViewFeaturedCakeImagesItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewFeaturedCakeImagesItems.setAdapter(mCakeImageAdapter);

        mButtonOrderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Globals.sIsGuestUserLoggedIn) {
                    Globals.sIsSignUpOpenedFromLogin = false;
                    Intent intent = new Intent(FeaturedCakeDetailsActivity.this, SignUpActivity.class);
                    intent.putExtra(Constants.EXTRA_KEY_IS_SIGN_UP_FOR_GUEST_USER, true);
                    startActivity(intent);
                    return;
                }

                Intent intent = new Intent(FeaturedCakeDetailsActivity.this, PreviewImagesActivity.class);
                intent.putExtra(Constants.EXTRA_KEY_TITLE_PREVIEW_IMAGES_ACTIVITY, getString(R.string.activity_title_preview_images));
                intent.putExtra(Constants.EXTRA_KEY_IS_PREVIEW_IMAGES_OPEN_FROM_UNITY, false);
                intent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, (ArrayList<String>) mImageUrls);
                startActivity(intent);
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.menu_share, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
//            case R.id.action_share:
//                Toast.makeText(this, "Not implemented yet.", Toast.LENGTH_SHORT).show();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRecyclerViewFeaturedCakeImagesItems = (RecyclerView) findViewById(R.id.rv_featured_cake_images_items);
        mTextViewDescription = (TextView) findViewById(R.id.tv_featured_cake_description);
        mButtonOrderNow = (Button) findViewById(R.id.btn_order_now);
    }

    private void setUpFeaturedCakeData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            mImageUrls = extras.getStringArrayList(Constants.EXTRA_KEY_FEATURED_CAKE_IMAGES);
            mTitle = extras.getString(Constants.EXTRA_KEY_FEATURED_CAKE_TITLE);
            mDescription = extras.getString(Constants.EXTRA_KEY_FEATURED_CAKE_DESCRIPTION);
        }

        getSupportActionBar().setTitle(mTitle);

        if (mDescription != null && !mDescription.isEmpty() && !mDescription.equals("")) {
            mTextViewDescription.setText(mDescription);
        } else {
            mTextViewDescription.setVisibility(View.GONE);
            mTextViewDescription.setText("");
        }
    }
}
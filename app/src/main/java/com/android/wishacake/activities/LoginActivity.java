package com.android.wishacake.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.ErrorTextWatcher;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.Utils;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    //    private TextView mTextViewHeading;
    private TextView mTextViewEmailHelper, mTextViewForgotPassEmailHelper, mTextViewPassHelper, mTextViewForgotPassword;
    private MaterialEditText mMaterialEditTextEmail, mMaterialEditTextForgotPassEmail, mMaterialEditTextPass;
    private Button mButtonLogin, mButtonSignUp;

    private String mEmail, mEmailForgotPass, mPass;

    private ProgressDialog mProgressDialog;

    private static final String LOG_TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            setTheme(R.style.AppTheme_NoActionBar_White);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);

        mMaterialEditTextEmail.addTextChangedListener(new ErrorTextWatcher(mTextViewEmailHelper));
        mMaterialEditTextPass.addTextChangedListener(new ErrorTextWatcher(mTextViewPassHelper));

        mTextViewForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickForgotPassword();
            }
        });

        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickLogin();
            }
        });

        mButtonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Globals.sIsSignUpOpenedFromLogin = true;
                startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
                finish();
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAffinity(LoginActivity.this); // Clear the previous activities stack
        startActivity(new Intent(LoginActivity.this, WelcomeActivity.class));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                ActivityCompat.finishAffinity(LoginActivity.this); // Clear the previous activities stack
                startActivity(new Intent(LoginActivity.this, WelcomeActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        mTextViewHeading = (TextView) findViewById(R.id.tv_login_heading);
        mMaterialEditTextEmail = (MaterialEditText) findViewById(R.id.et_email);
        mTextViewEmailHelper = (TextView) findViewById(R.id.tv_email_helper);
        mMaterialEditTextPass = (MaterialEditText) findViewById(R.id.et_pass);
        mTextViewPassHelper = (TextView) findViewById(R.id.tv_pass_helper);
        mButtonLogin = (Button) findViewById(R.id.btn_login);
        mTextViewForgotPassword = (TextView) findViewById(R.id.tv_forgot_password);
        mButtonSignUp = (Button) findViewById(R.id.btn_sign_up);

//        mTextViewHeading.setText(Html.fromHtml("<font color='#9b9b9b'>Let's</font>"
//                + "<br><font color='#f06292'>" + getString(R.string.login_heading) + "</font>"));
    }

    private void onClickLogin() {
        if (isValid()) {
            if (!Utils.isNetworkAvailable(getApplicationContext())) {
                Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
                // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
                return;
            }
            mProgressDialog.setMessage("Authenticating...");
            mProgressDialog.show();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    login();
                    return null;
                }
            }.execute();
        }
    }

    @SuppressLint("StringFormatInvalid")
    private boolean isValid() {
        boolean flag = false;

        mEmail = mMaterialEditTextEmail.getText().toString().trim();
        mPass = mMaterialEditTextPass.getText().toString().trim();

        if (mEmail.isEmpty()) {
            Utils.showError(mMaterialEditTextEmail, getString(R.string.error_required, getString(R.string.shared_hint_email)), mTextViewEmailHelper);
        } else if (Utils.isEmailValid(mEmail) == false) {
            Utils.showError(mMaterialEditTextEmail, getString(R.string.error_badly_formatted, getString(R.string.shared_hint_email)), mTextViewEmailHelper);
        } else if (mPass.isEmpty()) {
            Utils.showError(mMaterialEditTextPass, getString(R.string.error_required, getString(R.string.shared_hint_pass)), mTextViewPassHelper);
        } else {
            flag = true;
        }
        return flag;
    }

    private void login() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_LOGIN, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        String id = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                        String fn = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                        String ln = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                        String mobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                        String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                        String emailVerifiedStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL_VERIFIED_STATUS);
                        String password = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PASSWORD);
                        String accountType = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_TYPE);
                        String accountStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_STATUS);
                        String modifiedAt = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MODIFIED_AT);

                        // Verified
                        if (emailVerifiedStatus.equals("1")) {
                            // Active user
                            if (accountStatus.equals("1")) {
                                SharedPrefSingleton.getInstance(LoginActivity.this).saveLoggedInUserData(id, fn, ln, mobileNumber,
                                        email, password, accountType);
                                Globals.sIsGuestUserLoggedIn = false;
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            }
                            // Blocked user
                            else if (accountStatus.equals("0")) {
                                Utils.showCustomOkAlertDialog(LoginActivity.this, "Your account has been temporarily blocked", "For further details please visit your nearest wishacake help center or call us at our helpline (021) 111-111-123.");
                            }
                        }
                        // Unverified
                        else if (emailVerifiedStatus.equals("0")) {
                            String dateTimeStr = DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd HH:mm:ss", "dd MMM, yyyy", modifiedAt) + " at " + DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd HH:mm:ss", "hh:mm a", modifiedAt);
                            Utils.showResendVerificationEmailDialog(LoginActivity.this, mEmail, "A verification email was sent to \"" + mEmail + "\" on " + dateTimeStr + ". If you can't find it, request a new one by tapping the resend button below.");
                        }
                    } else {
                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);

                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && (errorCode.equals("0") || errorCode.equals("-2"))) {
                            Utils.showError(mMaterialEditTextEmail, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                    mTextViewEmailHelper);
                        } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("-1")) {
                            Utils.showError(mMaterialEditTextPass, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                    mTextViewPassHelper);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(LoginActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_FIREBASE_TOKEN, Utils.getFirebaseToken());
                params.put(ApiConstants.PARAM_EMAIL, mEmail.toLowerCase());
                params.put(ApiConstants.PARAM_PASSWORD, mPass);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void onClickForgotPassword() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_forgot_pass_dialog, null);

        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.login_forgot_pass_dialog_btn_reset_password), null);

        mMaterialEditTextForgotPassEmail = (MaterialEditText) dialogView.findViewById(R.id.et_email_forgot_pass);
        mTextViewForgotPassEmailHelper = (TextView) dialogView.findViewById(R.id.tv_email_forgot_pass_helper);

        mMaterialEditTextForgotPassEmail.addTextChangedListener(new ErrorTextWatcher(mTextViewForgotPassEmailHelper));

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                // Hides the keyboard
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StringFormatInvalid")
            @Override
            public void onClick(View v) {
                mEmailForgotPass = mMaterialEditTextForgotPassEmail.getText().toString().trim();
                if (mEmailForgotPass.isEmpty()) {
                    Utils.showError(mMaterialEditTextForgotPassEmail, getString(R.string.error_required,
                            getString(R.string.shared_hint_email)), mTextViewForgotPassEmailHelper);
                } else if (Utils.isEmailValid(mEmailForgotPass) == false) {
                    Utils.showError(mMaterialEditTextForgotPassEmail, getString(R.string.error_badly_formatted,
                            getString(R.string.shared_hint_email)), mTextViewForgotPassEmailHelper);
                } else {
                    if (!Utils.isNetworkAvailable(getApplicationContext())) {
                        Toast.makeText(LoginActivity.this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
                        // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
                        return;
                    }
                    mProgressDialog.setMessage("Requesting password reset email...");
                    mProgressDialog.show();

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_FORGOT_PASSWORD, new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    mProgressDialog.dismiss();
                                    try {
                                        JSONObject rootJsonObject = new JSONObject(response);
                                        String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                                        if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                            alertDialog.dismiss();

                                            // Hides the keyboard
                                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                                            Utils.showCustomOkAlertDialog(LoginActivity.this, "",
                                                    "We have sent an email to \"" + mEmailForgotPass + "\" containing instructions on how to reset your password.");
                                        } else {
                                            String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
                                            if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("0")) {
                                                Utils.showError(mMaterialEditTextForgotPassEmail, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                                        mTextViewForgotPassEmailHelper);
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    mProgressDialog.dismiss();
                                    if (error != null) {
                                        String toastMsg = "Sorry, something went wrong. Please try again.";
                                        Toast.makeText(LoginActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                                        Log.e(LOG_TAG, error.toString());
                                    }
                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<>();
                                    params.put(ApiConstants.PARAM_EMAIL, mEmailForgotPass.toLowerCase());
                                    return params;
                                }
                            };

                            stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                            VolleyRequestHandlerSingleton.getInstance(LoginActivity.this).addToRequestQueue(stringRequest);
                            return null;
                        }
                    }.execute();
                }
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }
}

package com.android.wishacake.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wishacake.R;
import com.android.wishacake.adapters.PreviewImagesAdapter;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.utilities.PermissionUtils;
import com.facebook.drawee.backends.pipeline.Fresco;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import me.relex.photodraweeview.PhotoDraweeView;

public class PreviewImagesActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;

    private RecyclerView mRecyclerViewImageItems;
    private Button mButtonCancel;
    public static FloatingActionButton sFabAdd;
    public static Button sButtonContinue;
    public static FrameLayout sFrameLayoutContinue;

    private PreviewImagesAdapter mPreviewImagesAdapter;
    private List<String> mImages;

    private boolean mIsOpenedFromUnity = false;

    public static PhotoDraweeView sPhotoDraweeViewImage;

    private static final int REQUEST_CODE_GALLERY = 01;
    private static final String LOG_TAG = PreviewImagesActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(PreviewImagesActivity.this);
        setContentView(R.layout.activity_preview_images);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.EXTRA_KEY_TITLE_PREVIEW_IMAGES_ACTIVITY)) {
                getSupportActionBar().setTitle(extras.getString(Constants.EXTRA_KEY_TITLE_PREVIEW_IMAGES_ACTIVITY));
            }
            if (extras.containsKey(Constants.EXTRA_KEY_IS_PREVIEW_IMAGES_OPEN_FROM_UNITY)) {
                mIsOpenedFromUnity = extras.getBoolean(Constants.EXTRA_KEY_IS_PREVIEW_IMAGES_OPEN_FROM_UNITY);

                // Preview mode on from unity
                if (mIsOpenedFromUnity == true) {
                    sFabAdd.setEnabled(false);
                    sFabAdd.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(PreviewImagesActivity.this, R.color.colorPrimaryLight)));

                    mImages = new ArrayList<>();
                    mPreviewImagesAdapter = new PreviewImagesAdapter(this);
                    mRecyclerViewImageItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                    mRecyclerViewImageItems.setAdapter(mPreviewImagesAdapter);

                    int uniqueId = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
                    String uniqueFolderId = "wishacake_" + uniqueId;

                    File sourceLocation = new File("/storage/emulated/0/Android/data/com.android.wishacake/files/");
                    File targetLocation = new File("/storage/emulated/0/wishacake/" + (uniqueFolderId) + "/");

                    Log.e(LOG_TAG, "Source location: " + sourceLocation);
                    Log.e(LOG_TAG, "Target location: " + targetLocation);

                    try {
                        if (!sourceLocation.exists()) {
                            Log.e(LOG_TAG, "Target location does not exist.");
                            return;
                        } else if (sourceLocation.exists()) {
                            File unityLocation = new File(sourceLocation + "/Unity");
                            deleteDirectory(unityLocation);
                            Log.e(LOG_TAG, "Unity folder deleted successfully: " + unityLocation);

                            File picturesLocation = new File(sourceLocation + "/Pictures");
                            deleteDirectory(picturesLocation);
                            Log.e(LOG_TAG, "Pictures folder deleted successfully: " + picturesLocation);

                            copyDirectoryOneLocationToAnotherLocation(sourceLocation, targetLocation);
                            Log.e(LOG_TAG, "Images moved successfully.");

                            deleteDirectory(sourceLocation);
                            Log.e(LOG_TAG, "Other remaining files deleted successfully: " + sourceLocation);
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                // Preview mode on from application
                else {
                    if (extras.containsKey(Constants.EXTRA_KEY_ORDER_IMAGES)) {
                        mImages = extras.getStringArrayList(Constants.EXTRA_KEY_ORDER_IMAGES);
                        mPreviewImagesAdapter = new PreviewImagesAdapter(this);
                        mRecyclerViewImageItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                        mRecyclerViewImageItems.setAdapter(mPreviewImagesAdapter);
                        mPreviewImagesAdapter.setImages(mImages);

                        if (mPreviewImagesAdapter.getItemCount() > 0) {
                            sFabAdd.setEnabled(false);
                            sFabAdd.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(PreviewImagesActivity.this, R.color.colorPrimaryLight)));
                        }
                    }
                }
            }
            // Upload mode
            else {
                mImages = new ArrayList<>();
                mPreviewImagesAdapter = new PreviewImagesAdapter(this);
                mRecyclerViewImageItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
                mRecyclerViewImageItems.setAdapter(mPreviewImagesAdapter);
            }
        }

        sFabAdd.setOnClickListener(this);
        mButtonCancel.setOnClickListener(this);
        sButtonContinue.setOnClickListener(this);
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        if (!getSupportActionBar().getTitle().toString().equals(getString(R.string.activity_title_preview_images))) {
            finish();
        } else {
            if (!this.isAnyUnsavedChangesAvailable()) {
                finish();
            } else {
                showUnsavedChangesDialog();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == sFabAdd) {
            if (PermissionUtils.checkGalleryPermission(PreviewImagesActivity.this)) {
                openGallery();
            }
        }
        if (view == mButtonCancel) {
            onBackPressed();
        }
        if (view == sButtonContinue) {
            if (Globals.sIsGuestUserLoggedIn) {
                Globals.sIsSignUpOpenedFromLogin = false;
                Intent intent = new Intent(PreviewImagesActivity.this, SignUpActivity.class);
                intent.putExtra(Constants.EXTRA_KEY_IS_SIGN_UP_FOR_GUEST_USER, true);
                startActivity(intent);
                return;
            }
            Intent intent = new Intent(PreviewImagesActivity.this, DeliveryLocationActivity.class);
            intent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, (ArrayList<String>) mPreviewImagesAdapter.getImages());
            startActivity(intent);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtils.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean writeStoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeStoragePermission) {
                    openGallery();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK) {
            if (data != null) {
                try {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        // For more than one images
                        if (data.getClipData() != null) {
                            // Evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                            int count = data.getClipData().getItemCount();
                            if (count > Constants.ORDER_IMAGES_SIZE) {
                                count = 10;
                                Toast.makeText(this, "Only " + Constants.ORDER_IMAGES_SIZE + " images can be processed.", Toast.LENGTH_SHORT).show();
                            }
                            for (int i = 0; i < count; i++) {
                                if (mImages.size() + 1 > Constants.ORDER_IMAGES_SIZE) {
                                    Toast.makeText(this, "Only " + Constants.ORDER_IMAGES_SIZE + " images can be processed.", Toast.LENGTH_SHORT).show();
                                    break;
                                }
                                Uri imageUri = data.getClipData().getItemAt(i).getUri();
                                mImages.add(imageUri.toString());
                            }
                            mPreviewImagesAdapter.setImages(mImages);
                        }
                        // For single image
                        else {
                            Uri uri = data.getData();
                            mImages.add(uri.toString());
                            mPreviewImagesAdapter.setImages(mImages);
                        }
                    } else {
                        Uri uri = data.getData();
                        mImages.add(uri.toString());
                        mPreviewImagesAdapter.setImages(mImages);
                    }
                } catch (Exception e) {
                    if (e != null) {
                        Log.e(LOG_TAG, e.getMessage());
                    }
                }
            }
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        sPhotoDraweeViewImage = (PhotoDraweeView) findViewById(R.id.pdv_img);
        mRecyclerViewImageItems = (RecyclerView) findViewById(R.id.rv_image_items);
        sFabAdd = (FloatingActionButton) findViewById(R.id.fab_add);
        mButtonCancel = (Button) findViewById(R.id.btn_cancel);
        sButtonContinue = (Button) findViewById(R.id.btn_continue);
        sFrameLayoutContinue = (FrameLayout) findViewById(R.id.frame_layout_continue);
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select picture(s)"), REQUEST_CODE_GALLERY);
    }

    private boolean isAnyUnsavedChangesAvailable() {
        boolean check = false;
        if (mPreviewImagesAdapter.getItemCount() > 0) {
            check = true;
        }
        return check;
    }

    private void showUnsavedChangesDialog() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.discard_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.cancel_alert_dialog_button), null);

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setText(getString(R.string.shared_discard_your_changes));

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
    }

    private boolean deleteDirectory(File directoryFile) {
        if (directoryFile.isDirectory()) {
            String[] children = directoryFile.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDirectory(new File(directoryFile, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return directoryFile.delete();
    }

    private void copyDirectoryOneLocationToAnotherLocation(File sourceLocation, File targetLocation) throws IOException {
        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }
            String[] children = sourceLocation.list();
            for (int i = 0; i < sourceLocation.listFiles().length; i++) {
                String imagePath = targetLocation + "/" + children[i];
                File imageFile = new File(imagePath);
                Uri uri = Uri.fromFile(imageFile);
                copyDirectoryOneLocationToAnotherLocation(new File(sourceLocation, children[i]), new File(targetLocation, children[i]));
                refreshAndroidGallery(uri);
                addImageToList(imagePath);
                Log.e(LOG_TAG, "IMG Image path: " + imagePath);
            }
            mPreviewImagesAdapter.setImages(mImages);
            int test = 1;
            test = 2;
        } else {
            InputStream in = new FileInputStream(sourceLocation);
            OutputStream out = new FileOutputStream(targetLocation);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }
    }

    private void refreshAndroidGallery(Uri fileUri) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            mediaScanIntent.setData(fileUri);
            sendBroadcast(mediaScanIntent);
        } else {
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }

    private void addImageToList(String imagePath) {
        File imageFile = new File(imagePath);
        if (imageFile.exists()) {
            Uri imageUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                imageUri = FileProvider.getUriForFile(PreviewImagesActivity.this, getPackageName() + ".fileprovider", imageFile);
            } else {
                imageUri = Uri.fromFile(imageFile);
            }
            mImages.add(imageUri.toString());
            Log.e(LOG_TAG, "Image list size: " + mImages.size());
        }
    }
}

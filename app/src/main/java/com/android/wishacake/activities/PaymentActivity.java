package com.android.wishacake.activities;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wishacake.R;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.utilities.Utils;

public class PaymentActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private LinearLayout mLinearLayoutCard;
    private TextView mTextViewUserCardNo;
    private ImageView mImageViewCard;

    private String mUserCardNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(PaymentActivity.this, R.drawable.ic_arrow_back_white));

        mLinearLayoutCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SharedPrefSingleton.getInstance(PaymentActivity.this).isCardAdded()) {
                    final CharSequence[] items = {"View details", "Delete"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this, R.style.AlertDialogTheme);
                    builder.setItems(items, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (items[item].equals("View details")) {
                                Toast.makeText(PaymentActivity.this, "Not implemented yet.", Toast.LENGTH_SHORT).show();
                            } else if (items[item].equals("Delete")) {
                                View dialogView = LayoutInflater.from(PaymentActivity.this).inflate(R.layout.custom_alert_dialog, null);
                                final AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivity.this, R.style.AlertDialogTheme);
                                builder.setView(dialogView);
                                builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
                                builder.setPositiveButton(getString(R.string.shared_delete), null);

                                TextView textViewMessage = (TextView) dialogView.findViewById(R.id.tv_msg);
                                textViewMessage.setText("Changes will apply only to this device not to your account.");

                                final AlertDialog alertDialog = builder.create();
                                alertDialog.setCancelable(true);

                                alertDialog.show();

                                /*
                                 *
                                 * Following code must be called after show() method of alert dialog
                                 */

                                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        alertDialog.dismiss();
                                    }
                                });

                                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                                    @SuppressLint("StringFormatInvalid")
                                    @Override
                                    public void onClick(View v) {
                                        SharedPrefSingleton.getInstance(PaymentActivity.this).removeCard();
                                        toggleCardLayoutView();
                                        alertDialog.dismiss();
                                    }
                                });

                                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(PaymentActivity.this, R.color.colorSecondaryText));
                                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(PaymentActivity.this, R.color.colorOrderStatusRejected));
                            }
                        }
                    });

                    AlertDialog alertDialog = builder.create();
                    alertDialog.setTitle(mTextViewUserCardNo.getText().toString().trim());
                    alertDialog.setCancelable(true);

                    alertDialog.show();

                    TextView textViewTitle = (TextView) alertDialog.findViewById(R.id.alertTitle);
                    textViewTitle.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/montserrat_semibold.ttf"));
                    textViewTitle.setTextSize(18);
                } else {
                    Intent intent = new Intent(PaymentActivity.this, AddCardActivity.class);
                    intent.putExtra(Constants.EXTRA_KEY_IS_ADD_CARD_OPENED_FROM_CHECKOUT, false);
                    startActivity(intent);
                }
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        toggleCardLayoutView();
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mLinearLayoutCard = (LinearLayout) findViewById(R.id.linear_layout_card);
        mTextViewUserCardNo = (TextView) findViewById(R.id.tv_card);
        mImageViewCard = (ImageView) findViewById(R.id.img_card);
    }

    private void toggleCardLayoutView() {
        if (SharedPrefSingleton.getInstance(this).isCardAdded()) {
            mTextViewUserCardNo.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryText));

            mUserCardNo = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_CARD_NUMBER);
            mTextViewUserCardNo.setText(Utils.getFormattedCardNumber(mUserCardNo));

            if (Utils.getCardType(mUserCardNo).equalsIgnoreCase(Constants.MASTER_CARD)) {
                mImageViewCard.setImageResource(R.drawable.ic_master_card);
            } else if (Utils.getCardType(mUserCardNo).equalsIgnoreCase(Constants.VISA)) {
                mImageViewCard.setImageResource(R.drawable.ic_visa);
            } else if (Utils.getCardType(mUserCardNo).equalsIgnoreCase(Constants.AMERICAN_EXPRESS)) {
                mImageViewCard.setImageResource(R.drawable.ic_american_express);
            } else {
                mImageViewCard.setImageResource(R.drawable.ic_card);
            }
        } else {
            mTextViewUserCardNo.setText("Add payment method");
            mTextViewUserCardNo.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
            mImageViewCard.setImageResource(R.drawable.ic_add);
        }
    }
}

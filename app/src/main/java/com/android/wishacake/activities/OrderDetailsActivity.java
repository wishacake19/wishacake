package com.android.wishacake.activities;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.adapters.CakeImageAdapter;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.ExpandableTextView;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import me.zhanghai.android.materialratingbar.MaterialRatingBar;

import static android.support.v7.app.AlertDialog.BUTTON_NEGATIVE;
import static android.support.v7.app.AlertDialog.BUTTON_POSITIVE;
import static android.support.v7.app.AlertDialog.Builder;

public class OrderDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;

    private TextView mTextViewDate, mTextViewStatus, mTextViewId, mTextViewQuantity, mTextViewPounds,
            mTextViewContactNumberCollapsed, mTextViewName, mTextViewEmailAddress, mTextViewContactNumberExpanded, mTextViewAmountCollapsed, mTextViewSubtotal, mTextViewDeliveryCharges, mTextViewAmountExpanded,
            mTextViewDeliveryDateCollapsed, mTextViewDeliveryLocation, mTextViewDeliveryAddress, mTextViewDeliveryDateExpanded, mTextViewDeliveryTime,
            mTextViewBakerName, mTextViewBakerAddress;
    private MaterialRatingBar mMaterialRatingBarOrderRating;
    private ImageView mImageViewArrowContact, mImageViewPaymentMethod, mImageViewArrowPayment, mImageViewArrowDelivery;
    private RelativeLayout mRelativeLayoutRootEmptyView, mRelativeLayoutEmptyView, mRelativeLayoutContactInfo, mRelativeLayoutDeliveryInfo, mRelativeLayoutPaymentInfo, mRelativeLayoutBakerInfo;
    private LinearLayout mLinearLayoutContactInfo, mLinearLayoutDeliveryInfo, mLinearLayoutPaymentInfoRootContainer,
            mLinearLayoutPaymentInfo, mLinearLayoutOrderRating, mLinearLayoutButtonsConfirmCancel, mLinearLayoutButtonReportAProblem,
            mLinearLayoutButtonViewReason;
    private ProgressBar mProgressBar;
    private TextView mTextViewEmptyView, mTextViewEmptyViewTextButton;
    private FrameLayout mFrameLayoutButtonConfirm;
    private ExpandableTextView mExpandableTextViewDescription;
    private CircleImageView mCircleImageViewBakerImg;
    private ImageView mImageViewOnline;
    private Button mButtonConfirm, mButtonCancel, mButtonReportAProblem, mButtonViewReason;
    private TextView mTextViewRateThisOrder;

    private ProgressDialog mProgressDialog;

    private ScrollView mScrollView;

    private CakeImageAdapter mCakeImageAdapter;
    private RecyclerView mRecyclerViewOrderImagesItems;

    private List<String> mOrderImageUrls;
    private long mDate;
    private String mQuantity, mPounds, mStatus = "", mOrderId, mDescription, mContactNumber, mDeliveryLocation,
            mDeliveryAddress, mDeliveryDate, mDeliveryTime, mPaymentMethod, mActiveStatus, mRejectedReason,
            mRatedStatus;
    private String mSubtotalStr, mDeliveryChargesStr, mTotalAmountStr;
    private double mSubtotal, mDeliveryCharges, mTotalAmount, mOrderRating;

    private List<String> mBakerSliderImageUrls;
    private String mBakerId, mBakerImageUrl, mBakerName, mBakerAddress, mBakerMobileNumber, mBakerEmail, mFavoriteId;
    private boolean mBakerIsFavorite;
    private double mBakerRating, mBakerDistance;

    private boolean mIsContactInfoCollapsed = true, mIsDeliveryInfoCollapsed = true, mIsPaymentInfoCollapsed = true,
            mIsOrderDataToBeLoadedFromServer = false;

    private static final String LOG_TAG = OrderDetailsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        initViews();

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(OrderDetailsActivity.this, R.drawable.ic_arrow_back_white));

        mOrderImageUrls = new ArrayList<>();
        mBakerSliderImageUrls = new ArrayList<>();
        onNewIntent(getIntent());
        prepareOrderData(getIntent());
        mCakeImageAdapter = new CakeImageAdapter(this, mOrderImageUrls);
        mRecyclerViewOrderImagesItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewOrderImagesItems.setAdapter(mCakeImageAdapter);

        mExpandableTextViewDescription.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/montserrat_semibold.ttf"));

        mExpandableTextViewDescription.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongClickDescription();
                return true;
            }
        });

        mTextViewEmptyViewTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTextViewEmptyViewTextButton.getText().toString().equalsIgnoreCase(getString(R.string.shared_try_again))) {
                    mTextViewEmptyViewTextButton.setVisibility(View.GONE);
                    getOrderDataFromServer();
                }
            }
        });

        mRelativeLayoutContactInfo.setOnClickListener(this);
        mRelativeLayoutPaymentInfo.setOnClickListener(this);
        mRelativeLayoutDeliveryInfo.setOnClickListener(this);
        mRelativeLayoutBakerInfo.setOnClickListener(this);
        mButtonCancel.setOnClickListener(this);
        mButtonConfirm.setOnClickListener(this);
        mButtonReportAProblem.setOnClickListener(this);
        mButtonViewReason.setOnClickListener(this);
        mTextViewRateThisOrder.setOnClickListener(this);
    }

    /*
     *
     * Overridden methods
     */

    public void onBackPressed() {
        onClickBack();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_order_details, menu);
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_REJECTED) ||
                mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ONGOING) ||
                mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_COMPLETED) ||
                mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CANCELLED)) {
            MenuItem menuItemReOrder = menu.findItem(R.id.action_re_order);
            menuItemReOrder.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onClickBack();
                return true;
            case R.id.action_re_order:
                Intent intent = new Intent(OrderDetailsActivity.this, PreviewImagesActivity.class);
                intent.putExtra(Constants.EXTRA_KEY_TITLE_PREVIEW_IMAGES_ACTIVITY, getString(R.string.activity_title_preview_images));
                intent.putExtra(Constants.EXTRA_KEY_IS_PREVIEW_IMAGES_OPEN_FROM_UNITY, false);
                intent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, (ArrayList<String>) mOrderImageUrls);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        prepareOrderData(intent);
    }

    @Override
    public void onClick(View view) {
        if (view == mRelativeLayoutContactInfo) {
            if (mIsContactInfoCollapsed) {
                // Expand
                mIsContactInfoCollapsed = false;
                mImageViewArrowContact.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_up));
                Utils.expand(mLinearLayoutContactInfo, 3);
            } else {
                // Collapse
                mIsContactInfoCollapsed = true;
                mImageViewArrowContact.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
                Utils.collapse(mLinearLayoutContactInfo, 3);
            }
        }
        if (view == mRelativeLayoutPaymentInfo) {
            if (mIsPaymentInfoCollapsed) {
                // Expand
                mIsPaymentInfoCollapsed = false;
                mImageViewArrowPayment.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_up));
                Utils.expand(mLinearLayoutPaymentInfo, 3);
            } else {
                // Collapse
                mIsPaymentInfoCollapsed = true;
                mImageViewArrowPayment.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
                Utils.collapse(mLinearLayoutPaymentInfo, 3);
            }
        }
        if (view == mRelativeLayoutDeliveryInfo) {
            if (mIsDeliveryInfoCollapsed) {
                // Expand
                mIsDeliveryInfoCollapsed = false;
                mImageViewArrowDelivery.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_up));
                Utils.expand(mLinearLayoutDeliveryInfo, 3);
            } else {
                // Collapse
                mIsDeliveryInfoCollapsed = true;
                mImageViewArrowDelivery.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_arrow_down));
                Utils.collapse(mLinearLayoutDeliveryInfo, 3);
            }
        }
        if (view == mRelativeLayoutBakerInfo) {
            Intent bakerIntent = new Intent(OrderDetailsActivity.this, BakerProfileActivity.class);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_ID, mBakerId);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_NAME, mBakerName);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_IMAGE, mBakerImageUrl);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_RATING, mBakerRating);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_DISTANCE, mBakerDistance);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_ADDRESS, mBakerAddress);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_MOBILE_NUMBER, mBakerMobileNumber);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_EMAIL, mBakerEmail);
            bakerIntent.putStringArrayListExtra(Constants.EXTRA_KEY_BAKER_SLIDER_IMAGES, (ArrayList<String>) mBakerSliderImageUrls);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_ACTIVE_STATUS, mActiveStatus);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_FAVORITE_ID, mFavoriteId);
            bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_IS_FAVORITE, mBakerIsFavorite);
            startActivity(bakerIntent);
        }
        if (view == mButtonCancel) {
            mSubtotalStr = String.format("%.0f", mSubtotal);
            mDeliveryChargesStr = String.format("%.0f", mDeliveryCharges);
            mTotalAmountStr = String.format("%.0f", mTotalAmount);
            updateOrderStatus(ApiConstants.PARAM_VALUE_CANCELLED);
        }
        if (view == mButtonConfirm) {
            mSubtotalStr = String.format("%.0f", mSubtotal);
            mDeliveryChargesStr = String.format("%.0f", mDeliveryCharges);
            mTotalAmountStr = String.format("%.0f", mTotalAmount);
            updateOrderStatus(ApiConstants.PARAM_VALUE_CONFIRMED);
        }
        if (view == mButtonReportAProblem) {
            Intent intent = new Intent(OrderDetailsActivity.this, HelpActivity.class);
            intent.putExtra(Constants.EXTRA_KEY_IS_HELP_OPENED_FOR_REPORTING, true);
            intent.putExtra(Constants.EXTRA_KEY_BAKER_ID, mBakerId);
            startActivity(intent);
        }
        if (view == mButtonViewReason) {
            Utils.showCustomOkAlertDialog(OrderDetailsActivity.this, "Rejected reason", mRejectedReason);
        }
        if (view == mTextViewRateThisOrder) {
            Intent intent = new Intent(OrderDetailsActivity.this, RateOrderActivity.class);
            intent.putExtra(Constants.EXTRA_KEY_BAKER_IMAGE, mBakerImageUrl);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_DATE, mTextViewDate.getText().toString().trim());
            intent.putExtra(Constants.EXTRA_KEY_BAKER_NAME, mBakerName);
            intent.putExtra(Constants.EXTRA_KEY_BAKER_ID, mBakerId);
            intent.putExtra(Constants.EXTRA_KEY_ORDER_ID, mOrderId);
            startActivity(intent);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        // Empty view
        mRelativeLayoutRootEmptyView = (RelativeLayout) findViewById(R.id.relative_layout_root_empty_view);
        mRelativeLayoutEmptyView = (RelativeLayout) findViewById(R.id.relative_layout_empty_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mTextViewEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        mTextViewEmptyViewTextButton = (TextView) findViewById(R.id.tv_empty_view_text_button);

        // Cake info views
        mRecyclerViewOrderImagesItems = (RecyclerView) findViewById(R.id.rv_order_images_items);
        mTextViewDate = (TextView) findViewById(R.id.tv_order_date);
        mTextViewStatus = (TextView) findViewById(R.id.tv_order_status);
        mTextViewId = (TextView) findViewById(R.id.tv_order_id);
        mTextViewQuantity = (TextView) findViewById(R.id.tv_quantity);
        mTextViewPounds = (TextView) findViewById(R.id.tv_pounds);
        mExpandableTextViewDescription = (ExpandableTextView) findViewById(R.id.etv_order_description);

        // Contact info views
        mRelativeLayoutContactInfo = (RelativeLayout) findViewById(R.id.relative_layout_contact_info);
        mLinearLayoutContactInfo = (LinearLayout) findViewById(R.id.linear_layout_contact_info);
        mTextViewContactNumberCollapsed = (TextView) findViewById(R.id.tv_contact_number_collapsed);
        mImageViewArrowContact = (ImageView) findViewById(R.id.img_arrow_contact);
        mTextViewName = (TextView) findViewById(R.id.tv_name);
        mTextViewEmailAddress = (TextView) findViewById(R.id.tv_email_address);
        mTextViewContactNumberExpanded = (TextView) findViewById(R.id.tv_contact_number_expanded);

        // Payment info views
        mLinearLayoutPaymentInfoRootContainer = (LinearLayout) findViewById(R.id.linear_layout_payment_info_root_container);
        mRelativeLayoutPaymentInfo = (RelativeLayout) findViewById(R.id.relative_layout_payment_info);
        mTextViewAmountCollapsed = (TextView) findViewById(R.id.tv_order_amount_collapsed);
        mImageViewArrowPayment = (ImageView) findViewById(R.id.img_arrow_payment);
        mLinearLayoutPaymentInfo = (LinearLayout) findViewById(R.id.linear_layout_payment_info);
        mTextViewSubtotal = (TextView) findViewById(R.id.tv_order_subtotal);
        mTextViewDeliveryCharges = (TextView) findViewById(R.id.tv_order_delivery_charges);
        mTextViewAmountExpanded = (TextView) findViewById(R.id.tv_order_amount_expanded);
        mImageViewPaymentMethod = (ImageView) findViewById(R.id.img_payment_method);

        // Delivery info views
        mRelativeLayoutDeliveryInfo = (RelativeLayout) findViewById(R.id.relative_layout_delivery_info);
        mTextViewDeliveryDateCollapsed = (TextView) findViewById(R.id.tv_order_delivery_date_collapsed);
        mImageViewArrowDelivery = (ImageView) findViewById(R.id.img_arrow_delivery);
        mLinearLayoutDeliveryInfo = (LinearLayout) findViewById(R.id.linear_layout_delivery_info);
        mTextViewDeliveryLocation = (TextView) findViewById(R.id.tv_order_delivery_location);
        mTextViewDeliveryAddress = (TextView) findViewById(R.id.tv_order_delivery_address);
        mTextViewDeliveryDateExpanded = (TextView) findViewById(R.id.tv_order_delivery_date_expanded);
        mTextViewDeliveryTime = (TextView) findViewById(R.id.tv_order_delivery_time);

        // Baker info views
        mRelativeLayoutBakerInfo = (RelativeLayout) findViewById(R.id.relative_layout_baker_info);
        mCircleImageViewBakerImg = (CircleImageView) findViewById(R.id.civ_baker_image);
        mImageViewOnline = (ImageView) findViewById(R.id.img_online);
        mTextViewBakerName = (TextView) findViewById(R.id.tv_baker_name);
        mLinearLayoutOrderRating = (LinearLayout) findViewById(R.id.linear_layout_order_rating);
        mMaterialRatingBarOrderRating = (MaterialRatingBar) findViewById(R.id.mrb_order_rating);
        mTextViewBakerAddress = (TextView) findViewById(R.id.tv_baker_address);

        // Other views
        mLinearLayoutButtonsConfirmCancel = (LinearLayout) findViewById(R.id.linear_layout_buttons_cancel_confirm);
        mLinearLayoutButtonReportAProblem = (LinearLayout) findViewById(R.id.linear_layout_button_report_a_problem);
        mLinearLayoutButtonViewReason = (LinearLayout) findViewById(R.id.linear_layout_button_view_reason);
        mButtonCancel = (Button) findViewById(R.id.btn_cancel);
        mButtonConfirm = (Button) findViewById(R.id.btn_confirm);
        mFrameLayoutButtonConfirm = (FrameLayout) findViewById(R.id.frame_layout_btn_confirm);
        mButtonReportAProblem = (Button) findViewById(R.id.btn_report_a_problem);
        mTextViewRateThisOrder = (TextView) findViewById(R.id.tv_rate_this_order);
        mButtonViewReason = (Button) findViewById(R.id.btn_view_reason);
        mScrollView = (ScrollView) findViewById(R.id.sv_scrollview);
    }

    private void onClickBack() {
        Log.d(LOG_TAG, "mIsOrderDataToBeLoadedFromServer: " + mIsOrderDataToBeLoadedFromServer);
        if (mIsOrderDataToBeLoadedFromServer == true) {
            ActivityCompat.finishAffinity(OrderDetailsActivity.this);
            startActivity(new Intent(OrderDetailsActivity.this, MainActivity.class));
        } else {
            finish();
        }
    }

    private void setOrderData() {
        String formattedDateStr = "";
        if (DateTimeUtils.isToday(mDate)) {
            formattedDateStr = "Today";
        } else if (DateTimeUtils.isYesterday(mDate)) {
            formattedDateStr = "Yesterday";
        } else {
            formattedDateStr = new SimpleDateFormat("dd MMM, yyyy").format(mDate);
        }
        formattedDateStr += " • " + new SimpleDateFormat("hh:mm a").format(mDate);
        mTextViewDate.setText(formattedDateStr);

        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_PENDING)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_PENDING,
                    R.color.colorOrderStatusPending, R.drawable.order_status_pending_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_REJECTED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_REJECTED,
                    R.color.colorOrderStatusRejected, R.drawable.order_status_rejected_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ACCEPTED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_ACCEPTED,
                    R.color.colorOrderStatusAccepted, R.drawable.order_status_accepted_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CANCELLED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_CANCELLED,
                    R.color.colorOrderStatusCancelled, R.drawable.order_status_cancelled_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CONFIRMED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_CONFIRMED,
                    R.color.colorOrderStatusConfirmed, R.drawable.order_status_confirmed_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ONGOING)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_ONGOING,
                    R.color.colorOrderStatusOngoing, R.drawable.order_status_ongoing_drawable);
        } else if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_COMPLETED)) {
            Utils.setUpOrderStatus(this, mTextViewStatus, ApiConstants.PARAM_VALUE_COMPLETED,
                    R.color.colorOrderStatusCompleted, R.drawable.order_status_completed_drawable);
        }
        mTextViewId.setText("ORDER # " + mOrderId);
        mTextViewQuantity.setText(Html.fromHtml("<font color='#9b9b9b'>Quantity</font>"
                + "<br><font color='#212121'>" + mQuantity + "</font>"));
        mTextViewPounds.setText(Html.fromHtml("<font color='#9b9b9b'>Pounds</font>"
                + "<br><font color='#212121'>" + mPounds + "</font>"));
        if (!Utils.isStringEmptyOrNull(mDescription)) {
            String transformedDescription = mDescription.replaceAll("\n", "<br>");
            mExpandableTextViewDescription.setText(Html.fromHtml("<font color='#9b9b9b'>Description</font>"
                    + "<br><font color='#212121'>" + transformedDescription + "</font>"));
        } else {
            mExpandableTextViewDescription.setVisibility(View.GONE);
            mExpandableTextViewDescription.setText("");
        }

        // contact info
        mTextViewContactNumberCollapsed.setText(mContactNumber);
        mTextViewName.setText(SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_FIRST_NAME) + " " + SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_LAST_NAME));
        mTextViewEmailAddress.setText(SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_EMAIL));
        mTextViewContactNumberExpanded.setText(mContactNumber);

        String formattedDeliveryDateStr = "";
        long deliveryDate = DateTimeUtils.convertStringDateToLong("dd MMM, yyyy", mDeliveryDate);
        if (DateTimeUtils.isToday(deliveryDate)) {
            formattedDeliveryDateStr = "Today";
        } else if (DateTimeUtils.isYesterday(deliveryDate)) {
            formattedDeliveryDateStr = "Yesterday";
        } else if (DateTimeUtils.isTomorrow(deliveryDate)) {
            formattedDeliveryDateStr = "Tomorrow";
        } else {
            formattedDeliveryDateStr = mDeliveryDate;
        }
        mTextViewDeliveryDateCollapsed.setText(formattedDeliveryDateStr);
        mTextViewDeliveryLocation.setText(mDeliveryLocation);
        mTextViewDeliveryAddress.setText(mDeliveryAddress);
        mTextViewDeliveryDateExpanded.setText(formattedDeliveryDateStr);
        mTextViewDeliveryTime.setText(mDeliveryTime);

        mTextViewAmountCollapsed.setText("PKR " + String.format("%.0f", mTotalAmount) + "");
        mTextViewSubtotal.setText(String.format("%.0f", mSubtotal) + "");
        mTextViewDeliveryCharges.setText(String.format("%.0f", mDeliveryCharges) + "");
        mTextViewAmountExpanded.setText("PKR " + String.format("%.0f", mTotalAmount) + "");

        if (mPaymentMethod.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD)) {
            mImageViewPaymentMethod.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_card_2));
        } else if (mPaymentMethod.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CASH)) {
            mImageViewPaymentMethod.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_cash));
        }

        if (mBakerImageUrl != null) {
            Log.v(LOG_TAG, mBakerImageUrl);
            Picasso.get().load(mBakerImageUrl).fit().placeholder(R.drawable.ic_default_profile_pic)
                    .into(mCircleImageViewBakerImg);
        } else {
            Picasso.get().load(R.drawable.ic_default_profile_pic).placeholder(R.drawable.ic_default_profile_pic)
                    .into(mCircleImageViewBakerImg);
        }
        if (mActiveStatus.equals("1") || mActiveStatus.equalsIgnoreCase("Online")) {
            mImageViewOnline.setVisibility(View.VISIBLE);
        }
        mTextViewBakerName.setText(mBakerName);
        mTextViewBakerAddress.setText(mBakerAddress);

        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_PENDING) ||
                mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CANCELLED) ||
                mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CONFIRMED)) {

            if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_PENDING)) {
                mLinearLayoutPaymentInfoRootContainer.setVisibility(View.GONE);
                if (DateTimeUtils.getMinutesDifference(mDate) <= 60) {
                    mLinearLayoutButtonsConfirmCancel.setVisibility(View.VISIBLE);
                } else {
                    hideMarginBottomScrollView();
                }
                mFrameLayoutButtonConfirm.setVisibility(View.GONE);
            } else {
                hideMarginBottomScrollView();
            }
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_REJECTED)) {
            mLinearLayoutButtonViewReason.setVisibility(View.VISIBLE);
            mTextViewAmountExpanded.setTextColor(ContextCompat.getColor(this, R.color.colorOrderStatusRejected));
            mTextViewAmountCollapsed.setTextColor(ContextCompat.getColor(this, R.color.colorOrderStatusRejected));
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_COMPLETED)) {
            mLinearLayoutOrderRating.setVisibility(View.VISIBLE);
            mMaterialRatingBarOrderRating.setRating((float) mOrderRating);

            // If not rated
            if (mRatedStatus.equals("0")) {
                mTextViewRateThisOrder.setVisibility(View.VISIBLE);
            }
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ACCEPTED)) {
            mLinearLayoutButtonsConfirmCancel.setVisibility(View.VISIBLE);
        }
        if (mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ONGOING) || mStatus.equalsIgnoreCase(ApiConstants.PARAM_VALUE_COMPLETED)) {
            mLinearLayoutButtonReportAProblem.setVisibility(View.VISIBLE);
        }
        invalidateOptionsMenu();
    }

    private void hideMarginBottomScrollView() {
        // Margin bottom for scrollview as there no bottom button
        final float scale = getResources().getDisplayMetrics().density;
        // convert the DP into pixel
        int topPixel = (int) (56 * scale + 0.5f);
        ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) mScrollView.getLayoutParams();
        p.setMargins(0, topPixel, 0, 0);
        mScrollView.setLayoutParams(p);
    }

    private void prepareOrderData(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mOrderId = extras.getString(Constants.EXTRA_KEY_ORDER_ID);
            mIsOrderDataToBeLoadedFromServer = extras.getBoolean(Constants.EXTRA_KEY_ORDER_DATA_LOAD_FROM_SERVER);

            if (extras.getBoolean(Constants.EXTRA_KEY_ORDER_DATA_LOAD_FROM_SERVER) == false) {
                mIsOrderDataToBeLoadedFromServer = false;
                mOrderImageUrls = extras.getStringArrayList(Constants.EXTRA_KEY_ORDER_IMAGES);
                mDate = extras.getLong(Constants.EXTRA_KEY_ORDER_DATE);
                mStatus = extras.getString(Constants.EXTRA_KEY_ORDER_STATUS);
                mQuantity = extras.getString(Constants.EXTRA_KEY_ORDER_QUANTITY);
                mPounds = extras.getString(Constants.EXTRA_KEY_ORDER_POUNDS);
                mDescription = extras.getString(Constants.EXTRA_KEY_ORDER_DESCRIPTION);
                mContactNumber = extras.getString(Constants.EXTRA_KEY_ORDER_CONTACT_NUMBER);
                mDeliveryLocation = extras.getString(Constants.EXTRA_KEY_ORDER_DELIVERY_LOCATION);
                mDeliveryAddress = extras.getString(Constants.EXTRA_KEY_ORDER_DELIVERY_ADDRESS);
                mDeliveryDate = extras.getString(Constants.EXTRA_KEY_ORDER_DELIVERY_DATE);
                mDeliveryTime = extras.getString(Constants.EXTRA_KEY_ORDER_DELIVERY_TIME);
                mPaymentMethod = extras.getString(Constants.EXTRA_KEY_ORDER_PAYMENT_METHOD);
                mRejectedReason = extras.getString(Constants.EXTRA_KEY_ORDER_REJECTED_REASON);
                mOrderRating = extras.getDouble(Constants.EXTRA_KEY_ORDER_ORDER_RATING);
                mRatedStatus = extras.getString(Constants.EXTRA_KEY_ORDER_RATED_STATUS);
                mSubtotal = extras.getDouble(Constants.EXTRA_KEY_ORDER_SUBTOTAL);
                mDeliveryCharges = extras.getDouble(Constants.EXTRA_KEY_ORDER_DELIVERY_CHARGES);
                mTotalAmount = extras.getDouble(Constants.EXTRA_KEY_ORDER_AMOUNT);
                mBakerId = extras.getString(Constants.EXTRA_KEY_BAKER_ID);
                mBakerImageUrl = extras.getString(Constants.EXTRA_KEY_BAKER_IMAGE);
                mBakerName = extras.getString(Constants.EXTRA_KEY_BAKER_NAME);
                mBakerAddress = extras.getString(Constants.EXTRA_KEY_BAKER_ADDRESS);
                mBakerRating = extras.getDouble(Constants.EXTRA_KEY_BAKER_RATING);
                mBakerMobileNumber = extras.getString(Constants.EXTRA_KEY_BAKER_MOBILE_NUMBER);
                mBakerEmail = extras.getString(Constants.EXTRA_KEY_BAKER_EMAIL);
                mBakerDistance = extras.getDouble(Constants.EXTRA_KEY_BAKER_DISTANCE);
                mBakerSliderImageUrls = extras.getStringArrayList(Constants.EXTRA_KEY_BAKER_SLIDER_IMAGES);
                mActiveStatus = extras.getString(Constants.EXTRA_KEY_BAKER_ACTIVE_STATUS);
                mBakerIsFavorite = extras.getBoolean(Constants.EXTRA_KEY_BAKER_IS_FAVORITE);
                mFavoriteId = extras.getString(Constants.EXTRA_KEY_BAKER_FAVORITE_ID);
                setOrderData();
            } else {
                mIsOrderDataToBeLoadedFromServer = true;
                getOrderDataFromServer();
            }
        }
    }

    private void showEmptyView(int textButtonVisibility, int progressBarVisibility, String textViewText) {
        mRelativeLayoutRootEmptyView.setVisibility(View.VISIBLE);
        mTextViewEmptyViewTextButton.setVisibility(textButtonVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mTextViewEmptyView.setText(textViewText);
    }

    private void toggleMainDataViews(int visibility) {
        mScrollView.setVisibility(visibility);
    }

    private void hideEmptyView() {
        mRelativeLayoutRootEmptyView.setVisibility(View.GONE);
    }

    private void getOrderDataFromServer() {
        toggleMainDataViews(View.GONE);
        showEmptyView(View.GONE, View.VISIBLE, "");

        if (!Utils.isNetworkAvailable(this)) {
            showEmptyView(View.VISIBLE, View.GONE, getString(R.string.error_no_internet_connection));
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                final String loggedInUserId = SharedPrefSingleton.getInstance(OrderDetailsActivity.this)
                        .getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID);

                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_GET_SINGLE_ORDER, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideEmptyView();
                        mProgressBar.setVisibility(View.GONE);

                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String responseStatus = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (responseStatus.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                // Get the json array as a response
                                JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);
                                JSONObject orderJsonObject = responseArray.getJSONObject(0);

                                // Get order details of order json object
                                String image1 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE1);
                                String image2 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE2);
                                String image3 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE3);
                                String image4 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE4);
                                String image5 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE5);
                                String image6 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE6);
                                String image7 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE7);
                                String image8 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE8);
                                String image9 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE9);
                                String image10 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE10);
                                mDate = DateTimeUtils.convertStringDateToLong("yyyy-MM-dd HH:mm:ss", orderJsonObject.getString(ApiConstants.PARAM_CREATED_AT));
                                mStatus = Utils.getOrderStatusByOrderStatusCode(orderJsonObject.getString(ApiConstants.PARAM_STATUS));
                                mQuantity = orderJsonObject.getString(ApiConstants.PARAM_QUANTITY);
                                mPounds = orderJsonObject.getString(ApiConstants.PARAM_POUNDS);
                                mDescription = orderJsonObject.getString(ApiConstants.PARAM_DESCRIPTION);
                                mContactNumber = orderJsonObject.getString(ApiConstants.PARAM_CONTACT_NUMBER);
                                mDeliveryLocation = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_LOCATION);
                                mDeliveryAddress = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_ADDRESS);
                                mDeliveryDate = DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd", "dd MMM, yyyy", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_DATE));
                                mDeliveryTime = DateTimeUtils.getFormattedDateTimeString("HH:mm:ss", "hh:mm a", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_TIME));
                                mSubtotal = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL));
                                mDeliveryCharges = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES));
                                mTotalAmount = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT));
                                mPaymentMethod = (orderJsonObject.getString(ApiConstants.PARAM_PAYMENT_METHOD).equals("1"))
                                        ? ApiConstants.PARAM_VALUE_CASH : ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD;
                                mRejectedReason = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON))
                                        ? "The baker didn't provided the reason for rejecting your order." : orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON);
                                mOrderRating = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING));
                                mRatedStatus = orderJsonObject.getString(ApiConstants.PARAM_RATED_STATUS);
                                mBakerId = orderJsonObject.getString(ApiConstants.PARAM_BAKER_ID);
                                mBakerImageUrl = orderJsonObject.getString(ApiConstants.PARAM_IMAGE);
                                String fn = orderJsonObject.getString(ApiConstants.PARAM_FIRST_NAME);
                                String ln = orderJsonObject.getString(ApiConstants.PARAM_LAST_NAME);
                                mBakerName = fn + " " + ln;
                                mBakerAddress = orderJsonObject.getString(ApiConstants.PARAM_LOCATION_NAME) + "\n" + orderJsonObject.getString(ApiConstants.PARAM_LOCATION_ADDRESS);
                                mBakerRating = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_RATING))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_RATING));
                                mBakerMobileNumber = orderJsonObject.getString(ApiConstants.PARAM_MOBILE_NUMBER);
                                mBakerEmail = orderJsonObject.getString(ApiConstants.PARAM_EMAIL);
                                mBakerSliderImageUrls.add(orderJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE1));
                                mBakerSliderImageUrls.add(orderJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE2));
                                mBakerSliderImageUrls.add(orderJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE3));

                                double bakerLatitude = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE));
                                double bakerLongitude = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE));
                                mActiveStatus = orderJsonObject.getString(ApiConstants.PARAM_ACTIVE_STATUS);
                                mFavoriteId = orderJsonObject.getString(ApiConstants.PARAM_FAVORITE_ID);
                                String userIds = orderJsonObject.getString(ApiConstants.PARAM_USER_IDS);

                                if (!Utils.isStringEmptyOrNull(image1)) {
                                    mOrderImageUrls.add(image1);
                                }
                                if (!Utils.isStringEmptyOrNull(image2)) {
                                    mOrderImageUrls.add(image2);
                                }
                                if (!Utils.isStringEmptyOrNull(image3)) {
                                    mOrderImageUrls.add(image3);
                                }
                                if (!Utils.isStringEmptyOrNull(image4)) {
                                    mOrderImageUrls.add(image4);
                                }
                                if (!Utils.isStringEmptyOrNull(image5)) {
                                    mOrderImageUrls.add(image5);
                                }
                                if (!Utils.isStringEmptyOrNull(image6)) {
                                    mOrderImageUrls.add(image6);
                                }
                                if (!Utils.isStringEmptyOrNull(image7)) {
                                    mOrderImageUrls.add(image7);
                                }
                                if (!Utils.isStringEmptyOrNull(image8)) {
                                    mOrderImageUrls.add(image8);
                                }
                                if (!Utils.isStringEmptyOrNull(image9)) {
                                    mOrderImageUrls.add(image9);
                                }
                                if (!Utils.isStringEmptyOrNull(image10)) {
                                    mOrderImageUrls.add(image10);
                                }

                                if (!(Globals.sUserCurrentLocation.getLatitude() <= 0 || Globals.sUserCurrentLocation.getLongitude() <= 0)) {
                                    // Calculate the circular distance between user current and baker location
                                    mBakerDistance = Utils.calculateCircularDistanceInKilometers(
                                            Globals.sUserCurrentLocation.getLatitude(),
                                            Globals.sUserCurrentLocation.getLongitude(),
                                            bakerLatitude,
                                            bakerLongitude
                                    );
                                }

                                mBakerIsFavorite = (userIds.contains(loggedInUserId)) ? true : false;

                                hideEmptyView();
                                toggleMainDataViews(View.VISIBLE);
                                setOrderData();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        toggleMainDataViews(View.GONE);
                        showEmptyView(View.VISIBLE, View.GONE, "Sorry, something went wrong. Please try again.");
                        if (error != null) {
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        HashMap<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ORDER_ID, mOrderId);
                        params.put(ApiConstants.PARAM_ORDER_FOR, ApiConstants.PARAM_VALUE_USER);
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(OrderDetailsActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }

    private void updateOrderStatus(final String statusAction) {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.setMessage("Processing...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_UPDATE_ORDER_STATUS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);
                            String responseMsg = rootJsonObject.getString(ApiConstants.KEY_RESPONSE);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                String toastMsg = "[" + mTextViewDate.getText().toString() + "] ";
                                if (statusAction.equals(ApiConstants.PARAM_VALUE_CONFIRMED)) {
                                    toastMsg += "Order confirmed successfully.";
                                } else if (statusAction.equals(ApiConstants.PARAM_VALUE_CANCELLED)) {
                                    toastMsg += "Order cancelled successfully.";
                                }
                                Toast.makeText(OrderDetailsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                                ActivityCompat.finishAffinity(OrderDetailsActivity.this);
                                startActivity(new Intent(OrderDetailsActivity.this, MainActivity.class));
                            } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                Toast.makeText(OrderDetailsActivity.this, responseMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(OrderDetailsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ORDER_ID, mOrderId);
                        params.put(ApiConstants.PARAM_USER_ID, SharedPrefSingleton.getInstance(OrderDetailsActivity.this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID));
                        params.put(ApiConstants.PARAM_BAKER_ID, mBakerId);
                        params.put(ApiConstants.PARAM_SUBTOTAL, mSubtotalStr);
                        params.put(ApiConstants.PARAM_DELIVERY_CHARGES, mDeliveryChargesStr);
                        params.put(ApiConstants.PARAM_TOTAL_AMOUNT, mTotalAmountStr);
                        params.put(ApiConstants.PARAM_REJECTED_REASON, "null");
                        params.put(ApiConstants.PARAM_STATUS, statusAction);
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(OrderDetailsActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }

    private void onLongClickDescription() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog, null);
        final Builder builder = new Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.copy_alert_dialog_button), null);

        TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setText(mDescription);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData = ClipData.newPlainText("order_description", mDescription);
                Toast.makeText(OrderDetailsActivity.this, "Description copied to clipboard.", Toast.LENGTH_SHORT).show();
                clipboardManager.setPrimaryClip(clipData);
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        alertDialog.getButton(BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }
}

package com.android.wishacake.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.adapters.SliderAdapter;
import com.android.wishacake.fragments.AboutBakerProfileFragment;
import com.android.wishacake.fragments.ReviewsBakerProfileFragment;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.utilities.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class BakerProfileActivity extends AppCompatActivity {

    private AppBarLayout mAppBarLayout;
    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private Toolbar mToolbar;
    private ViewPager mViewPagerSlider, mViewPagerBakerProfile;
    private LinearLayout mLinearLayoutDotIndicator;

    public static TabLayout sTabLayoutBakerProfile;

    private CircleImageView mCircleImageViewBakerImg;
    private ImageView mImageViewOnline;
    private TextView mTextViewBakerName, mTextViewBakerRating, mTextViewBakerDistance;

    private LinearLayout mLinearLayoutRequestAnOrder;
    private Button mButtonRequestAnOrder;

    MenuItem mMenuItemFavoriteUnfavorite;

    private SliderAdapter mSliderAdapter;
    private ImageView[] mImageViewsDots;
    private int mDotCounts = 0;

    private String mBakerImageUrl, mBakerName, mActiveStatus, mFavoriteId;
    private List<String> mBakerSliderImageUrls, mOrderImages;
    private boolean mBakerIsFavorite;

    private ProgressDialog mProgressDialog;

    public static String sBakerId, sBakerAddress, sBakerMobileNumber, sBakerEmail;

    private static final String LOG_TAG = BakerProfileActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setTheme(R.style.AppTheme_NoActionBar_Transparent);
        }
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_baker_profile);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_white));
        getSupportActionBar().setTitle("");

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);

        setUpBakerData();
        setUpSlider();
        setUpSliderDots();
        mViewPagerSlider.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int pos) {
                for (int i = 0; i < mDotCounts; i++) {
                    mImageViewsDots[i].setImageDrawable(ContextCompat.getDrawable(BakerProfileActivity.this, R.drawable.inactive_dot_indicator));
                }
                mImageViewsDots[pos].setImageDrawable(ContextCompat.getDrawable(BakerProfileActivity.this, R.drawable.active_dot_indicator));
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    toggleCollapseToolbarTitle(mBakerName, true);
                } else if (verticalOffset == 0) {
                    // Expanded
                    toggleCollapseToolbarTitle("", false);
                } else {
                    // Somewhere in between
                    toggleCollapseToolbarTitle("", false);
                }
            }
        });

        BakerProfileViewPagerAdapter bakerProfileViewPagerAdapter = new BakerProfileViewPagerAdapter(getSupportFragmentManager());
        mViewPagerBakerProfile.setAdapter(bakerProfileViewPagerAdapter);
        mViewPagerBakerProfile.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(sTabLayoutBakerProfile));
        sTabLayoutBakerProfile.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPagerBakerProfile));

        mOrderImages = getIntent().getExtras().getStringArrayList(Constants.EXTRA_KEY_ORDER_IMAGES);

        if (mOrderImages == null || mOrderImages.size() <= 0 || mOrderImages.isEmpty()) {
            mLinearLayoutRequestAnOrder.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
            mButtonRequestAnOrder.setEnabled(false);
        }

        mButtonRequestAnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (mOrderImages == null || mOrderImages.size() <= 0 || mOrderImages.isEmpty()) {
//                    Toast.makeText(BakerProfileActivity.this, "No cake images attached.", Toast.LENGTH_SHORT).show();
//                    return;
//                }
                onClickRequestAnOrder();
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_baker_profile, menu);
        mMenuItemFavoriteUnfavorite = menu.findItem(R.id.action_favorite_unfavorite);
        toggleMenuItem();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_favorite_unfavorite:
                onClickFavoriteUnfavoriteMenuItem();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUpSlider();
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapse_toolbar);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        sTabLayoutBakerProfile = (TabLayout) findViewById(R.id.tl_baker_profile);
        mViewPagerSlider = (ViewPager) findViewById(R.id.vp_slider);
        mViewPagerBakerProfile = (ViewPager) findViewById(R.id.vp_baker_profile);
        mLinearLayoutDotIndicator = (LinearLayout) findViewById(R.id.linear_layout_dot_indicator);
        mCircleImageViewBakerImg = (CircleImageView) findViewById(R.id.civ_baker_image);
        mImageViewOnline = (ImageView) findViewById(R.id.img_online);
        mTextViewBakerName = (TextView) findViewById(R.id.tv_baker_name);
        mTextViewBakerRating = (TextView) findViewById(R.id.tv_baker_rating);
        mTextViewBakerDistance = (TextView) findViewById(R.id.tv_baker_distance);
        mLinearLayoutRequestAnOrder = (LinearLayout) findViewById(R.id.linear_layout_request_an_order);
        mButtonRequestAnOrder = (Button) findViewById(R.id.btn_request_an_order);

        Typeface customFont = Typeface.createFromAsset(getAssets(), "fonts/montserrat_semibold.ttf");
        mCollapsingToolbarLayout.setCollapsedTitleTypeface(customFont);
        mCollapsingToolbarLayout.setExpandedTitleTypeface(customFont);
    }

    private void onClickFavoriteUnfavoriteMenuItem() {
        if (!mBakerIsFavorite) {
            if (Utils.isBakerFavoriteStatusUpdated(this, mFavoriteId, mBakerName, ApiConstants.PARAM_VALUE_FAVORITE)) {
                mBakerIsFavorite = true;
                toggleMenuItem();
            }
        } else {
            View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog, null);
            final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
            builder.setView(dialogView);
            builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
            builder.setPositiveButton(getString(R.string.bakers_remove_alert_dialog_button), null);

            TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
            textViewMsg.setText("Are you sure you want to remove \"" + mBakerName + "\" from your favorite bakers?");

            final AlertDialog alertDialog = builder.create();
            alertDialog.setCancelable(true);

            alertDialog.show();

            /*
             *
             * Following code must be called after show() method of alert dialog
             */

            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDialog.dismiss();
                }
            });

            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Utils.isBakerFavoriteStatusUpdated(BakerProfileActivity.this, mFavoriteId, mBakerName, ApiConstants.PARAM_VALUE_UNFAVORITE)) {
                        alertDialog.dismiss();
                        mBakerIsFavorite = false;
                        toggleMenuItem();
                    }
                }
            });

            alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
            alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        }
    }

    private void toggleMenuItem() {
        if (mBakerIsFavorite) {
            mMenuItemFavoriteUnfavorite.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_favorite_filled_white));
            mMenuItemFavoriteUnfavorite.setTitle("Unfavorite");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mMenuItemFavoriteUnfavorite.setTooltipText("Unfavorite");
            }
        } else {
            mMenuItemFavoriteUnfavorite.setIcon(ContextCompat.getDrawable(this, R.drawable.ic_favorite_unfilled_white));
            mMenuItemFavoriteUnfavorite.setTitle("Favorite");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mMenuItemFavoriteUnfavorite.setTooltipText("Favorite");
            }
        }
    }

    private void setUpSlider() {
        Globals.sIsSliderFromMainActivity = false;
        mSliderAdapter = new SliderAdapter(this, mBakerSliderImageUrls);
        mViewPagerSlider.setAdapter(mSliderAdapter);
        mViewPagerSlider.setCurrentItem(0);
    }

    private void setUpSliderDots() {
        mDotCounts = mSliderAdapter.getCount();
        mImageViewsDots = new ImageView[mDotCounts];

        // Calculating width for dots becoz neither match_parent working nor wrap_content
        int width = 0;
        for (int i = 0; i < mDotCounts; i++) {
            // 4 is left margin and 4 is size of dot
            width += 8;
        }
        width += 4; // Right margin for last dot
        Log.v(LOG_TAG, "Width -> " + width + "");

        for (int i = 0; i < mDotCounts; i++) {
            mImageViewsDots[i] = new ImageView(this);
            mImageViewsDots[i].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.inactive_dot_indicator));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, 24);
            params.setMargins(4, 0, 4, 0);
            mLinearLayoutDotIndicator.addView(mImageViewsDots[i], params);
        }
        mImageViewsDots[0].setImageDrawable(ContextCompat.getDrawable(this, R.drawable.active_dot_indicator));
    }

    private void setUpBakerData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            sBakerId = extras.getString(Constants.EXTRA_KEY_BAKER_ID);
            mBakerName = extras.getString(Constants.EXTRA_KEY_BAKER_NAME);
            mBakerImageUrl = extras.getString(Constants.EXTRA_KEY_BAKER_IMAGE);
            double bakerRating = extras.getDouble(Constants.EXTRA_KEY_BAKER_RATING);
            double bakerDistance = extras.getDouble(Constants.EXTRA_KEY_BAKER_DISTANCE);
            sBakerAddress = extras.getString(Constants.EXTRA_KEY_BAKER_ADDRESS);
            sBakerMobileNumber = extras.getString(Constants.EXTRA_KEY_BAKER_MOBILE_NUMBER);
            sBakerEmail = extras.getString(Constants.EXTRA_KEY_BAKER_EMAIL);
            mBakerSliderImageUrls = extras.getStringArrayList(Constants.EXTRA_KEY_BAKER_SLIDER_IMAGES);
            mActiveStatus = extras.getString(Constants.EXTRA_KEY_BAKER_ACTIVE_STATUS);
            mBakerIsFavorite = extras.getBoolean(Constants.EXTRA_KEY_BAKER_IS_FAVORITE);
            mFavoriteId = extras.getString(Constants.EXTRA_KEY_BAKER_FAVORITE_ID);

            if (mBakerImageUrl != null) {
                if (mBakerImageUrl.trim().length() != 0) {
                    Log.v(LOG_TAG, mBakerImageUrl);
                    Picasso.get().load(mBakerImageUrl).fit().placeholder(R.drawable.ic_default_profile_pic)
                            .into(mCircleImageViewBakerImg);
                } else {
                    Picasso.get().load(R.drawable.ic_default_profile_pic).placeholder(R.drawable.ic_default_profile_pic)
                            .into(mCircleImageViewBakerImg);
                }
            } else {
                Picasso.get().load(R.drawable.ic_default_profile_pic).placeholder(R.drawable.ic_default_profile_pic)
                        .into(mCircleImageViewBakerImg);
            }
            if (mActiveStatus.equals("1") || mActiveStatus.equalsIgnoreCase("Online")) {
                mImageViewOnline.setVisibility(View.VISIBLE);
            }
            mTextViewBakerName.setText(mBakerName);
            if (bakerDistance <= 0) {
                mTextViewBakerDistance.setText("N/A");
            } else {
                mTextViewBakerDistance.setText(bakerDistance + " KM away");
            }
            if (bakerRating <= 0) {
                mTextViewBakerRating.setText("N/A");
            } else {
                mTextViewBakerRating.setText(bakerRating + "");
            }

            invalidateOptionsMenu(); // This will call onCreateOptionsMenu()
        }
    }

    private void toggleCollapseToolbarTitle(String title, boolean titleEnabled) {
        mCollapsingToolbarLayout.setTitle(title);
        mCollapsingToolbarLayout.setTitleEnabled(titleEnabled);
    }

    private void onClickRequestAnOrder() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                validateOrder();
                return null;
            }
        }.execute();
    }

    private void validateOrder() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_VALIDATE_ORDER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        Intent intent = new Intent(BakerProfileActivity.this, CheckoutActivity.class);
                        intent.putExtra(Constants.EXTRA_KEY_BAKER_NAME, mBakerName);
                        intent.putExtra(Constants.EXTRA_KEY_BAKER_ID, sBakerId);
                        intent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, (ArrayList<String>) mOrderImages);
                        startActivity(intent);
                    } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                        Utils.showCustomOkAlertDialog(BakerProfileActivity.this, "Oops!", rootJsonObject.getString(ApiConstants.KEY_RESPONSE));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(BakerProfileActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_USER_ID, SharedPrefSingleton.getInstance(BakerProfileActivity.this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID));
                params.put(ApiConstants.PARAM_BAKER_ID, sBakerId);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }



    /*
     *
     * Inner classes
     */

    private class BakerProfileViewPagerAdapter extends FragmentPagerAdapter {

        public BakerProfileViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return new AboutBakerProfileFragment();
            } else {
                return new ReviewsBakerProfileFragment();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}

package com.android.wishacake.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.ErrorTextWatcher;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.Utils;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private ScrollView mScrollView;
    //    private TextView mTextViewHeading;
    private TextView mTextViewFirstNameHelper, mTextViewLastNameHelper, mTextViewEmailHelper,
            mTextViewPassHelper, mTextViewTermsOfServiceAndPrivacyPolicy;
    private MaterialEditText mMaterialEditTextFirstName, mMaterialEditTextLastName, mMaterialEditTextEmail, mMaterialEditTextPass;
    private Button mButtonSignUp, mButtonFb, mButtonLogin;
    private RelativeLayout mRelativeLayoutOr;
    private FrameLayout mFrameLayoutFb;
    private LinearLayout mLinearLayoutLogin;

    private ProgressDialog mProgressDialog;

    private CallbackManager mCallbackManager;

    private String mFirstName, mLastName, mEmail, mPassword;
    private boolean mIsSignUpForGuestUser;

    private static final String LOG_TAG = SignUpActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            setTheme(R.style.AppTheme_NoActionBar_White);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.EXTRA_KEY_IS_SIGN_UP_FOR_GUEST_USER)) {
                mIsSignUpForGuestUser = extras.getBoolean(Constants.EXTRA_KEY_IS_SIGN_UP_FOR_GUEST_USER);
                if (mIsSignUpForGuestUser) {
                    CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) mScrollView.getLayoutParams();
                    layoutParams.bottomMargin = 0;
                    mScrollView.setLayoutParams(layoutParams);

                    mLinearLayoutLogin.setVisibility(View.GONE);

                    mRelativeLayoutOr.setVisibility(View.VISIBLE);
                    mFrameLayoutFb.setVisibility(View.VISIBLE);
                    mButtonFb.setVisibility(View.VISIBLE);

                    getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(SignUpActivity.this, R.drawable.ic_close));
                }
            }
        }

        mMaterialEditTextFirstName.addTextChangedListener(new ErrorTextWatcher(mTextViewFirstNameHelper));
        mMaterialEditTextLastName.addTextChangedListener(new ErrorTextWatcher(mTextViewLastNameHelper));
        mMaterialEditTextEmail.addTextChangedListener(new ErrorTextWatcher(mTextViewEmailHelper));
        mMaterialEditTextPass.addTextChangedListener(new ErrorTextWatcher(mTextViewPassHelper));

        mCallbackManager = CallbackManager.Factory.create();

        mButtonFb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickSignUpWithFb();
            }
        });

        mButtonSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSignUp();
            }
        });

        mButtonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Globals.sIsSignUpOpenedFromLogin) {
            Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
            return;
        }
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (Globals.sIsSignUpOpenedFromLogin) {
                    Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
                    startActivity(i);
                    finish();
                    return true;
                }
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
//        mTextViewHeading = (TextView) findViewById(R.id.tv_sign_up_heading);
        mScrollView = (ScrollView) findViewById(R.id.sv_scrollview);
        mMaterialEditTextFirstName = (MaterialEditText) findViewById(R.id.et_first_name);
        mTextViewFirstNameHelper = (TextView) findViewById(R.id.tv_first_name_helper);
        mMaterialEditTextLastName = (MaterialEditText) findViewById(R.id.et_last_name);
        mTextViewLastNameHelper = (TextView) findViewById(R.id.tv_last_name_helper);
        mMaterialEditTextEmail = (MaterialEditText) findViewById(R.id.et_email);
        mTextViewEmailHelper = (TextView) findViewById(R.id.tv_email_helper);
        mMaterialEditTextPass = (MaterialEditText) findViewById(R.id.et_pass);
        mTextViewPassHelper = (TextView) findViewById(R.id.tv_pass_helper);
        mButtonSignUp = (Button) findViewById(R.id.btn_sign_up);
        mRelativeLayoutOr = (RelativeLayout) findViewById(R.id.relative_layout_or);
        mFrameLayoutFb = (FrameLayout) findViewById(R.id.frame_layout_fb);
        mButtonFb = (Button) findViewById(R.id.btn_sign_up_with_fb);
        mTextViewTermsOfServiceAndPrivacyPolicy = (TextView) findViewById(R.id.tv_terms_of_service_and_privacy_policy);
        mButtonLogin = (Button) findViewById(R.id.btn_login);
        mLinearLayoutLogin = (LinearLayout) findViewById(R.id.linear_layout_login);

//        mTextViewHeading.setText(Html.fromHtml("<font color='#9b9b9b'>Let's</font>"
//                + "<br><font color='#f06292'>" + getString(R.string.sign_up_heading) + "</font>"));

        mTextViewTermsOfServiceAndPrivacyPolicy.setText(Html.fromHtml("By signing up, you agree to " +
                getString(R.string.app_name) + "'s" +
                "<br><font color='#f06292'><a href='http://feedpakistan.com/wishacake/terms-of-service.html'><u>Terms of service</u></font>" +
                " & <font color='#f06292'><a href='http://feedpakistan.com/wishacake/privacy-policy.html'><u>Privacy policy</u></font>."));
        mTextViewTermsOfServiceAndPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void onClickSignUp() {
        if (isValid()) {
            if (!Utils.isNetworkAvailable(getApplicationContext())) {
                Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
                // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
                return;
            }
            mProgressDialog.setMessage("Creating account...");
            mProgressDialog.show();

            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    signUp();
                    return null;
                }
            }.execute();
        }
    }

    @SuppressLint("StringFormatInvalid")
    private boolean isValid() {
        boolean flag = false;

        mFirstName = mMaterialEditTextFirstName.getText().toString().trim();
        mLastName = mMaterialEditTextLastName.getText().toString().trim();
        mEmail = mMaterialEditTextEmail.getText().toString().trim();
        mPassword = mMaterialEditTextPass.getText().toString().trim();

        if (mFirstName.isEmpty()) {
            Utils.showError(mMaterialEditTextFirstName, getString(R.string.error_required, getString(R.string.shared_hint_first_name)), mTextViewFirstNameHelper);
        } else if (Utils.isNameValid(mFirstName) == false) {
            Utils.showError(mMaterialEditTextFirstName, getString(R.string.error_badly_formatted, getString(R.string.shared_hint_first_name)), mTextViewFirstNameHelper);
        } else if (mLastName.isEmpty()) {
            Utils.showError(mMaterialEditTextLastName, getString(R.string.error_required, getString(R.string.shared_hint_last_name)), mTextViewLastNameHelper);
        } else if (Utils.isNameValid(mLastName) == false) {
            Utils.showError(mMaterialEditTextLastName, getString(R.string.error_badly_formatted, getString(R.string.shared_hint_last_name)), mTextViewLastNameHelper);
        } else if (mEmail.isEmpty()) {
            Utils.showError(mMaterialEditTextEmail, getString(R.string.error_required, getString(R.string.shared_hint_email)), mTextViewEmailHelper);
        } else if (Utils.isEmailValid(mEmail) == false) {
            Utils.showError(mMaterialEditTextEmail, getString(R.string.error_badly_formatted, getString(R.string.shared_hint_email)), mTextViewEmailHelper);
        } else if (mPassword.isEmpty()) {
            Utils.showError(mMaterialEditTextPass, getString(R.string.error_required, getString(R.string.shared_hint_pass)), mTextViewPassHelper);
        } else if (Utils.isPasswordValid(mPassword) == false) {
            Utils.showError(mMaterialEditTextPass, getString(R.string.error_invalid_pass), mTextViewPassHelper);
        } else {
            flag = true;
        }
        return flag;
    }

    private void signUp() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_SIGN_UP, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        // Clear input fields
                        mMaterialEditTextFirstName.setText("");
                        mMaterialEditTextLastName.setText("");
                        mMaterialEditTextEmail.setText("");
                        mMaterialEditTextPass.setText("");
                        mMaterialEditTextFirstName.requestFocus();

                        Utils.showCustomOkAlertDialog(SignUpActivity.this, "Your account has been created successfully",
                                "To continue using " + getString(R.string.app_name) + ", you'll need to verify your account." +
                                        " We have sent an email to \"" + mEmail + "\" containing instructions on how to verify your account.");
                    } else {
                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);

                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && (errorCode.equals("0") || errorCode.equals("-2"))) {
                            Utils.showError(mMaterialEditTextEmail, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                    mTextViewEmailHelper);
                        } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
                            Toast.makeText(SignUpActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(SignUpActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_FIREBASE_TOKEN, Utils.getFirebaseToken());
                params.put(ApiConstants.PARAM_FIRST_NAME, mFirstName);
                params.put(ApiConstants.PARAM_LAST_NAME, mLastName);
                params.put(ApiConstants.PARAM_EMAIL, mEmail.toLowerCase());
                params.put(ApiConstants.PARAM_PASSWORD, mPassword);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void onClickSignUpWithFb() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }

        LoginManager.getInstance().logInWithReadPermissions(SignUpActivity.this,
                Arrays.asList("email", "public_profile"));

        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    mFirstName = object.getString("first_name");
                                    mLastName = object.getString("last_name");
                                    mEmail = object.getString("email");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                // Request Graph API
                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name,last_name,email");
                request.setParameters(parameters);
                request.executeAsync();

                Log.v(LOG_TAG, "First name: " + mFirstName);
                Log.v(LOG_TAG, "Last name: " + mLastName);
                Log.v(LOG_TAG, "Email: " + mEmail);

                if (mFirstName == null || mLastName == null || mEmail == null) {
                    onClickSignUpWithFb();
                } else {
                    mProgressDialog.setMessage("Processing...");
                    mProgressDialog.show();

                    new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... voids) {
                            signUpWithFacebook();
                            return null;
                        }
                    }.execute();
                }
            }

            @Override
            public void onCancel() {
                Log.v(LOG_TAG, "onCancel() called of Fb");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(LOG_TAG, "onError() called of Fb: " + error.toString());
            }
        });
    }

    private void signUpWithFacebook() {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_SIGN_UP_WTIH_FACEBOOK,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);
                            String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS) || (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("-2"))) {
                                JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                                String id = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                                String fn = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                                String ln = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                                String mobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                                String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                                String emailVerifiedStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL_VERIFIED_STATUS);
                                String password = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PASSWORD);
                                String accountType = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_TYPE);
                                String accountStatus = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_STATUS);
                                String modifiedAt = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MODIFIED_AT);

                                // Verified
                                if (emailVerifiedStatus.equals("1")) {
                                    // Active user
                                    if (accountStatus.equals("1")) {
                                        SharedPrefSingleton.getInstance(SignUpActivity.this).saveLoggedInUserData(id, fn, ln, mobileNumber,
                                                email, password, accountType);
                                        Globals.sIsGuestUserLoggedIn = false;
                                        startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                                    }
                                    // Blocked user
                                    else if (accountStatus.equals("0")) {
                                        Utils.showCustomOkAlertDialog(SignUpActivity.this, "Your account has been temporarily blocked", "For further details please visit your nearest wishacake help center or call us at our helpline (021) 111-111-123.");
                                    }
                                }
                                // Unverified
                                else if (emailVerifiedStatus.equals("0")) {
                                    // First time FB login
                                    if (errorCode.equals("")) {
                                        Utils.showCustomOkAlertDialog(SignUpActivity.this, "Your account has been created successfully",
                                                "To continue using " + getString(R.string.app_name) + ", you'll need to verify your account." +
                                                        " We have sent an email to \"" + mEmail + "\" containing instructions on how to verify your account.");
                                    } else if (errorCode.equals("-2")) {
                                        String dateTimeStr = DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd HH:mm:ss", "dd MMM, yyyy", modifiedAt) + " at " + DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd HH:mm:ss", "hh:mm a", modifiedAt);
                                        Utils.showResendVerificationEmailDialog(SignUpActivity.this, mEmail, "A verification email was sent to \"" + mEmail + "\" on " + dateTimeStr + ". If you can't find it, request a new one by tapping the resend button below.");
                                    }
                                }
                            } else {
                                if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("0")) {
                                    showDialogIfEmailAlreadyExist(mEmail, rootJsonObject.getString(ApiConstants.KEY_RESPONSE));
                                } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
                                    Toast.makeText(SignUpActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(SignUpActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_FIREBASE_TOKEN, Utils.getFirebaseToken());
                params.put(ApiConstants.PARAM_FIRST_NAME, mFirstName);
                params.put(ApiConstants.PARAM_LAST_NAME, mLastName);
                params.put(ApiConstants.PARAM_EMAIL, mEmail);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void showDialogIfEmailAlreadyExist(String title, String msg) {
        View dialogView = LayoutInflater.from(SignUpActivity.this).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(SignUpActivity.this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.login_alert_dialog_button), null);

        final TextView textViewTitle = (TextView) dialogView.findViewById(R.id.tv_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText(title);

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText)); // Default in XML is PrimaryTextColor
        textViewMsg.setText(msg);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
                finish();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }
}

package com.android.wishacake.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.adapters.FeaturedCakesAdapter;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.models.FeaturedCake;
import com.android.wishacake.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FeaturedCakesActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private List<FeaturedCake> mFeaturedCakes;
    private FeaturedCakesAdapter mFeaturedCakesAdapter;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerViewFeaturedCakesItems;
    private RelativeLayout mRelativeLayoutEmptyView;
    private ProgressBar mProgressBar;
    private ImageView mImageViewEmptyView;
    private TextView mTextViewEmptyView, mTextViewEmptyViewTextButton;

    private static final String LOG_TAG = FeaturedCakesActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_featured_cakes);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(FeaturedCakesActivity.this, R.drawable.ic_arrow_back_white));

        mFeaturedCakes = new ArrayList<>();
        mFeaturedCakesAdapter = new FeaturedCakesAdapter(FeaturedCakesActivity.this, mFeaturedCakes);
        mFeaturedCakesAdapter.setItemClickListener(new FeaturedCakesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                FeaturedCake featuredCake = mFeaturedCakesAdapter.getItem(position);
                Intent intent = new Intent(FeaturedCakesActivity.this, FeaturedCakeDetailsActivity.class);
                intent.putStringArrayListExtra(Constants.EXTRA_KEY_FEATURED_CAKE_IMAGES, (ArrayList<String>) featuredCake.getImageUrls());
                intent.putExtra(Constants.EXTRA_KEY_FEATURED_CAKE_TITLE, featuredCake.getTitle());
                intent.putExtra(Constants.EXTRA_KEY_FEATURED_CAKE_DESCRIPTION, featuredCake.getDescription());
                startActivity(intent);
            }
        });
        mRecyclerViewFeaturedCakesItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        mRecyclerViewFeaturedCakesItems.setAdapter(mFeaturedCakesAdapter);

        executeAsyncTaskFeaturedCakes();

        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(this, R.color.colorPrimary));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeAsyncTaskFeaturedCakes();
            }
        });

        mTextViewEmptyViewTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTextViewEmptyViewTextButton.getText().toString().equalsIgnoreCase(getString(R.string.shared_try_again))) {
                    mTextViewEmptyViewTextButton.setVisibility(View.GONE);
                    executeAsyncTaskFeaturedCakes();
                }
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerViewFeaturedCakesItems = (RecyclerView) findViewById(R.id.rv_featured_cakes_items);
        mRelativeLayoutEmptyView = (RelativeLayout) findViewById(R.id.relative_layout_empty_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mImageViewEmptyView = (ImageView) findViewById(R.id.img_empty_view);
        mTextViewEmptyView = (TextView) findViewById(R.id.tv_empty_view);
        mTextViewEmptyViewTextButton = (TextView) findViewById(R.id.tv_empty_view_text_button);
    }

    private void stopRefreshingSwipeLayout() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void toggleRecyclerView(int recyclerViewVisibility) {
        mRecyclerViewFeaturedCakesItems.setVisibility(recyclerViewVisibility);
    }

    private void showEmptyView(int imageVisibility, int textButtonVisibility, int progressBarVisibility, String textViewText, String textButtonText) {
        mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        mImageViewEmptyView.setVisibility(imageVisibility);
        mTextViewEmptyViewTextButton.setVisibility(textButtonVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mTextViewEmptyView.setText(textViewText);
        mTextViewEmptyViewTextButton.setText(textButtonText);
    }

    private void hideEmptyView() {
        mRelativeLayoutEmptyView.setVisibility(View.GONE);
    }

    public void executeAsyncTaskFeaturedCakes() {
        mFeaturedCakes.clear();
        toggleRecyclerView(View.GONE);
        showEmptyView(View.GONE, View.GONE, View.VISIBLE, "", "");

        if (!Utils.isNetworkAvailable(this)) {
            stopRefreshingSwipeLayout();
            showEmptyView(View.GONE, View.VISIBLE, View.GONE, getString(R.string.error_no_internet_connection), getString(R.string.shared_try_again));
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                getFeaturedCakesFromServer();
                return null;
            }
        }.execute();
    }

    private void getFeaturedCakesFromServer() {
        final List<FeaturedCake> featuredCakes = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ApiConstants.API_GET_FEATURED_CAKES, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideEmptyView();
                mProgressBar.setVisibility(View.GONE);
                stopRefreshingSwipeLayout();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String responseStatus = rootJsonObject.getString(ApiConstants.KEY_STATUS);
                    if (responseStatus.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);
                        if (responseArray.length() > 0) {
                            // Iterate through the array
                            for (int i = 0; i < responseArray.length(); i++) {
                                List<String> imageUrls = new ArrayList<>();
                                JSONObject jsonObject = responseArray.getJSONObject(i);
                                String image1 = jsonObject.getString(ApiConstants.PARAM_IMAGE1);
                                String image2 = jsonObject.getString(ApiConstants.PARAM_IMAGE2);
                                String image3 = jsonObject.getString(ApiConstants.PARAM_IMAGE3);
                                String image4 = jsonObject.getString(ApiConstants.PARAM_IMAGE4);
                                String image5 = jsonObject.getString(ApiConstants.PARAM_IMAGE5);
                                String image6 = jsonObject.getString(ApiConstants.PARAM_IMAGE6);
                                String image7 = jsonObject.getString(ApiConstants.PARAM_IMAGE7);
                                String image8 = jsonObject.getString(ApiConstants.PARAM_IMAGE8);
                                String image9 = jsonObject.getString(ApiConstants.PARAM_IMAGE9);
                                String image10 = jsonObject.getString(ApiConstants.PARAM_IMAGE10);
                                String title = jsonObject.getString(ApiConstants.PARAM_TITLE);
                                String description = jsonObject.getString(ApiConstants.PARAM_DESCRIPTION);

                                if (!Utils.isStringEmptyOrNull(image1)) {
                                    imageUrls.add(image1);
                                }
                                if (!Utils.isStringEmptyOrNull(image2)) {
                                    imageUrls.add(image2);
                                }
                                if (!Utils.isStringEmptyOrNull(image3)) {
                                    imageUrls.add(image3);
                                }
                                if (!Utils.isStringEmptyOrNull(image4)) {
                                    imageUrls.add(image4);
                                }
                                if (!Utils.isStringEmptyOrNull(image5)) {
                                    imageUrls.add(image5);
                                }
                                if (!Utils.isStringEmptyOrNull(image6)) {
                                    imageUrls.add(image6);
                                }
                                if (!Utils.isStringEmptyOrNull(image7)) {
                                    imageUrls.add(image7);
                                }
                                if (!Utils.isStringEmptyOrNull(image8)) {
                                    imageUrls.add(image8);
                                }
                                if (!Utils.isStringEmptyOrNull(image9)) {
                                    imageUrls.add(image9);
                                }
                                if (!Utils.isStringEmptyOrNull(image10)) {
                                    imageUrls.add(image10);
                                }
                                if (Utils.isStringEmptyOrNull(title)) {
                                    title = "Title";
                                }
                                featuredCakes.add(new FeaturedCake(imageUrls, title, description));
                            }
                            hideEmptyView();
                            toggleRecyclerView(View.VISIBLE);
                            mFeaturedCakes.addAll(featuredCakes);
                            mRecyclerViewFeaturedCakesItems.setAdapter(mFeaturedCakesAdapter);
                            mFeaturedCakesAdapter.notifyDataSetChanged();
                        } else {
                            toggleRecyclerView(View.GONE);
                            showEmptyView(View.VISIBLE, View.GONE, View.GONE, getString(R.string.featured_cakes_text_empty_view), "");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                stopRefreshingSwipeLayout();
                toggleRecyclerView(View.GONE);
                showEmptyView(View.GONE, View.VISIBLE, View.GONE, "Sorry, something went wrong. Please try again.", getString(R.string.shared_try_again));
                if (error != null) {
                    Log.e(LOG_TAG, error.toString());
                }
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}

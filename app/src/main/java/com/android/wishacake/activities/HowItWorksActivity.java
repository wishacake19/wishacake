package com.android.wishacake.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.wishacake.R;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

public class HowItWorksActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private YouTubePlayerSupportFragment mYouTubePlayerSupportFragment;
    private YouTubePlayer.OnInitializedListener mOnInitializedListener;

    private Button mButtonOpenInYoutube;

    private static final String LOG_TAG = HowItWorksActivity.class.getSimpleName();
    private final String VIDEO_ID = "VAjbSoVPZOY"; // --CdAeCT45k

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_how_it_works);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(HowItWorksActivity.this, R.drawable.ic_arrow_back_white));

        mOnInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                Log.v(LOG_TAG, "onInitializationSuccess() called...");
                youTubePlayer.setShowFullscreenButton(false);
                youTubePlayer.loadVideo(VIDEO_ID);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Log.v(LOG_TAG, "onInitializationFailure() called...");
            }
        };

        mYouTubePlayerSupportFragment.initialize(getString(R.string.youtube_android_player_api_key), mOnInitializedListener);

        mButtonOpenInYoutube.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri youtubeUri = Uri.parse("https://www.youtube.com/watch?v=" + VIDEO_ID);
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, youtubeUri);
                startActivity(websiteIntent);
            }
        });
    }

    /*
     *
     * Overridden methods
     */

//    @Override
//    public void onBackPressed() {
//        finish();
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mYouTubePlayerSupportFragment = (YouTubePlayerSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.youtube_player_support_frag);
        mButtonOpenInYoutube = (Button) findViewById(R.id.btn_open_in_youtube);
    }
}

package com.android.wishacake.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.android.wishacake.R;
import com.squareup.picasso.Picasso;

public class SplashActivity extends AppCompatActivity {

    private final int SPLASH_SCREEN_DISPLAY_LENGTH = 3500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_splash);

        // hides the action bar
        getSupportActionBar().hide();

        ImageView imageViewSplash = (ImageView) findViewById(R.id.img_splash);
        Picasso.get().load(R.drawable.ic_splash).fit().into(imageViewSplash);

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, WelcomeActivity.class);
                startActivity(i);
                finish();
            }
        }, SPLASH_SCREEN_DISPLAY_LENGTH);
    }
}

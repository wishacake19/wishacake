package com.android.wishacake.activities;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.adapters.SliderAdapter;
import com.android.wishacake.ar.UnityPlayerActivity;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.SliderTimer;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.models.Baker;
import com.android.wishacake.models.Order;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.PermissionUtils;
import com.android.wishacake.utilities.Utils;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private ViewPager mViewPagerSlider;
    private LinearLayout mLinearLayoutDotIndicator;

    private List<Integer> mImageResIds;
    private SliderAdapter mSliderAdapter;
    private ImageView[] mImageViewsDots;
    private int mDotCounts = 0;

    private TextView mTextViewViewAll;
    private RecyclerView mRecyclerViewRecentOrdersItems;
    private RecentOrdersAdapter mRecentOrdersAdapter;
    private List<Order> mOrders;

    private RelativeLayout mRelativeLayoutRecentOrders, mRelativeLayoutLetsBake;

    private AlertDialog mAlertDialogLetsBake;
    private Timer mTimer;

    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        File wishacakeFolder = new File("/storage/emulated/0/wishacake");
        if (!wishacakeFolder.exists()) {
            wishacakeFolder.mkdir();
        }

        setSupportActionBar(mToolbar);

        setUpNavigationView();

        mImageResIds = new ArrayList<>();
        mImageResIds.add(R.drawable.ic_slider_img1);
        mImageResIds.add(R.drawable.ic_slider_img2);
        mImageResIds.add(R.drawable.ic_slider_img3);

        setUpSlider();
        setUpSliderDots();
        mViewPagerSlider.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int pos) {
                for (int i = 0; i < mDotCounts; i++) {
                    mImageViewsDots[i].setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.inactive_dot_indicator));
                }
                mImageViewsDots[pos].setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.active_dot_indicator));
            }

            @Override
            public void onPageScrollStateChanged(int i) {
            }
        });

        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new SliderTimer(MainActivity.this, mViewPagerSlider, mImageResIds), 2500, 2500);

        if (Globals.sIsGuestUserLoggedIn) {
            Menu menuDrawer = mNavigationView.getMenu();
            menuDrawer.findItem(R.id.nav_your_orders).setVisible(false);
            menuDrawer.findItem(R.id.nav_bakers).setVisible(false);
            menuDrawer.findItem(R.id.nav_payment).setVisible(false);
            menuDrawer.findItem(R.id.nav_settings).setVisible(false);
            menuDrawer.findItem(R.id.nav_help).setVisible(false);
            invalidateOptionsMenu(); // This will call onCreateOptionsMenu()
        }
        if (!Globals.sIsGuestUserLoggedIn) {
            mTextViewViewAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, YourOrdersActivity.class));
                }
            });

            mOrders = new ArrayList<>();
            mRecentOrdersAdapter = new RecentOrdersAdapter(this, mOrders);
            mRecentOrdersAdapter.setItemClickListener(new RecentOrdersAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Order order = mRecentOrdersAdapter.getItem(position);
                    Intent orderIntent = new Intent(MainActivity.this, OrderDetailsActivity.class);
                    orderIntent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, (ArrayList<String>) order.getImageUrls());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DATE, order.getDate());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_STATUS, order.getStatus());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_ID, order.getId());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_QUANTITY, order.getQuantity());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_POUNDS, order.getPounds());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DESCRIPTION, order.getDescription());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_CONTACT_NUMBER, order.getContactNumber());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_LOCATION, order.getDeliveryLocation());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_ADDRESS, order.getDeliveryAddress());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_DATE, order.getDeliveryDate());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_TIME, order.getDeliveryTime());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_SUBTOTAL, order.getSubTotal());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DELIVERY_CHARGES, order.getDeliveryCharges());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_AMOUNT, order.getTotalAmount());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_PAYMENT_METHOD, order.getPaymentMethod());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_REJECTED_REASON, order.getRejectedReason());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_ORDER_RATING, order.getOrderRating());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_RATED_STATUS, order.getRatedStatus());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_ID, order.getBaker().getId());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_IMAGE, order.getBaker().getImageUrl());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_NAME, order.getBaker().getFirstName() + " " + order.getBaker().getLastName());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_ADDRESS, order.getBaker().getAddress());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_RATING, order.getBaker().getRating());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_MOBILE_NUMBER, order.getBaker().getMobileNumber());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_EMAIL, order.getBaker().getEmail());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_DISTANCE, order.getBaker().getDistance());
                    orderIntent.putStringArrayListExtra(Constants.EXTRA_KEY_BAKER_SLIDER_IMAGES, (ArrayList<String>) order.getBaker().getSliderImageUrls());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_ACTIVE_STATUS, order.getBaker().getActiveStatus());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_FAVORITE_ID, order.getBaker().getFavoriteId());
                    orderIntent.putExtra(Constants.EXTRA_KEY_BAKER_IS_FAVORITE, order.getBaker().isFavorite());
                    orderIntent.putExtra(Constants.EXTRA_KEY_ORDER_DATA_LOAD_FROM_SERVER, false);
                    startActivity(orderIntent);
                }
            });
            mRecyclerViewRecentOrdersItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
            mRecyclerViewRecentOrdersItems.setAdapter(mRecentOrdersAdapter);

            executeAsyncTaskOrders();
        }

        mRelativeLayoutLetsBake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickLetsBake(MainActivity.this);
            }
        });
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
            return;
        }
        super.onBackPressed();

        // For actual user
        if (!Globals.sIsGuestUserLoggedIn) {
            // Closes the whole app
            ActivityCompat.finishAffinity(MainActivity.this); // Clear the previous activities stack
            finish();
        }
        // For guest user
        else {
            ActivityCompat.finishAffinity(MainActivity.this);
            startActivity(new Intent(MainActivity.this, WelcomeActivity.class));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setUpSlider();
        setUpHeaderView();
//        if (!Globals.sIsGuestUserLoggedIn) {
//            executeAsyncTaskOrders();
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionUtils.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0) {
                boolean writeStoragePermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                if (writeStoragePermission) {
                    openUnityPlayerActivity(mAlertDialogLetsBake);
                }
            }
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mViewPagerSlider = (ViewPager) findViewById(R.id.vp_slider);
        mLinearLayoutDotIndicator = (LinearLayout) findViewById(R.id.linear_layout_dot_indicator);
        mTextViewViewAll = (TextView) findViewById(R.id.tv_view_all);
        mRecyclerViewRecentOrdersItems = (RecyclerView) findViewById(R.id.rv_recent_orders_items);
        mRelativeLayoutRecentOrders = (RelativeLayout) findViewById(R.id.relative_layout_recent_orders);
        mRelativeLayoutLetsBake = (RelativeLayout) findViewById(R.id.relative_layout_lets_bake);
    }

    private void setUpNavigationView() {
        // Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                // Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    case R.id.nav_your_orders:
                        startActivity(new Intent(MainActivity.this, YourOrdersActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_featured_cakes:
                        startActivity(new Intent(MainActivity.this, FeaturedCakesActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_bakers:
                        Intent intent = new Intent(MainActivity.this, DeliveryLocationActivity.class);
                        intent.putExtra(Constants.EXTRA_KEY_IS_BAKERS_OPENED_FROM_NAVIGATION_MENU, true);
                        startActivity(intent);
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_payment:
                        startActivity(new Intent(MainActivity.this, PaymentActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_how_it_works:
                        startActivity(new Intent(MainActivity.this, HowItWorksActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_settings:
                        startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                        mDrawerLayout.closeDrawers();
                        return true;
                    case R.id.nav_help:
                        startActivity(new Intent(MainActivity.this, HelpActivity.class));
//                        Uri uri = Uri.parse("http://feedpakistan.com/wishacake/help.html");
//                        Intent intentHelp = new Intent(Intent.ACTION_VIEW, uri);
//                        if (intentHelp.resolveActivity(getPackageManager()) != null) {
//                            startActivity(intentHelp);
//                        }
                        mDrawerLayout.closeDrawers();
                        return true;
                }

                return true;
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,
                mDrawerLayout, mToolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes
                super.onDrawerClosed(drawerView);
//                Utils.changeStatusBarBackground(MainActivity.this, ContextCompat.getColor(MainActivity.this, R.color.colorPrimaryDark));
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open
                super.onDrawerOpened(drawerView);
//                  Utils.changeStatusBarBackground(MainActivity.this, Color.TRANSPARENT);
            }
        };

        // Setting the actionBarToggle to drawer layout
        mDrawerLayout.setDrawerListener(actionBarDrawerToggle);

        // Calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void setUpHeaderView() {
        View headerView = mNavigationView.getHeaderView(0);
        LinearLayout linearLayoutNavHeaderActualUser = (LinearLayout) headerView.findViewById(R.id.linear_layout_nav_header_actual_user);
        LinearLayout linearLayoutNavHeaderGuestUser = (LinearLayout) headerView.findViewById(R.id.linear_layout_nav_header_guest_user);

        // For actual user
        if (!Globals.sIsGuestUserLoggedIn) {
            linearLayoutNavHeaderGuestUser.setVisibility(View.GONE);
            linearLayoutNavHeaderActualUser.setVisibility(View.VISIBLE);

            String userName = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_FIRST_NAME)
                    + " " + SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_LAST_NAME);

            TextView tvUserName = (TextView) headerView.findViewById(R.id.tv_user_name);
            tvUserName.setText(userName);
        }
        // For guest user
        else {
            linearLayoutNavHeaderActualUser.setVisibility(View.GONE);
            linearLayoutNavHeaderGuestUser.setVisibility(View.VISIBLE);

            linearLayoutNavHeaderGuestUser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Globals.sIsSignUpOpenedFromLogin = false;
                    Intent intent = new Intent(MainActivity.this, SignUpActivity.class);
                    intent.putExtra(Constants.EXTRA_KEY_IS_SIGN_UP_FOR_GUEST_USER, true);
                    startActivity(intent);
                }
            });
        }
    }

    private void setUpSlider() {
        Globals.sIsSliderFromMainActivity = true;
        List<String> dummyList = new ArrayList<>();
        mSliderAdapter = new SliderAdapter(this, dummyList, mImageResIds);
        mViewPagerSlider.setAdapter(mSliderAdapter);
        mViewPagerSlider.setCurrentItem(0);
    }

    private void setUpSliderDots() {
        mDotCounts = mSliderAdapter.getCount();
        mImageViewsDots = new ImageView[mDotCounts];

        // Calculating width for dots becoz neither match_parent working nor wrap_content
        int width = 0;
        for (int i = 0; i < mDotCounts; i++) {
            // 4 is left margin and 4 is size of dot
            width += 8;
        }
        width += 4; // Right margin for last dot
        Log.v(LOG_TAG, "Width -> " + width + "");

        for (int i = 0; i < mDotCounts; i++) {
            mImageViewsDots[i] = new ImageView(this);
            mImageViewsDots[i].setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.inactive_dot_indicator));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, 24);
            params.setMargins(4, 0, 4, 0);
            mLinearLayoutDotIndicator.addView(mImageViewsDots[i], params);
        }
        mImageViewsDots[0].setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.active_dot_indicator));
    }

    private void onClickLetsBake(final Context context) {
        View dialogView = LayoutInflater.from(context).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNeutralButton(context.getString(R.string.main_previously_designed_cake), null);
        builder.setPositiveButton(context.getString(R.string.main_new_cake), null);

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setText(context.getString(R.string.main_on_click_lets_bake));

        mAlertDialogLetsBake = builder.create();
        mAlertDialogLetsBake.setCancelable(true);

        mAlertDialogLetsBake.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        mAlertDialogLetsBake.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialogLetsBake.dismiss();
                Intent intent = new Intent(context, PreviewImagesActivity.class);
                intent.putExtra(Constants.EXTRA_KEY_TITLE_PREVIEW_IMAGES_ACTIVITY, "Upload images");
                context.startActivity(intent);
            }
        });

        mAlertDialogLetsBake.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PermissionUtils.checkGalleryPermission(MainActivity.this)) {
                    openUnityPlayerActivity(mAlertDialogLetsBake);
                }
            }
        });

        mAlertDialogLetsBake.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
        mAlertDialogLetsBake.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
    }

    private void openUnityPlayerActivity(AlertDialog alertDialog) {
        alertDialog.dismiss();
        Intent intent = new Intent(MainActivity.this, UnityPlayerActivity.class);
        startActivity(intent);

//        Intent intentTwo = new Intent();
//        String packageName = getPackageName();
//        intentTwo.setClassName(packageName, packageName + ".activities.PreviewImagesActivity");
//        intentTwo.putExtra(Constants.EXTRA_KEY_TITLE_PREVIEW_IMAGES_ACTIVITY, getString(R.string.activity_title_preview_images));
//        startActivity(intentTwo);
    }

    public void executeAsyncTaskOrders() {
        mOrders.clear();
        mRelativeLayoutRecentOrders.setVisibility(View.GONE);
        if (!Utils.isNetworkAvailable(this)) {
            return;
        }
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                getOrdersFromServer();
                return null;
            }
        }.execute();
    }

    private void getOrdersFromServer() {
        final List<Order> orders = new ArrayList<>();
        final String loggedInUserId = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_GET_ORDERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mRelativeLayoutRecentOrders.setVisibility(View.GONE);

                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String responseStatus = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (responseStatus.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {

                        // Get the json array as a response
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        if (responseArray.length() > 0) {

                            // Iterate through the array
                            for (int i = 0; i < responseArray.length(); i++) {

                                if (i == 3) {
                                    break;
                                }

                                List<String> orderImageUrls = new ArrayList<>();
                                List<String> bakerSliderImageUrls = new ArrayList<>();

                                // Get json object inside the array index wise
                                JSONObject orderJsonObject = responseArray.getJSONObject(i);

                                // Get order details of order json object index wise
                                String id = orderJsonObject.getString(ApiConstants.PARAM_ID);
                                String image1 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE1);
                                String image2 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE2);
                                String image3 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE3);
                                String image4 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE4);
                                String image5 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE5);
                                String image6 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE6);
                                String image7 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE7);
                                String image8 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE8);
                                String image9 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE9);
                                String image10 = orderJsonObject.getString(ApiConstants.PARAM_IMAGE10);
                                long date = DateTimeUtils.convertStringDateToLong("yyyy-MM-dd HH:mm:ss", orderJsonObject.getString(ApiConstants.PARAM_CREATED_AT));
                                String status = Utils.getOrderStatusByOrderStatusCode(orderJsonObject.getString(ApiConstants.PARAM_STATUS));
                                String quantity = orderJsonObject.getString(ApiConstants.PARAM_QUANTITY);
                                String pounds = orderJsonObject.getString(ApiConstants.PARAM_POUNDS);
                                String description = orderJsonObject.getString(ApiConstants.PARAM_DESCRIPTION);
                                String contactNumber = orderJsonObject.getString(ApiConstants.PARAM_CONTACT_NUMBER);
                                String deliveryLocation = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_LOCATION);
                                String deliveryAddress = orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_ADDRESS);
                                String deliveryDate = DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd", "dd MMM, yyyy", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_DATE));
                                String deliveryTime = DateTimeUtils.getFormattedDateTimeString("HH:mm:ss", "hh:mm a", orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_TIME));
                                double subtotal = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_SUBTOTAL));
                                double deliveryCharges = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_DELIVERY_CHARGES));
                                double totalAmount = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT))
                                        ? 0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_TOTAL_AMOUNT));
                                String paymentMethod = (orderJsonObject.getString(ApiConstants.PARAM_PAYMENT_METHOD).equals("1"))
                                        ? ApiConstants.PARAM_VALUE_CASH : ApiConstants.PARAM_VALUE_CREDIT_DEBIT_CARD;
                                String rejectedReason = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON))
                                        ? "The baker didn't provided the reason for rejecting your order." : orderJsonObject.getString(ApiConstants.PARAM_REJECTED_REASON);
                                double orderRating = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_ORDER_RATING));
                                String ratedStatus = orderJsonObject.getString(ApiConstants.PARAM_RATED_STATUS);
                                String bakerId = orderJsonObject.getString(ApiConstants.PARAM_BAKER_ID);
                                String image = orderJsonObject.getString(ApiConstants.PARAM_IMAGE);
                                String fn = orderJsonObject.getString(ApiConstants.PARAM_FIRST_NAME);
                                String ln = orderJsonObject.getString(ApiConstants.PARAM_LAST_NAME);
                                String address = orderJsonObject.getString(ApiConstants.PARAM_LOCATION_NAME) + "\n" + orderJsonObject.getString(ApiConstants.PARAM_LOCATION_ADDRESS);
                                String mobileNumber = orderJsonObject.getString(ApiConstants.PARAM_MOBILE_NUMBER);
                                String email = orderJsonObject.getString(ApiConstants.PARAM_EMAIL);
                                bakerSliderImageUrls.add(orderJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE1));
                                bakerSliderImageUrls.add(orderJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE2));
                                bakerSliderImageUrls.add(orderJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE3));
                                double bakerRating = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_RATING))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_RATING));
                                double bakerLatitude = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE));
                                double bakerLongitude = Utils.isStringEmptyOrNull(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE))
                                        ? 0.0 : Double.parseDouble(orderJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE));
                                String activeStatus = orderJsonObject.getString(ApiConstants.PARAM_ACTIVE_STATUS);
                                String favoriteId = orderJsonObject.getString(ApiConstants.PARAM_FAVORITE_ID);
                                String userIds = orderJsonObject.getString(ApiConstants.PARAM_USER_IDS);

                                if (!Utils.isStringEmptyOrNull(image1)) {
                                    orderImageUrls.add(image1);
                                }
                                if (!Utils.isStringEmptyOrNull(image2)) {
                                    orderImageUrls.add(image2);
                                }
                                if (!Utils.isStringEmptyOrNull(image3)) {
                                    orderImageUrls.add(image3);
                                }
                                if (!Utils.isStringEmptyOrNull(image4)) {
                                    orderImageUrls.add(image4);
                                }
                                if (!Utils.isStringEmptyOrNull(image5)) {
                                    orderImageUrls.add(image5);
                                }
                                if (!Utils.isStringEmptyOrNull(image6)) {
                                    orderImageUrls.add(image6);
                                }
                                if (!Utils.isStringEmptyOrNull(image7)) {
                                    orderImageUrls.add(image7);
                                }
                                if (!Utils.isStringEmptyOrNull(image8)) {
                                    orderImageUrls.add(image8);
                                }
                                if (!Utils.isStringEmptyOrNull(image9)) {
                                    orderImageUrls.add(image9);
                                }
                                if (!Utils.isStringEmptyOrNull(image10)) {
                                    orderImageUrls.add(image10);
                                }

                                double distance = 0;
                                if (!(Globals.sUserCurrentLocation.getLatitude() <= 0 || Globals.sUserCurrentLocation.getLongitude() <= 0)) {
                                    // Calculate the circular distance between user current and baker location
                                    distance = Utils.calculateCircularDistanceInKilometers(
                                            Globals.sUserCurrentLocation.getLatitude(),
                                            Globals.sUserCurrentLocation.getLongitude(),
                                            bakerLatitude,
                                            bakerLongitude
                                    );
                                }

                                boolean isFavorite = (userIds.contains(loggedInUserId)) ? true : false;

                                Baker baker = new Baker(bakerId, image, fn, ln, address, mobileNumber, email,
                                        bakerSliderImageUrls, bakerRating, Utils.getDoubleValueRoundedOff(distance), isFavorite,
                                        favoriteId);
                                baker.setActiveStatus(activeStatus);

                                orders.add(new Order(id, orderImageUrls, date, status, quantity, pounds, description, contactNumber,
                                        deliveryLocation, deliveryAddress, deliveryDate, deliveryTime, subtotal, deliveryCharges, totalAmount,
                                        paymentMethod, rejectedReason, orderRating, ratedStatus, baker));
                            }
                            mRelativeLayoutRecentOrders.setVisibility(View.VISIBLE);
                            mOrders.addAll(orders);
                            mRecyclerViewRecentOrdersItems.setAdapter(mRecentOrdersAdapter);
                            mRecentOrdersAdapter.notifyDataSetChanged();
                        } else {
                            mRelativeLayoutRecentOrders.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mRelativeLayoutRecentOrders.setVisibility(View.GONE);
                if (error != null) {
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_USER_ID, loggedInUserId);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    /*
     *
     * Inner classes
     */

    private static class RecentOrdersAdapter extends RecyclerView.Adapter<RecentOrdersAdapter.RecentOrdersViewHolder> {

        private Context mContext;
        private List<Order> mOrders = new ArrayList<>();
        private OnItemClickListener mItemClickListener;

        /*
         *
         * Constructor
         */

        public RecentOrdersAdapter(Context context, List<Order> orders) {
            mContext = context;
            mOrders = orders;
        }

        /*
         *
         * Overridden methods
         */

        @NonNull
        @Override
        public RecentOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_recent_orders_recyclerview_item, parent, false);
            return new RecentOrdersViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull RecentOrdersViewHolder holder, int position) {
            Order order = mOrders.get(position);

            Picasso.get().load(R.drawable.ic_cake_sample).fit().placeholder(R.color.colorDivider).into(holder.mCircleImageViewCakeSample);

            String formattedDateStr = "";
            if (DateTimeUtils.isToday(order.getDate())) {
                formattedDateStr = "Today";
            } else if (DateTimeUtils.isYesterday(order.getDate())) {
                formattedDateStr = "Yesterday";
            } else {
                formattedDateStr = new SimpleDateFormat("dd MMM, yyyy").format(order.getDate());
            }
            formattedDateStr += "\n";
            formattedDateStr += new SimpleDateFormat("hh:mm a").format(order.getDate());
            holder.mTextViewOrderDate.setText(formattedDateStr);

            String status = order.getStatus();

            if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_PENDING)) {
                Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_PENDING,
                        R.color.colorOrderStatusPending, R.drawable.order_status_pending_drawable);
            } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_REJECTED)) {
                Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_REJECTED,
                        R.color.colorOrderStatusRejected, R.drawable.order_status_rejected_drawable);
            } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ACCEPTED)) {
                Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_ACCEPTED,
                        R.color.colorOrderStatusAccepted, R.drawable.order_status_accepted_drawable);
            } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CANCELLED)) {
                Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_CANCELLED,
                        R.color.colorOrderStatusCancelled, R.drawable.order_status_cancelled_drawable);
            } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CONFIRMED)) {
                Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_CONFIRMED,
                        R.color.colorOrderStatusConfirmed, R.drawable.order_status_confirmed_drawable);
            } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ONGOING)) {
                Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_ONGOING,
                        R.color.colorOrderStatusOngoing, R.drawable.order_status_ongoing_drawable);
            } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_COMPLETED)) {
                Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_COMPLETED,
                        R.color.colorOrderStatusCompleted, R.drawable.order_status_completed_drawable);
            }
        }

        @Override
        public int getItemCount() {
            return mOrders.size();
        }

        /*
         *
         * Setters
         */

        public void setItemClickListener(OnItemClickListener itemClickListener) {
            mItemClickListener = itemClickListener;
        }

        /*
         *
         * Helper methods
         */

        public Order getItem(int position) {
            return mOrders.get(position);
        }

        /*
         *
         * ViewHolder class
         */

        public class RecentOrdersViewHolder extends RecyclerView.ViewHolder {

            private CircleImageView mCircleImageViewCakeSample;
            private TextView mTextViewOrderDate, mTextViewOrderStatus;

            public RecentOrdersViewHolder(View view) {
                super(view);
                mCircleImageViewCakeSample = (CircleImageView) view.findViewById(R.id.civ_cake_sample);
                mTextViewOrderDate = (TextView) view.findViewById(R.id.tv_order_date);
                mTextViewOrderStatus = (TextView) view.findViewById(R.id.tv_order_status);

                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mItemClickListener != null) {
                            mItemClickListener.onItemClick(v, getAdapterPosition());
                        }
                    }
                });
            }
        }

        /*
         *
         * Item click interface
         */

        public interface OnItemClickListener {
            void onItemClick(View view, int position);
        }
    }
}

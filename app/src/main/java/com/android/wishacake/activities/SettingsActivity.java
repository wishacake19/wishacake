package com.android.wishacake.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.BuildConfig;
import com.android.wishacake.R;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.ErrorTextWatcher;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.utilities.Utils;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;

    private LinearLayout mLinearLayoutUserName, mLinearLayoutUserEmail, mLinearLayoutUserMobileNumber, mLinearLayoutUserPassword,
            mLinearLayoutDeactivateYourAccount, mLinearLayoutBecomeABaker, mLinearLayoutRateUs, mLinearLayoutVisitWebsite, mLinearLayoutTermsOfService,
            mLinearLayoutPrivacyPolicy, mLinearLayoutLogout;
    private TextView mTextViewUserName, mTextViewUserEmail, mTextViewUserMobileNumber, mTextViewUserPassword,
            mTextViewAppVersion;

    private MaterialEditText mMaterialEditTextMobileNumber;
    private TextView mTextViewMobileNumberHelper;

    private ProgressDialog mProgressDialog;
    private AlertDialog mAlertDialogMobileNumber;

    private String mId, mFirstName, mLastName, mEmail, mMobileNumber, mPassword;

    private static final int VERIFY_MOBILE_NUMBER_REQUEST_CODE = 21;
    private static final String LOG_TAG = SettingsActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(SettingsActivity.this, R.drawable.ic_arrow_back_white));

        mProgressDialog = new ProgressDialog(this, R.style.ProgressDialogTheme);
        mProgressDialog.setMessage("Processing...");
        mProgressDialog.setCancelable(false);

        mLinearLayoutUserName.setOnClickListener(this);
        mLinearLayoutUserEmail.setOnClickListener(this);
        mLinearLayoutUserMobileNumber.setOnClickListener(this);
        mLinearLayoutUserPassword.setOnClickListener(this);
        mLinearLayoutBecomeABaker.setOnClickListener(this);
//        mLinearLayoutDeactivateYourAccount.setOnClickListener(this);
        mLinearLayoutRateUs.setOnClickListener(this);
        mLinearLayoutVisitWebsite.setOnClickListener(this);
        mLinearLayoutTermsOfService.setOnClickListener(this);
        mLinearLayoutPrivacyPolicy.setOnClickListener(this);
        mLinearLayoutLogout.setOnClickListener(this);

        mTextViewAppVersion.setText(BuildConfig.VERSION_NAME);
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mLinearLayoutUserName) {
            onClickName();
        }
        if (view == mLinearLayoutUserEmail) {
            onClickEmail();
        }
        if (view == mLinearLayoutUserMobileNumber) {
            onClickMobileNumber();
        }
        if (view == mLinearLayoutUserPassword) {
            onClickPassword();
        }
        if (view == mLinearLayoutBecomeABaker) {
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.android.bakerapp");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
//        if (view == mLinearLayoutDeactivateYourAccount) {
//            onClickDeactivateYourAccount();
//        }
        if (view == mLinearLayoutRateUs) {
            Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName());
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mLinearLayoutVisitWebsite) {
            Uri uri = Uri.parse("http://feedpakistan.com/wishacake/");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mLinearLayoutTermsOfService) {
            Uri uri = Uri.parse("http://feedpakistan.com/wishacake/terms-of-service.html");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mLinearLayoutPrivacyPolicy) {
            Uri uri = Uri.parse("http://feedpakistan.com/wishacake/privacy-policy.html");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
        }
        if (view == mLinearLayoutLogout) {
            onClickLogout();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadUserDataFromSharedPref();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == VERIFY_MOBILE_NUMBER_REQUEST_CODE && resultCode == RESULT_OK) {
            AccountKitLoginResult result = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
            if (result.getError() != null) {
                return;
            } else if (result.wasCancelled()) {
                return;
            } else {
                AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
                    @Override
                    public void onSuccess(Account account) {
                        mMobileNumber = String.valueOf(account.getPhoneNumber());
                        mMaterialEditTextMobileNumber.setText(mMobileNumber);
                        Utils.hideError(mTextViewMobileNumberHelper);
                        mMaterialEditTextMobileNumber.setEnabled(false);
                        onClickDone(mAlertDialogMobileNumber, mMaterialEditTextMobileNumber, mTextViewMobileNumberHelper);
                    }

                    @Override
                    public void onError(AccountKitError accountKitError) {
                    }
                });
            }
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mLinearLayoutUserName = (LinearLayout) findViewById(R.id.linear_layout_user_name);
        mLinearLayoutUserEmail = (LinearLayout) findViewById(R.id.linear_layout_user_email);
        mLinearLayoutUserMobileNumber = (LinearLayout) findViewById(R.id.linear_layout_user_mobile_number);
        mLinearLayoutUserPassword = (LinearLayout) findViewById(R.id.linear_layout_user_password);
//        mLinearLayoutDeactivateYourAccount = (LinearLayout) findViewById(R.id.linear_layout_deactivate_your_account);
        mLinearLayoutBecomeABaker = (LinearLayout) findViewById(R.id.linear_layout_become_a_baker);
        mLinearLayoutRateUs = (LinearLayout) findViewById(R.id.linear_layout_rate_us);
        mLinearLayoutVisitWebsite = (LinearLayout) findViewById(R.id.linear_layout_visit_website);
        mLinearLayoutTermsOfService = (LinearLayout) findViewById(R.id.linear_layout_terms_of_service);
        mLinearLayoutPrivacyPolicy = (LinearLayout) findViewById(R.id.linear_layout_privacy_policy);
        mLinearLayoutLogout = (LinearLayout) findViewById(R.id.linear_layout_logout);
        mTextViewUserName = (TextView) findViewById(R.id.tv_user_name);
        mTextViewUserEmail = (TextView) findViewById(R.id.tv_user_email);
        mTextViewUserMobileNumber = (TextView) findViewById(R.id.tv_user_mobile_number);
        mTextViewUserPassword = (TextView) findViewById(R.id.tv_user_password);
        mTextViewAppVersion = (TextView) findViewById(R.id.tv_app_version);
    }

    private void loadUserDataFromSharedPref() {
        mId = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID);
        mFirstName = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_FIRST_NAME);
        mLastName = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_LAST_NAME);
        mEmail = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_EMAIL);
        mMobileNumber = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_MOBILE_NUMBER);

        mTextViewUserName.setText(mFirstName + " " + mLastName);
        mTextViewUserEmail.setText(mEmail);
        if (Utils.isStringEmptyOrNull(mMobileNumber)) {
            mTextViewUserMobileNumber.setText("Not added");
        } else {
            mTextViewUserMobileNumber.setText(mMobileNumber);
        }
        String userAccountType = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ACCOUNT_TYPE);
        mPassword = SharedPrefSingleton.getInstance(this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_PASSWORD);

        // Facebook user: 2
        if (userAccountType.equals("2") && Utils.isStringEmptyOrNull(mPassword)) {
            mTextViewUserPassword.setText("Set a password");
        } else {
            mTextViewUserPassword.setText("Change your password");
        }
    }

    private void onClickName() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_update_name_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.thankyou_done), null);

        final MaterialEditText materialEditTextFirstName = (MaterialEditText) dialogView.findViewById(R.id.et_first_name);
        final MaterialEditText materialEditTextLastName = (MaterialEditText) dialogView.findViewById(R.id.et_last_name);
        final TextView textViewFirstNameHelper = (TextView) dialogView.findViewById(R.id.tv_first_name_helper);
        final TextView textViewLastNameHelper = (TextView) dialogView.findViewById(R.id.tv_last_name_helper);

        materialEditTextFirstName.addTextChangedListener(new ErrorTextWatcher(textViewFirstNameHelper));
        materialEditTextLastName.addTextChangedListener(new ErrorTextWatcher(textViewLastNameHelper));

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StringFormatInvalid")
            @Override
            public void onClick(View v) {
                mFirstName = materialEditTextFirstName.getText().toString().trim();
                mLastName = materialEditTextLastName.getText().toString().trim();

                if (mFirstName.isEmpty()) {
                    Utils.showError(materialEditTextFirstName, getString(R.string.error_required, getString(R.string.shared_hint_first_name)), textViewFirstNameHelper);
                } else if (Utils.isNameValid(mFirstName) == false) {
                    Utils.showError(materialEditTextFirstName, getString(R.string.error_badly_formatted, getString(R.string.shared_hint_first_name)), textViewFirstNameHelper);
                } else if (mLastName.isEmpty()) {
                    Utils.showError(materialEditTextLastName, getString(R.string.error_required, getString(R.string.shared_hint_last_name)), textViewLastNameHelper);
                } else if (Utils.isNameValid(mLastName) == false) {
                    Utils.showError(materialEditTextLastName, getString(R.string.error_badly_formatted, getString(R.string.shared_hint_last_name)), textViewLastNameHelper);
                } else {
                    onClickDone(alertDialog, null, null);
                }
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    private void onClickEmail() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_update_email_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.thankyou_done), null);

        final MaterialEditText materialEditTextEmail = (MaterialEditText) dialogView.findViewById(R.id.et_email);
        final TextView textViewEmailHelper = (TextView) dialogView.findViewById(R.id.tv_email_helper);

        materialEditTextEmail.addTextChangedListener(new ErrorTextWatcher(textViewEmailHelper));

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StringFormatInvalid")
            @Override
            public void onClick(View v) {
                mEmail = materialEditTextEmail.getText().toString().trim();

                if (mEmail.isEmpty()) {
                    Utils.showError(materialEditTextEmail, getString(R.string.error_required, getString(R.string.shared_hint_email)), textViewEmailHelper);
                } else if (Utils.isEmailValid(mEmail) == false) {
                    Utils.showError(materialEditTextEmail, getString(R.string.error_badly_formatted, getString(R.string.shared_hint_email)), textViewEmailHelper);
                } else {
                    onClickDone(alertDialog, materialEditTextEmail, textViewEmailHelper);
                }
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    private void onClickMobileNumber() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_update_mobile_number_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.action_verify), null);

        if (!mTextViewUserMobileNumber.getText().toString().equalsIgnoreCase("Not added")) {
            builder.setNeutralButton(getString(R.string.bakers_remove_alert_dialog_button), null);
        }

        mMaterialEditTextMobileNumber = (MaterialEditText) dialogView.findViewById(R.id.et_mobile_number);
        mTextViewMobileNumberHelper = (TextView) dialogView.findViewById(R.id.tv_mobile_number_helper);

        mMaterialEditTextMobileNumber.addTextChangedListener(new ErrorTextWatcher(mTextViewMobileNumberHelper));

        mMaterialEditTextMobileNumber.setText(Constants.MOBILE_NUMBER_PREFIX);
        Selection.setSelection(mMaterialEditTextMobileNumber.getText(), mMaterialEditTextMobileNumber.getText().length());

        mMaterialEditTextMobileNumber.addTextChangedListener(new TextWatcher() {
            String text;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                text = charSequence.toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().startsWith(Constants.MOBILE_NUMBER_PREFIX)) {
                    mMaterialEditTextMobileNumber.setText(text);
                    Selection.setSelection(mMaterialEditTextMobileNumber.getText(), mMaterialEditTextMobileNumber.getText().length());
                }
            }
        });

        mAlertDialogMobileNumber = builder.create();
        mAlertDialogMobileNumber.setCancelable(true);

        mAlertDialogMobileNumber.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        mAlertDialogMobileNumber.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialogMobileNumber.dismiss();
            }
        });

        mAlertDialogMobileNumber.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMobileNumber = "remove";
                onClickDone(mAlertDialogMobileNumber, mMaterialEditTextMobileNumber, mTextViewMobileNumberHelper);
            }
        });

        mAlertDialogMobileNumber.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StringFormatInvalid")
            @Override
            public void onClick(View v) {
                mMobileNumber = mMaterialEditTextMobileNumber.getText().toString().trim();

                if (mMobileNumber.isEmpty()) {
                    Utils.showError(mMaterialEditTextMobileNumber, getString(R.string.error_required, "Mobile number"), mTextViewMobileNumberHelper);
                } else if (Utils.isMobileNumberValid(mMobileNumber) == false) {
                    Utils.showError(mMaterialEditTextMobileNumber, getString(R.string.error_badly_formatted, "Mobile number"), mTextViewMobileNumberHelper);
                } else {
                    verify();
                }
            }
        });

        mAlertDialogMobileNumber.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        mAlertDialogMobileNumber.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
        mAlertDialogMobileNumber.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    private void verify() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(SettingsActivity.this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder accountKitConfigurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);  // Use token when Yes 'Enable Client Access Token Flow'
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, accountKitConfigurationBuilder.build());
        startActivityForResult(intent, VERIFY_MOBILE_NUMBER_REQUEST_CODE);
    }

    private void onClickPassword() {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_update_password_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.thankyou_done), null);

        final TextInputLayout textInputLayoutCurrentPass = (TextInputLayout) dialogView.findViewById(R.id.til_current_pass);
        final MaterialEditText materialEditTextCurrentPass = (MaterialEditText) dialogView.findViewById(R.id.et_current_pass);
        final MaterialEditText materialEditTextNewPass = (MaterialEditText) dialogView.findViewById(R.id.et_new_pass);
        final TextView textViewCurrentPassHelper = (TextView) dialogView.findViewById(R.id.tv_current_pass_helper);
        final TextView textViewNewPassHelper = (TextView) dialogView.findViewById(R.id.tv_new_pass_helper);

        materialEditTextCurrentPass.addTextChangedListener(new ErrorTextWatcher(textViewCurrentPassHelper));
        materialEditTextNewPass.addTextChangedListener(new ErrorTextWatcher(textViewNewPassHelper));

        // Means user is Facebook user and does not have a password
        if (mTextViewUserPassword.getText().toString().equalsIgnoreCase("Set a password")) {
            textInputLayoutCurrentPass.setVisibility(View.GONE);
        }

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StringFormatInvalid")
            @Override
            public void onClick(View v) {
                String currentPass = materialEditTextCurrentPass.getText().toString().trim();
                String newPass = materialEditTextNewPass.getText().toString().trim();

                // Only check for those users who have a password
                if (textInputLayoutCurrentPass.getVisibility() == View.VISIBLE) {
                    if (currentPass.isEmpty()) {
                        Utils.showError(materialEditTextCurrentPass, getString(R.string.error_required, "Current password"), textViewCurrentPassHelper);
                    } else if (!currentPass.equals(SharedPrefSingleton.getInstance(SettingsActivity.this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_PASSWORD))) {
                        Utils.showError(materialEditTextCurrentPass, "Wrong password.", textViewCurrentPassHelper);
                    } else if (newPass.isEmpty()) {
                        Utils.showError(materialEditTextNewPass, getString(R.string.error_required, "New password"), textViewNewPassHelper);
                    } else if (Utils.isPasswordValid(newPass) == false) {
                        Utils.showError(materialEditTextNewPass, getString(R.string.error_invalid_pass), textViewNewPassHelper);
                    } else if (currentPass.equals(newPass)) {
                        Utils.showError(materialEditTextNewPass, "New password must be different than the current password.", textViewNewPassHelper);
                    } else {
                        mPassword = newPass;
                        onClickChangePassword(alertDialog);
                    }
                } else {
                    if (newPass.isEmpty()) {
                        Utils.showError(materialEditTextNewPass, getString(R.string.error_required, "New password"), textViewNewPassHelper);
                    } else if (Utils.isPasswordValid(newPass) == false) {
                        Utils.showError(materialEditTextNewPass, getString(R.string.error_invalid_pass), textViewNewPassHelper);
                    } else {
                        mPassword = newPass;
                        onClickChangePassword(alertDialog);
                    }
                }
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

//    private void onClickDeactivateYourAccount() {
//        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_deactivate_account_dialog, null);
//        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
//        builder.setView(dialogView);
//        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
//        builder.setPositiveButton(getString(R.string.deactivate_alert_dialog_button), null);
//
//        final MaterialEditText materialEditTextPass = (MaterialEditText) dialogView.findViewById(R.id.et_pass);
//        final TextView textViewPassHelper = (TextView) dialogView.findViewById(R.id.tv_pass_helper);
//
//        materialEditTextPass.addTextChangedListener(new ErrorTextWatcher(textViewPassHelper));
//
//        final AlertDialog alertDialog = builder.create();
//        alertDialog.setCancelable(true);
//
//        alertDialog.show();
//
//        /*
//         *
//         * Following code must be called after show() method of alert dialog
//         */
//
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//            }
//        });
//
//        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
//            @SuppressLint("StringFormatInvalid")
//            @Override
//            public void onClick(View v) {
//                mPassword = materialEditTextPass.getText().toString().trim();
//
//                if (mPassword.isEmpty()) {
//                    Utils.showError(materialEditTextPass, getString(R.string.error_required, getString(R.string.shared_hint_pass)), textViewPassHelper);
//                } else if (!mPassword.equals(SharedPrefSingleton.getInstance(SettingsActivity.this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_PASSWORD))) {
//                    Utils.showError(materialEditTextPass, "Wrong password or the user does not have a password.", textViewPassHelper);
//                } else {
//                    onClickDeactivate(alertDialog);
//                }
//            }
//        });
//
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
//        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorOrderStatusRejected));
//    }

    private void onClickLogout() {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.setMessage("Logging out...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_LOGOUT_USER, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                SharedPrefSingleton.getInstance(SettingsActivity.this).clearLoggedInUserData();
                                ActivityCompat.finishAffinity(SettingsActivity.this); // Clear the previous activities stack
                                startActivity(new Intent(SettingsActivity.this, LoginActivity.class));
                            } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                Toast.makeText(SettingsActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(SettingsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ID, SharedPrefSingleton.getInstance(SettingsActivity.this).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID));
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(SettingsActivity.this).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();

//        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_alert_dialog, null);
//        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AlertDialogTheme);
//        builder.setView(dialogView);
//        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
//        builder.setPositiveButton(getString(R.string.logout_alert_dialog_button), null);
//
//        TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
//        textViewMsg.setText("Are you sure you want to log out?");
//
//        final AlertDialog alertDialog = builder.create();
//        alertDialog.setCancelable(true);
//
//        alertDialog.show();
//
//        /*
//         *
//         * Following code must be called after show() method of alert dialog
//         */
//
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//            }
//        });
//
//        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                alertDialog.dismiss();
//                SharedPrefSingleton.getInstance(SettingsActivity.this).clearLoggedInUserData();
//                ActivityCompat.finishAffinity(SettingsActivity.this); // Clear the previous activities stack
//                startActivity(new Intent(SettingsActivity.this, LoginActivity.class));
//            }
//        });
//
//        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSecondaryText));
//        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
    }

    private void onClickDone(final AlertDialog doneAlertDialog, final MaterialEditText materialEditText, final TextView textViewHelper) {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.show();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                update(doneAlertDialog, materialEditText, textViewHelper);
                return null;
            }
        }.execute();
    }

    private void update(final AlertDialog doneAlertDialog, final MaterialEditText materialEditText, final TextView textViewHelper) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_UPDATE_ACCOUNT, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        String id = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                        String fn = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                        String ln = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                        String mobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                        String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                        String password = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PASSWORD);
                        String accountType = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_TYPE);

                        SharedPrefSingleton.getInstance(SettingsActivity.this).saveLoggedInUserData(id, fn, ln, mobileNumber,
                                email, password, accountType);

                        doneAlertDialog.dismiss();
                        loadUserDataFromSharedPref();
                    } else {
                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);

                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && (errorCode.equals("-2") || errorCode.equals("-3"))) {
                            if (materialEditText != null && textViewHelper != null) {
                                if (rootJsonObject.getString(ApiConstants.KEY_RESPONSE).contains("mobile")) {
                                    mMaterialEditTextMobileNumber.setEnabled(true);
                                    Selection.setSelection(mMaterialEditTextMobileNumber.getText(), mMaterialEditTextMobileNumber.getText().length());
                                    Utils.showError(mMaterialEditTextMobileNumber, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), mTextViewMobileNumberHelper);
                                } else {
                                    Utils.showError(materialEditText, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                            textViewHelper);
                                }
                            }
                        } else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
                            Toast.makeText(SettingsActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(SettingsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_ID, mId);
                params.put(ApiConstants.PARAM_FIRST_NAME, mFirstName);
                params.put(ApiConstants.PARAM_LAST_NAME, mLastName);
                params.put(ApiConstants.PARAM_EMAIL, mEmail.toLowerCase());
                params.put(ApiConstants.PARAM_MOBILE_NUMBER, mMobileNumber);
                params.put(ApiConstants.PARAM_PASSWORD, mPassword);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void onClickChangePassword(final AlertDialog passwordAlertDialog) {
        if (!Utils.isNetworkAvailable(getApplicationContext())) {
            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.show();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                changePassword(passwordAlertDialog);
                return null;
            }
        }.execute();
    }

    private void changePassword(final AlertDialog passwordAlertDialog) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_CHANGE_PASSWORD, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                mProgressDialog.dismiss();
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        String id = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                        String fn = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                        String ln = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                        String mobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                        String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                        String password = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PASSWORD);
                        String accountType = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_TYPE);

                        SharedPrefSingleton.getInstance(SettingsActivity.this).saveLoggedInUserData(id, fn, ln, mobileNumber,
                                email, password, accountType);

                        passwordAlertDialog.dismiss();
                        loadUserDataFromSharedPref();
                    } else {
                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
                            Toast.makeText(SettingsActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgressDialog.dismiss();
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(SettingsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_ID, mId);
                params.put(ApiConstants.PARAM_PASSWORD, mPassword);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

//    private void onClickDeactivate(final AlertDialog deactivateAlertDialog) {
//        if (!Utils.isNetworkAvailable(getApplicationContext())) {
//            Toast.makeText(this, getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
//            // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
//            return;
//        }
//        mProgressDialog.show();
//        new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... voids) {
//                deactivate(deactivateAlertDialog);
//                return null;
//            }
//        }.execute();
//    }

//    private void deactivate(final AlertDialog deactivateAlertDialog) {
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_DEACTIVATE_REACTIVATE_ACCOUNT, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                mProgressDialog.dismiss();
//                try {
//                    JSONObject rootJsonObject = new JSONObject(response);
//                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);
//
//                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
//                        deactivateAlertDialog.dismiss();
//
//                        View dialogView = LayoutInflater.from(SettingsActivity.this).inflate(R.layout.custom_alert_dialog, null);
//                        final AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this, R.style.AlertDialogTheme);
//                        builder.setView(dialogView);
//                        builder.setPositiveButton(SettingsActivity.this.getString(R.string.ok_alert_dialog_button), null);
//
//                        String title = "Account deactivated";
//                        String msg = rootJsonObject.getString(ApiConstants.KEY_RESPONSE);
//
//                        TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
//                        if (!title.equals("") && title != null && !title.isEmpty()) {
//                            // Title
//                            TextView textViewTitle = (TextView) dialogView.findViewById(R.id.tv_title);
//                            textViewTitle.setVisibility(View.VISIBLE);
//                            textViewTitle.setText(title);
//
//                            // Message
//                            textViewMsg.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.colorSecondaryText)); // Default in XML is PrimaryTextColor
//                        }
//                        textViewMsg.setText(msg);
//
//                        final AlertDialog alertDialog = builder.create();
//                        alertDialog.setCancelable(true);
//
//                        alertDialog.show();
//
//                        /*
//                         *
//                         * Following code must be called after show() method of alert dialog
//                         */
//
//                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                alertDialog.dismiss();
//                                SharedPrefSingleton.getInstance(SettingsActivity.this).clearLoggedInUserData();
//                                ActivityCompat.finishAffinity(SettingsActivity.this); // Clear the previous activities stack
//                                startActivity(new Intent(SettingsActivity.this, WelcomeActivity.class));
//                            }
//                        });
//
//                        // Set the color of positive button
//                        // As the AlertDialogTheme is not working on API >= 25, so had to do it programmatically
//                        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.colorPrimary));
//                    } else {
//                        String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
//                        if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
//                            Toast.makeText(SettingsActivity.this, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
//                                    Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                mProgressDialog.dismiss();
//                if (error != null) {
//                    String toastMsg = "Sorry, something went wrong. Please try again.";
//                    Toast.makeText(SettingsActivity.this, toastMsg, Toast.LENGTH_SHORT).show();
//                    Log.e(LOG_TAG, error.toString());
//                }
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<>();
//                params.put(ApiConstants.PARAM_ID, mId);
//                params.put(ApiConstants.PARAM_ACCOUNT_STATUS, ApiConstants.PARAM_VALUE_DEACTIVATE);
//                return params;
//            }
//        };
//
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        VolleyRequestHandlerSingleton.getInstance(this).addToRequestQueue(stringRequest);
//    }
}

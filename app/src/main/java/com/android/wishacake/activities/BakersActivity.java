package com.android.wishacake.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.android.wishacake.R;
import com.android.wishacake.fragments.FavoriteBakersFragment;
import com.android.wishacake.fragments.NearbyBakersFragment;
import com.android.wishacake.fragments.RecentBakersFragment;
import com.android.wishacake.helpers.Constants;

public class BakersActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TabLayout mTabLayoutBakers;
    private ViewPager mViewPagerBakers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bakers);

        initViews();

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(BakersActivity.this, R.drawable.ic_arrow_back_white));

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(Constants.EXTRA_KEY_IS_BAKERS_OPENED_FROM_NAVIGATION_MENU)) {
                if (extras.getBoolean(Constants.EXTRA_KEY_IS_BAKERS_OPENED_FROM_NAVIGATION_MENU)) {
                    getSupportActionBar().setTitle(getString(R.string.action_bakers));
                }
            }
        }

        BakersViewPagerAdapter bakersViewPagerAdapter = new BakersViewPagerAdapter(getSupportFragmentManager());
        mViewPagerBakers.setAdapter(bakersViewPagerAdapter);
        mViewPagerBakers.setOffscreenPageLimit(3);
        mViewPagerBakers.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayoutBakers));
        mTabLayoutBakers.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPagerBakers));

        // Shows NEARBY initially because user will enter delivery address before switching to bakers activity
        mViewPagerBakers.setCurrentItem(2);
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mTabLayoutBakers = (TabLayout) findViewById(R.id.tl_bakers);
        mViewPagerBakers = (ViewPager) findViewById(R.id.vp_bakers);
    }

    /*
     *
     * Inner classes
     */

    private class BakersViewPagerAdapter extends FragmentPagerAdapter {

        public BakersViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = new FavoriteBakersFragment();
            } else if (position == 1) {
                fragment = new RecentBakersFragment();
            } else if (position == 2) {
                fragment = new NearbyBakersFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 3;
        }
    }
}

package com.android.wishacake.helpers;

public class ApiConstants {

    // Base URL
    private static final String BASE_URL = "http://feedpakistan.com/wishacake/v1/";
//    private static final String BASE_URL = "http://192.168.0.105/wishacake/v1/";

    // JSON response keys
    public static final String KEY_STATUS = "status";
    public static final String KEY_ERROR_CODE = "errorCode";
    public static final String KEY_RESPONSE = "response";

    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_ERROR = "error";

    // POST Params
    public static final String PARAM_ID = "id";
    public static final String PARAM_FIREBASE_TOKEN = "firebase_token";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_USER_IDS = "user_ids";
    public static final String PARAM_BAKER_ID = "baker_id";
    public static final String PARAM_FIRST_NAME = "first_name";
    public static final String PARAM_LAST_NAME = "last_name";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_EMAIL_VERIFIED_STATUS = "email_verified_status";
    public static final String PARAM_MOBILE_NUMBER = "mobile_number";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_ACCOUNT_TYPE = "account_type";
    public static final String PARAM_ACCOUNT_STATUS = "account_status";
    public static final String PARAM_IMAGE = "image";
    public static final String PARAM_LOCATION_NAME = "location_name";
    public static final String PARAM_LOCATION_ADDRESS = "location_address";
    public static final String PARAM_LOCATION_LATITUDE = "location_latitude";
    public static final String PARAM_LOCATION_LONGITUDE = "location_longitude";
    public static final String PARAM_SLIDER_IMAGE1 = "slider_image1";
    public static final String PARAM_SLIDER_IMAGE2 = "slider_image2";
    public static final String PARAM_SLIDER_IMAGE3 = "slider_image3";
    public static final String PARAM_RATING = "rating";
    public static final String PARAM_RATED_STATUS = "rated_status";
    public static final String PARAM_ACTIVE_STATUS = "active_status";
    public static final String PARAM_FAVORITE_STATUS = "favorite_status";
    public static final String PARAM_FAVORITE_ID = "favorite_id";
    public static final String PARAM_REVIEW = "review";
    public static final String PARAM_RELEVANCE = "relevance";
    public static final String PARAM_NEAREST_FIRST = "nearest_first";
    public static final String PARAM_CREATED_AT = "created_at";
    public static final String PARAM_MODIFIED_AT = "modified_at";
    public static final String PARAM_ORDER_BY = "order_by";
    public static final String PARAM_ORDER_ID = "order_id";
    public static final String PARAM_ORDER_FOR = "order_for";
    public static final String PARAM_IMAGE1 = "image1";
    public static final String PARAM_IMAGE2 = "image2";
    public static final String PARAM_IMAGE3 = "image3";
    public static final String PARAM_IMAGE4 = "image4";
    public static final String PARAM_IMAGE5 = "image5";
    public static final String PARAM_IMAGE6 = "image6";
    public static final String PARAM_IMAGE7 = "image7";
    public static final String PARAM_IMAGE8 = "image8";
    public static final String PARAM_IMAGE9 = "image9";
    public static final String PARAM_IMAGE10 = "image10";
    public static final String PARAM_STATUS = "status";
    public static final String PARAM_QUANTITY = "quantity";
    public static final String PARAM_POUNDS = "pounds";
    public static final String PARAM_DESCRIPTION = "description";
    public static final String PARAM_TITLE = "title";
    public static final String PARAM_CONTACT_NUMBER = "contact_number";
    public static final String PARAM_DELIVERY_LOCATION = "delivery_location";
    public static final String PARAM_DELIVERY_ADDRESS = "delivery_address";
    public static final String PARAM_DELIVERY_DATE = "delivery_date";
    public static final String PARAM_DELIVERY_TIME = "delivery_time";
    public static final String PARAM_SUBTOTAL = "subtotal";
    public static final String PARAM_DELIVERY_CHARGES = "delivery_charges";
    public static final String PARAM_TOTAL_AMOUNT = "total_amount";
    public static final String PARAM_PAYMENT_METHOD = "payment_method";
    public static final String PARAM_CARD_NUMBER = "card_number";
    public static final String PARAM_EXPIRATION_DATE = "expiration_date";
    public static final String PARAM_CVV = "cvv";
    public static final String PARAM_CARDHOLDER_NAME = "cardholder_name";
    public static final String PARAM_COUNTRY = "country";
    public static final String PARAM_REJECTED_REASON = "rejected_reason";
    public static final String PARAM_ORDER_RATING = "order_rating";

    // Post Params values
    public static final String PARAM_VALUE_DEACTIVATE = "Deactivate";
    public static final String PARAM_VALUE_REACTIVATE = "Reactivate";
    public static final String PARAM_VALUE_FAVORITE = "Favorite";
    public static final String PARAM_VALUE_UNFAVORITE = "Unfavorite";
    public static final String PARAM_VALUE_CASH = "Cash";
    public static final String PARAM_VALUE_CREDIT_DEBIT_CARD = "Card";
    public static final String PARAM_VALUE_PENDING = "Pending";
    public static final String PARAM_VALUE_ACCEPTED = "Accepted";
    public static final String PARAM_VALUE_REJECTED = "Rejected";
    public static final String PARAM_VALUE_CONFIRMED = "Confirmed";
    public static final String PARAM_VALUE_CANCELLED = "Cancelled";
    public static final String PARAM_VALUE_ONGOING = "Ongoing";
    public static final String PARAM_VALUE_COMPLETED = "Completed";
    public static final String PARAM_VALUE_USER = "User";

    // API URLs
    public static final String API_SIGN_UP = BASE_URL + "create-user.php";
    public static final String API_SIGN_UP_WTIH_FACEBOOK = BASE_URL + "create-fb-user.php";
    public static final String API_LOGIN = BASE_URL + "login-user.php";
    public static final String API_FORGOT_PASSWORD = BASE_URL + "forgot-password-user.php";
    public static final String API_UPDATE_ACCOUNT = BASE_URL + "update-user.php";
    public static final String API_CHANGE_PASSWORD = BASE_URL + "change-password-user.php";
    public static final String API_DEACTIVATE_REACTIVATE_ACCOUNT = BASE_URL + "deactivate-reactivate-account-user.php";
    public static final String API_GET_BAKERS = BASE_URL + "get-bakers.php";
    public static final String API_GET_FAVORITE_BAKERS = BASE_URL + "get-favorite-bakers.php";
    public static final String API_GET_RECENT_BAKERS = BASE_URL + "get-recent-bakers.php";
    public static final String API_GET_RELEVANT_BAKERS = BASE_URL + "get-relevant-bakers.php";
    public static final String API_FAVORITE_UNFAVORITE_BAKER = BASE_URL + "favorite-unfavorite-baker.php";
    public static final String API_VALIDATE_ORDER = BASE_URL + "validate-order.php";
    public static final String API_PLACE_ORDER = BASE_URL + "create-order.php";
    public static final String API_UPDATE_ORDER_STATUS = BASE_URL + "update-order-status.php";
    public static final String API_GET_REVIEWS = BASE_URL + "get-reviews.php";
    public static final String API_CREATE_REVIEW = BASE_URL + "create-review.php";
    public static final String API_REPORT_A_PROBLEM = BASE_URL + "report-a-problem.php";
    public static final String API_LOGOUT_USER = BASE_URL + "logout-user.php";
    public static final String API_RESEND_VERIFICATION_EMAIL = BASE_URL + "resend-verification-email.php";
    public static final String API_GET_ORDERS = BASE_URL + "get-orders.php";
    public static final String API_GET_SINGLE_ORDER = BASE_URL + "get-single-order.php";
    public static final String API_GET_FEATURED_CAKES = BASE_URL + "get-featured-cakes.php";
}

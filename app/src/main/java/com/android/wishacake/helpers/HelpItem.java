package com.android.wishacake.helpers;

import android.net.Uri;

public class HelpItem {

    private Uri mImageUri;
    private String mFileName;

    public HelpItem(Uri imageUri, String fileName) {
        mImageUri = imageUri;
        mFileName = fileName;
    }

    public Uri getImageUri() {
        return mImageUri;
    }

    public void setImageUri(Uri imageUri) {
        mImageUri = imageUri;
    }

    public String getFileName() {
        return mFileName;
    }

    public void setFileName(String fileName) {
        mFileName = fileName;
    }
}

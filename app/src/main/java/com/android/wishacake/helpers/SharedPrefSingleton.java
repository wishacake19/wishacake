package com.android.wishacake.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SharedPrefSingleton {

    private static SharedPrefSingleton sInstance;
    private static Context sContext;

    private SharedPrefSingleton(Context context) {
        sContext = context;
    }

    public static synchronized SharedPrefSingleton getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SharedPrefSingleton(context);
        }
        return sInstance;
    }

    public void saveLoggedInUserData(String id, String firstName, String lastName, String mobileNumber, String email, String password, String accountType) {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.SHARED_PREF_KEY_USER_ID, id);
        editor.putString(Constants.SHARED_PREF_KEY_USER_FIRST_NAME, firstName);
        editor.putString(Constants.SHARED_PREF_KEY_USER_LAST_NAME, lastName);
        editor.putString(Constants.SHARED_PREF_KEY_USER_MOBILE_NUMBER, mobileNumber);
        editor.putString(Constants.SHARED_PREF_KEY_USER_EMAIL, email);
        editor.putString(Constants.SHARED_PREF_KEY_USER_PASSWORD, password);
        editor.putString(Constants.SHARED_PREF_KEY_USER_ACCOUNT_TYPE, accountType);
        editor.apply();
    }

    public boolean isUserLoggedIn() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_USER, Context.MODE_PRIVATE);
        if (preferences.getString(Constants.SHARED_PREF_KEY_USER_ID, null) != null) {
            return true;
        }
        return false;
    }

    public void clearLoggedInUserData() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.apply();
    }

    public void saveCardData(String cardNumber, String expDate, String securityCode, String cardholderName, String country) {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARD_NUMBER, cardNumber);
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARD_EXP_DATE, expDate);
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARD_SECURITY_CODE, securityCode);
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARDHOLDER_NAME, cardholderName);
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARD_COUNTRY, country);
        editor.apply();
    }

    public boolean isCardAdded() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_USER, Context.MODE_PRIVATE);
        if (preferences.getString(Constants.SHARED_PREF_KEY_USER_CARD_NUMBER, null) != null) {
            return true;
        }
        return false;
    }

    public void removeCard() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_USER, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARD_NUMBER, null);
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARD_EXP_DATE, null);
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARD_SECURITY_CODE, null);
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARDHOLDER_NAME, null);
        editor.putString(Constants.SHARED_PREF_KEY_USER_CARD_COUNTRY, null);
        editor.apply();
    }

    public HashMap<String, String> getLoggedInUserData() {
        SharedPreferences preferences = sContext.getSharedPreferences(Constants.SHARED_PREF_NAME_USER, Context.MODE_PRIVATE);
        HashMap<String, String> userData = new HashMap<>();
        userData.put(Constants.SHARED_PREF_KEY_USER_ID, preferences.getString(Constants.SHARED_PREF_KEY_USER_ID, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_FIRST_NAME, preferences.getString(Constants.SHARED_PREF_KEY_USER_FIRST_NAME, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_LAST_NAME, preferences.getString(Constants.SHARED_PREF_KEY_USER_LAST_NAME, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_EMAIL, preferences.getString(Constants.SHARED_PREF_KEY_USER_EMAIL, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_MOBILE_NUMBER, preferences.getString(Constants.SHARED_PREF_KEY_USER_MOBILE_NUMBER, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_PASSWORD, preferences.getString(Constants.SHARED_PREF_KEY_USER_PASSWORD, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_ACCOUNT_TYPE, preferences.getString(Constants.SHARED_PREF_KEY_USER_ACCOUNT_TYPE, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_CURRENT_PAYMENT_METHOD, preferences.getString(Constants.SHARED_PREF_KEY_USER_CURRENT_PAYMENT_METHOD, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_CARD_NUMBER, preferences.getString(Constants.SHARED_PREF_KEY_USER_CARD_NUMBER, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_CARD_EXP_DATE, preferences.getString(Constants.SHARED_PREF_KEY_USER_CARD_EXP_DATE, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_CARD_SECURITY_CODE, preferences.getString(Constants.SHARED_PREF_KEY_USER_CARD_SECURITY_CODE, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_CARDHOLDER_NAME, preferences.getString(Constants.SHARED_PREF_KEY_USER_CARDHOLDER_NAME, null));
        userData.put(Constants.SHARED_PREF_KEY_USER_CARD_COUNTRY, preferences.getString(Constants.SHARED_PREF_KEY_USER_CARD_COUNTRY, null));
        return userData;
    }
}
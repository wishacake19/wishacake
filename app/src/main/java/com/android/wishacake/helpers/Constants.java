package com.android.wishacake.helpers;

public class Constants {

    // Intent extra keys
    public static final String EXTRA_KEY_BAKER_ID = "extra-key-baker-id";
    public static final String EXTRA_KEY_BAKER_NAME = "extra-key-baker-name";
    public static final String EXTRA_KEY_BAKER_IMAGE = "extra-key-baker-image";
    public static final String EXTRA_KEY_BAKER_RATING = "extra-key-baker-rating";
    public static final String EXTRA_KEY_BAKER_ADDRESS = "extra-key-baker-address";
    public static final String EXTRA_KEY_BAKER_ACTIVE_STATUS = "extra-key-baker-active-status";
    public static final String EXTRA_KEY_BAKER_MOBILE_NUMBER = "extra-key-baker-mobile-number";
    public static final String EXTRA_KEY_BAKER_EMAIL = "extra-key-baker-email";
    public static final String EXTRA_KEY_BAKER_SLIDER_IMAGES = "extra-key-baker-slider-images";
    public static final String EXTRA_KEY_BAKER_DISTANCE = "extra-key-baker-distance";
    public static final String EXTRA_KEY_BAKER_IS_FAVORITE = "extra-key-baker-is-favorite";
    public static final String EXTRA_KEY_BAKER_FAVORITE_ID = "extra-key-baker-favorite-id";
    public static final String EXTRA_KEY_FULLSCREEN_IMAGE = "extra-key-fullscreen-image";
    public static final String EXTRA_KEY_IS_SIGN_UP_FOR_GUEST_USER = "extra-key-is-sign-up-for-guest-user";
    public static final String EXTRA_KEY_IS_BAKERS_OPENED_FROM_NAVIGATION_MENU = "extra-key-is-bakers-opened-from-navigation-menu";
    public static final String EXTRA_KEY_ORDER_IMAGES = "extra-key-order-images";
    public static final String EXTRA_KEY_ORDER_DATE = "extra-key-order-date";
    public static final String EXTRA_KEY_ORDER_STATUS = "extra-key-order-status";
    public static final String EXTRA_KEY_ORDER_ID = "extra-key-order-id";
    public static final String EXTRA_KEY_ORDER_QUANTITY = "extra-key-order-quantity";
    public static final String EXTRA_KEY_ORDER_POUNDS = "extra-key-order-pounds";
    public static final String EXTRA_KEY_ORDER_DESCRIPTION = "extra-key-order-description";
    public static final String EXTRA_KEY_ORDER_CONTACT_NUMBER = "extra-key-order-contact-number";
    public static final String EXTRA_KEY_ORDER_DELIVERY_LOCATION = "extra-key-order-delivery-location";
    public static final String EXTRA_KEY_ORDER_DELIVERY_ADDRESS = "extra-key-order-delivery-address";
    public static final String EXTRA_KEY_ORDER_DELIVERY_DATE = "extra-key-order-delivery-date";
    public static final String EXTRA_KEY_ORDER_DELIVERY_TIME = "extra-key-order-delivery-time";
    public static final String EXTRA_KEY_ORDER_SUBTOTAL = "extra-key-order-subtotal";
    public static final String EXTRA_KEY_ORDER_DELIVERY_CHARGES = "extra-key-order-delivery-charges";
    public static final String EXTRA_KEY_ORDER_AMOUNT = "extra-key-order-amount";
    public static final String EXTRA_KEY_ORDER_PAYMENT_METHOD = "extra-key-order-payment-method";
    public static final String EXTRA_KEY_ORDER_REJECTED_REASON = "extra-key-order-rejected-reason";
    public static final String EXTRA_KEY_ORDER_ORDER_RATING = "extra-key-order-order-rating";
    public static final String EXTRA_KEY_ORDER_RATED_STATUS = "extra-key-order-rated-status";
    public static final String EXTRA_KEY_ORDER_DATA_LOAD_FROM_SERVER = "extra-key-order-data-load-from-server";
    public static final String EXTRA_KEY_IS_HELP_OPENED_FOR_REPORTING = "extra-key-is-help-opened-for-reporting";
    public static final String EXTRA_KEY_FEATURED_CAKE_IMAGES = "extra-key-featured-cake-images";
    public static final String EXTRA_KEY_FEATURED_CAKE_TITLE = "extra-key-featured-cake-title";
    public static final String EXTRA_KEY_FEATURED_CAKE_DESCRIPTION = "extra-key-featured-cake-desc";
    public static final String EXTRA_KEY_IS_ADD_CARD_OPENED_FROM_CHECKOUT = "extra-key-is-add-card-opened-from-checkout";
    public static final String EXTRA_KEY_IS_ADD_CARD_ADDED = "extra-key-is-add-card-added";
    public static final String EXTRA_KEY_TITLE_PREVIEW_IMAGES_ACTIVITY = "extra-key-title-preview-images";
    public static final String EXTRA_KEY_IS_PREVIEW_IMAGES_OPEN_FROM_UNITY = "extra-key-is-preview-images-open-from-unity";

    // Shared pref constants
    public static final String SHARED_PREF_NAME_USER = "shared-pref-user";
    public static final String SHARED_PREF_KEY_USER_ID = "user-id";
    public static final String SHARED_PREF_KEY_USER_FIRST_NAME = "user-first-name";
    public static final String SHARED_PREF_KEY_USER_LAST_NAME = "user-last-name";
    public static final String SHARED_PREF_KEY_USER_EMAIL = "user-email";
    public static final String SHARED_PREF_KEY_USER_MOBILE_NUMBER = "user-mobile-number";
    public static final String SHARED_PREF_KEY_USER_PASSWORD = "user-password";
    public static final String SHARED_PREF_KEY_USER_ACCOUNT_TYPE = "user-account-type";
    public static final String SHARED_PREF_KEY_USER_CURRENT_PAYMENT_METHOD = "user-current-payment-method";
    public static final String SHARED_PREF_KEY_USER_CARD_NUMBER = "user-card-number";
    public static final String SHARED_PREF_KEY_USER_CARD_EXP_DATE = "user-card-expiration-date";
    public static final String SHARED_PREF_KEY_USER_CARD_SECURITY_CODE = "user-card-security-code";
    public static final String SHARED_PREF_KEY_USER_CARDHOLDER_NAME = "user-cardholder-name";
    public static final String SHARED_PREF_KEY_USER_CARD_COUNTRY = "user-card-country";

    // Other constants
    public static final int DEFAULT_EDITTEXT_HINT_COLOR = 1140850688;
    public static final int ORDER_IMAGES_SIZE = 10;
    public static final double NEARBY_KM_VALUE = 10;
    public static final String MASTER_CARD = "master";
    public static final String VISA = "visa";
    public static final String AMERICAN_EXPRESS = "american-express";
    public static final String MOBILE_NUMBER_PREFIX = "+92";
}

package com.android.wishacake.helpers;

import android.app.Activity;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

public class SliderTimer extends TimerTask {

    private Activity mActivity;
    private ViewPager mViewPager;
    private List<Integer> mImageResIds = new ArrayList<>();

    public SliderTimer(Activity activity, ViewPager viewPager, List<Integer> imageResIds) {
        mActivity = activity;
        mViewPager = viewPager;
        mImageResIds = imageResIds;
    }

    @Override
    public void run() {
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mViewPager.getCurrentItem() < mImageResIds.size() - 1) {
                    mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1);
                } else {
                    mViewPager.setCurrentItem(0);
                }
            }
        });
    }
}

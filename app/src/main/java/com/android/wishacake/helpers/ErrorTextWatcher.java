package com.android.wishacake.helpers;

import android.text.Editable;
import android.widget.TextView;

import com.android.wishacake.utilities.Utils;

public class ErrorTextWatcher implements android.text.TextWatcher {

    private TextView mTextViewHelper;

    public ErrorTextWatcher(TextView textViewHelper) {
        mTextViewHelper = textViewHelper;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Utils.hideError(mTextViewHelper);
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}

package com.android.wishacake.helpers;

import java.util.Calendar;

public class ExpirationDateValidator {

    /**
     * Maximum amount of years in advance that a credit card expiration date should be trusted to be
     * valid. This is mostly used if the current date is towards the end of the century and the
     * expiration date is at the start of the following one.
     * <p/>
     * Ex. Current year is 2099, Expiration date is "01/01". The YY is "less than" the current year,
     * but since the difference is less than {@code MAXIMUM_VALID_YEAR_DIFFERENCE}, it should still
     * be trusted to be valid client-side.
     */

    private static final int MAXIMUM_VALID_YEAR_DIFFERENCE = 20;

    private final Calendar mCalendar = Calendar.getInstance();

    public boolean isValid(String month, String year) {
        return isValidHelper(month, year);
    }

    /**
     * Helper for determining whether a date is a valid credit card expiry date.
     *
     * @param monthString Two-digit month
     * @param yearString  Two or four digit year
     * @return Whether the date is a valid credit card expiry date.
     */

    private boolean isValidHelper(String monthString, String yearString) {
        int month = Integer.parseInt(monthString);
        if (month < 1 || month > 12) {
            return false;
        }

        int year = Integer.parseInt(yearString);
        int currentYear = getCurrentTwoDigitYear();

        if (year == currentYear && month < getCurrentMonth()) {
            return false;
        }
        if (year < currentYear) {
            // account for century-overlapping in 2-digit year representations
            int adjustedYear = year + 100;
            if (adjustedYear - currentYear > MAXIMUM_VALID_YEAR_DIFFERENCE) {
                return false;
            }
        }
        if (year > currentYear + MAXIMUM_VALID_YEAR_DIFFERENCE) {
            return false;
        }
        return true;
    }

    /**
     * {@link java.util.Calendar#MONTH} is 0-prefixed. Add {@code 1} to align it with visualized expiration
     * dates.
     */

    private int getCurrentMonth() {
        return mCalendar.get(Calendar.MONTH) + 1;
    }

    /**
     * {@link java.util.Calendar#YEAR} is the full, 4-digit year. Take the trailing two digits to align it
     * with visualized expiration dates.
     */

    private int getCurrentTwoDigitYear() {
        return mCalendar.get(Calendar.YEAR) % 100;
    }
}

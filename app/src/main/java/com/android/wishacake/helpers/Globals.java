package com.android.wishacake.helpers;

import android.location.Location;
import android.location.LocationManager;

public class Globals {

    public Globals() {
    }

    public static boolean sIsGuestUserLoggedIn = false;
    public static boolean sIsSignUpOpenedFromLogin = false;
    public static boolean sIsSliderFromMainActivity = false;
    public static boolean sIsDeliveryLocationObtainedFromUser = false;
    public static Location sUserCurrentLocation = new Location(LocationManager.GPS_PROVIDER);
    public static String sUserDeliveryLocationMap = "";
    public static int sFilterSelectedItemIndex = 0, sFilterPreviousSelectedItemIndex = 0; // Bakers Sort by
    public static boolean sIsFilterApplied = false;
    public static boolean sIsCardAddedFromCheckout = false;
}

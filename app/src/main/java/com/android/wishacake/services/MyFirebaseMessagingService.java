package com.android.wishacake.services;

import android.content.Intent;
import android.util.Log;

import com.android.wishacake.activities.MainActivity;
import com.android.wishacake.models.Notification;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.NotificationUtils;
import com.android.wishacake.utilities.Utils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private static final String IMAGE = "image";
    private static final String ACTION = "action";
    private static final String ACTION_DESTINATION = "action_destination";
    private static final String ORDER_ID = "order_id";
    private static final String WHEN = "when";

    private static final String LOG_TAG = MyFirebaseMessagingService.class.getSimpleName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.v(LOG_TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload
        if (remoteMessage.getData().size() > 0) {
            Log.v(LOG_TAG, "Message data payload: " + remoteMessage.getData());
            Map<String, String> data = remoteMessage.getData();
            handleNotificationWithData(data);
        }
        // Check if message contains a notification payload.
        else if (remoteMessage.getNotification() != null) {
            Log.v(LOG_TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            handleSimpleNotification(remoteMessage.getNotification());
        }
    }

    private void handleNotificationWithData(Map<String, String> data) {
        String title, message, image, action, actionDestination, orderStatus, orderId;
        long when;

        title = data.containsKey(TITLE) ? data.get(TITLE) : "";
        message = data.containsKey(MESSAGE) ? data.get(MESSAGE) : "";
        image = data.containsKey(IMAGE) ? data.get(IMAGE) : "";
        action = data.containsKey(ACTION) ? data.get(ACTION) : "";
        actionDestination = data.containsKey(ACTION_DESTINATION) ? data.get(ACTION_DESTINATION) : "";
        orderId = data.containsKey(ORDER_ID) ? data.get(ORDER_ID) : "";
        when = data.containsKey(WHEN) ? DateTimeUtils.convertStringDateToLong("yyyy-MM-dd HH:mm:ss", data.get(WHEN)) : 0;

        Log.d(LOG_TAG, "title: " + title);
        Log.d(LOG_TAG, "message: " + message);
        if (!Utils.isStringEmptyOrNull(image)) {
            Log.d(LOG_TAG, "image: " + image);
        }
        Log.d(LOG_TAG, "action: " + action);
        Log.d(LOG_TAG, "actionDestination: " + actionDestination);
        if (!Utils.isStringEmptyOrNull(orderId)) {
            Log.d(LOG_TAG, "orderId: " + orderId);
        }

        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        Notification notification = new Notification(title, message, image, action, actionDestination, orderId, when);
        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);
        notificationUtils.displayNotification(notification, resultIntent);
    }

    private void handleSimpleNotification(RemoteMessage.Notification RemoteMsgNotification) {
        String title = RemoteMsgNotification.getTitle();
        String message = RemoteMsgNotification.getBody();
        Notification notification = new Notification(title, message);

        Intent resultIntent = new Intent(getApplicationContext(), MainActivity.class);

        NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
        notificationUtils.displayNotification(notification, resultIntent);
    }
}

package com.android.wishacake.models;

import java.util.List;

public class FeaturedCake {

    private List<String> mImageUrls;
    private String mTitle, mDescription;

    public FeaturedCake(List<String> imageUrls, String title, String description) {
        mImageUrls = imageUrls;
        mTitle = title;
        mDescription = description;
    }

    public List<String> getImageUrls() {
        return mImageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        mImageUrls = imageUrls;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }
}

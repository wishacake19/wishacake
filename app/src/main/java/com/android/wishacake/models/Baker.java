package com.android.wishacake.models;

import java.util.List;

public class Baker {

    private String mId, mImageUrl, mFirstName, mLastName, mAddress, mMobileNumber, mEmail;
    private List<String> mSliderImageUrls;
    private double mRating, mDistance;
    private boolean mIsFavorite;
    private String mFavoriteId, mActiveStatus;

    public Baker(String id, String imageUrl, String firstName, String lastName, String address, String mobileNumber,
                 String email, List<String> sliderImageUrls, double rating, double distance, boolean isFavorite, String favoriteId) {
        mId = id;
        mImageUrl = imageUrl;
        mFirstName = firstName;
        mLastName = lastName;
        mAddress = address;
        mMobileNumber = mobileNumber;
        mEmail = email;
        mSliderImageUrls = sliderImageUrls;
        mRating = rating;
        mDistance = distance;
        mIsFavorite = isFavorite;
        mFavoriteId = favoriteId;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public String getMobileNumber() {
        return mMobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        mMobileNumber = mobileNumber;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public List<String> getSliderImageUrls() {
        return mSliderImageUrls;
    }

    public void setSliderImageUrls(List<String> sliderImageUrls) {
        mSliderImageUrls = sliderImageUrls;
    }

    public double getRating() {
        return mRating;
    }

    public void setRating(double rating) {
        mRating = rating;
    }

    public double getDistance() {
        return mDistance;
    }

    public void setDistance(double distance) {
        mDistance = distance;
    }

    public boolean isFavorite() {
        return mIsFavorite;
    }

    public void setFavorite(boolean favorite) {
        mIsFavorite = favorite;
    }

    public String getFavoriteId() {
        return mFavoriteId;
    }

    public void setFavoriteId(String favoriteId) {
        mFavoriteId = favoriteId;
    }

    public String getActiveStatus() {
        return mActiveStatus;
    }

    public void setActiveStatus(String activeStatus) {
        mActiveStatus = activeStatus;
    }
}

package com.android.wishacake.models;

public class Review {

    private String mReviewBy, mReview;
    private double mRating;
    private String mDate;

    public Review(String reviewBy, String review, double rating, String date) {
        mReviewBy = reviewBy;
        mReview = review;
        mRating = rating;
        mDate = date;
    }

    public String getReviewBy() {
        return mReviewBy;
    }

    public void setReviewBy(String reviewBy) {
        mReviewBy = reviewBy;
    }

    public String getReview() {
        return mReview;
    }

    public void setReview(String review) {
        mReview = review;
    }

    public double getRating() {
        return mRating;
    }

    public void setRating(double rating) {
        mRating = rating;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }
}

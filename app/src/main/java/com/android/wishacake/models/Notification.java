package com.android.wishacake.models;

public class Notification {

    private String mTitle, mMessage, mImageUrl, mAction, mActionDestination, mOrderId;
    private long mWhen;

    public Notification(String title, String message) {
        mTitle = title;
        mMessage = message;
    }

    public Notification(String title, String message, String imageUrl, String action, String actionDestination,
                        String orderId, long when) {
        mTitle = title;
        mMessage = message;
        mImageUrl = imageUrl;
        mAction = action;
        mActionDestination = actionDestination;
        mOrderId = orderId;
        mWhen = when;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getMessage() {
        return mMessage;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public String getAction() {
        return mAction;
    }

    public String getActionDestination() {
        return mActionDestination;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public long getWhen() {
        return mWhen;
    }
}

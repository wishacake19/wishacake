package com.android.wishacake.models;

import java.util.List;

public class Order {

    private String mId;
    private List<String> mImageUrls;
    private long mDate;
    private String mStatus, mQuantity, mPounds, mDescription, mContactNumber, mDeliveryLocation, mDeliveryAddress,
            mDeliveryDate, mDeliveryTime;
    private double mSubTotal, mDeliveryCharges, mTotalAmount;
    private String mPaymentMethod, mRejectedReason;
    private double mOrderRating;
    private String mRatedStatus;
    private Baker mBaker;

    public Order(long date, String status) {
        mDate = date;
        mStatus = status;
    }

    public Order(String id, List<String> imageUrls, long date, String status, String quantity,
                 String pounds, String description, String contactNumber, String deliveryLocation, String deliveryAddress, String deliveryDate,
                 String deliveryTime, double subTotal, double deliveryCharges, double totalAmount, String paymentMethod,
                 String rejectedReason, double orderRating, String ratedStatus, Baker baker) {
        mId = id;
        mImageUrls = imageUrls;
        mDate = date;
        mStatus = status;
        mQuantity = quantity;
        mPounds = pounds;
        mDescription = description;
        mContactNumber = contactNumber;
        mDeliveryLocation = deliveryLocation;
        mDeliveryAddress = deliveryAddress;
        mDeliveryDate = deliveryDate;
        mDeliveryTime = deliveryTime;
        mSubTotal = subTotal;
        mDeliveryCharges = deliveryCharges;
        mTotalAmount = totalAmount;
        mPaymentMethod = paymentMethod;
        mRejectedReason = rejectedReason;
        mOrderRating = orderRating;
        mRatedStatus = ratedStatus;
        mBaker = baker;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public List<String> getImageUrls() {
        return mImageUrls;
    }

    public void setImageUrls(List<String> imageUrls) {
        mImageUrls = imageUrls;
    }

    public long getDate() {
        return mDate;
    }

    public void setDate(long date) {
        mDate = date;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getPounds() {
        return mPounds;
    }

    public void setPounds(String pounds) {
        mPounds = pounds;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getContactNumber() {
        return mContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        mContactNumber = contactNumber;
    }

    public String getDeliveryLocation() {
        return mDeliveryLocation;
    }

    public void setDeliveryLocation(String deliveryLocation) {
        mDeliveryLocation = deliveryLocation;
    }

    public String getDeliveryAddress() {
        return mDeliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        mDeliveryAddress = deliveryAddress;
    }

    public String getDeliveryDate() {
        return mDeliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        mDeliveryDate = deliveryDate;
    }

    public String getDeliveryTime() {
        return mDeliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        mDeliveryTime = deliveryTime;
    }

    public double getSubTotal() {
        return mSubTotal;
    }

    public void setSubTotal(double subTotal) {
        mSubTotal = subTotal;
    }

    public double getDeliveryCharges() {
        return mDeliveryCharges;
    }

    public void setDeliveryCharges(double deliveryCharges) {
        mDeliveryCharges = deliveryCharges;
    }

    public double getTotalAmount() {
        return mTotalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        mTotalAmount = totalAmount;
    }

    public String getPaymentMethod() {
        return mPaymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        mPaymentMethod = paymentMethod;
    }

    public String getRejectedReason() {
        return mRejectedReason;
    }

    public void setRejectedReason(String rejectedReason) {
        mRejectedReason = rejectedReason;
    }

    public double getOrderRating() {
        return mOrderRating;
    }

    public void setOrderRating(double orderRating) {
        mOrderRating = orderRating;
    }

    public Baker getBaker() {
        return mBaker;
    }

    public void setBaker(Baker baker) {
        mBaker = baker;
    }

    public String getRatedStatus() {
        return mRatedStatus;
    }

    public void setRatedStatus(String ratedStatus) {
        mRatedStatus = ratedStatus;
    }
}

package com.android.wishacake.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.adapters.BakersAdapter;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.models.Baker;
import com.android.wishacake.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.wishacake.helpers.Globals.sIsFilterApplied;
import static com.android.wishacake.helpers.Globals.sFilterPreviousSelectedItemIndex;
import static com.android.wishacake.helpers.Globals.sFilterSelectedItemIndex;
import static com.android.wishacake.utilities.Utils.getAppliedFilterParamValue;

public class NearbyBakersFragment extends Fragment implements View.OnClickListener {

    private List<Baker> mBakers;

    private RelativeLayout mRelativeLayoutSearch;
    private EditText mEditTextSearch;
    private ImageView mImageViewClear;

    private BakersAdapter mBakersAdapter, mBakersFilteredAdapter;
    private LinearLayout mLinearLayoutDeliveryLocation;
    private TextView mTextViewDeliveryLocation;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerViewBakerItems;
    private RelativeLayout mRelativeLayoutEmptyView;
    private ProgressBar mProgressBar;
    private ImageView mImageViewEmptyView;
    private TextView mTextViewEmptyView, mTextViewEmptyViewTextButton;

    private MenuItem mSearchMenuItem, mFilterMenuItem;

    private String mLoggedInUserId;
    private String[] mFilterEntries;
    private boolean mIsSearchCollapsed = true;

    private static final String LOG_TAG = NearbyBakersFragment.class.getSimpleName();

    public NearbyBakersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nearby_bakers, container, false);

        Log.d(LOG_TAG, "onCreateView() called...");

        setHasOptionsMenu(true);

        initViews(view);

        mFilterEntries = getContext().getResources().getStringArray(R.array.array_filter_bakers);
        mLoggedInUserId = SharedPrefSingleton.getInstance(getContext()).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID);

        mIsSearchCollapsed = true;
        setUpSearch();

        mBakers = new ArrayList<>();

        mBakersAdapter = new BakersAdapter(getContext(), mBakers, false);
        mBakersAdapter.setItemClickListener(new BakersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Baker baker = null;
                if (mBakersFilteredAdapter != null) {
                    baker = mBakersFilteredAdapter.getItem(position);
                } else {
                    baker = mBakersAdapter.getItem(position);
                }
                Intent bakerIntent = Utils.getBakerIntent(getContext(), baker);
                bakerIntent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, getActivity().getIntent().getExtras().getStringArrayList(Constants.EXTRA_KEY_ORDER_IMAGES));
                startActivity(bakerIntent);
            }
        });
        mRecyclerViewBakerItems.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerViewBakerItems.setAdapter(mBakersAdapter);

        mTextViewEmptyViewTextButton.setOnClickListener(this);

        mTextViewDeliveryLocation.setText(Globals.sUserDeliveryLocationMap);

        mLinearLayoutDeliveryLocation.setOnClickListener(this);
        mLinearLayoutDeliveryLocation.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                onLongClickDeliveryLocation();
                return true;
            }
        });

        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeAsyncTaskBakers(getAppliedFilterParamValue(getContext()));
            }
        });

        return view;
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_nearby_bakers, menu);
        mSearchMenuItem = menu.findItem(R.id.action_search);
        mFilterMenuItem = menu.findItem(R.id.action_filter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                onClickSearch();
                return true;
            case R.id.action_filter:
                onClickFilter();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart() called...");
        executeAsyncTaskBakers(getAppliedFilterParamValue(getContext()));
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause() called...");
    }

    @Override
    public void onClick(View v) {
        if (v == mLinearLayoutDeliveryLocation) {
            getActivity().onBackPressed();
        }
        if (v == mImageViewClear) {
            mEditTextSearch.setText("");
        }
        if (v == mTextViewEmptyViewTextButton) {
            if (mTextViewEmptyViewTextButton.getText().toString().equalsIgnoreCase(getString(R.string.shared_try_again))) {
                mTextViewEmptyViewTextButton.setVisibility(View.GONE);
                executeAsyncTaskBakers(getAppliedFilterParamValue(getContext()));
            } else {
                getActivity().onBackPressed();
            }
        }
    }

    /*
     *
     * Helper methods
     */

    private void initViews(View view) {
        mRelativeLayoutSearch = (RelativeLayout) view.findViewById(R.id.relative_layout_search);
        mEditTextSearch = (EditText) view.findViewById(R.id.et_search);
        mImageViewClear = (ImageView) view.findViewById(R.id.img_clear);
        mLinearLayoutDeliveryLocation = (LinearLayout) view.findViewById(R.id.linear_layout_delivery_location);
        mTextViewDeliveryLocation = (TextView) view.findViewById(R.id.tv_delivery_location);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mRecyclerViewBakerItems = (RecyclerView) view.findViewById(R.id.rv_nearby_bakers);
        mRelativeLayoutEmptyView = (RelativeLayout) view.findViewById(R.id.relative_layout_empty_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        mImageViewEmptyView = (ImageView) view.findViewById(R.id.img_empty_view);
        mTextViewEmptyView = (TextView) view.findViewById(R.id.tv_empty_view);
        mTextViewEmptyViewTextButton = (TextView) view.findViewById(R.id.tv_empty_view_text_button);
    }

    private void setUpSearch() {
        Utils.updateSearchRightMargin(getContext(), mEditTextSearch, 4);
        mEditTextSearch.setHint("Search by name or address");
        mEditTextSearch.setHintTextColor(Constants.DEFAULT_EDITTEXT_HINT_COLOR);

        mEditTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String searchStr = editable.toString().trim();
                if (searchStr.equals("")) {
                    mImageViewClear.setVisibility(View.GONE);
                    Utils.updateSearchRightMargin(getContext(), mEditTextSearch, 4);
                    executeAsyncTaskBakers(getAppliedFilterParamValue(getContext()));
                } else {
                    Utils.updateSearchRightMargin(getContext(), mEditTextSearch, 40);
                    mImageViewClear.setVisibility(View.VISIBLE);
                    filterBakers(searchStr);
                }
            }
        });

        mImageViewClear.setOnClickListener(this);
    }

    private void onLongClickDeliveryLocation() {
        View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getString(R.string.copy_alert_dialog_button), null);

        TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setText(mTextViewDeliveryLocation.getText().toString().trim());

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboardManager = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData = ClipData.newPlainText("delivery_location", mTextViewDeliveryLocation.getText().toString().trim());
                Toast.makeText(getContext(), "Delivery location copied to clipboard.", Toast.LENGTH_SHORT).show();
                clipboardManager.setPrimaryClip(clipData);
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(getContext(), R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
    }

    private void onClickFilter() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogThemeFilter);
        builder.setNegativeButton(getContext().getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(getContext().getString(R.string.nearby_bakers_apply_alert_dialog_button), null);

        int selectedItem = sFilterSelectedItemIndex;
        builder.setSingleChoiceItems(mFilterEntries, selectedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                sIsFilterApplied = false;
                sFilterSelectedItemIndex = position;
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setTitle(getContext().getString(R.string.action_filter));
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getAppliedFilterParamValue(getContext()).equalsIgnoreCase(ApiConstants.PARAM_NEAREST_FIRST)) {
                    sIsFilterApplied = false;
                    return;
                }
                sFilterPreviousSelectedItemIndex = sFilterSelectedItemIndex;
                sIsFilterApplied = true;
                alertDialog.dismiss();
                executeAsyncTaskBakers(getAppliedFilterParamValue(getContext()));
            }
        });

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (sIsFilterApplied == false) {
                    sFilterSelectedItemIndex = sFilterPreviousSelectedItemIndex;
                }
            }
        });

        TextView textViewTitle = (TextView) alertDialog.findViewById(R.id.alertTitle);
        textViewTitle.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/montserrat_semibold.ttf"));
        textViewTitle.setTextSize(18);

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextSize(14);
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextSize(14);

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(getContext(), R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
    }

    private void onClickSearch() {
        String searchStr = mEditTextSearch.getText().toString().trim();
        if (mIsSearchCollapsed) {
            hideEmptyView();
            showEmptyView(View.GONE, View.GONE, View.GONE, "", getString(R.string.nearby_bakers_change_delivery_location));
            if (!Utils.isStringEmptyOrNull(searchStr)) {
                mEditTextSearch.setText("");
            }
            // Expand
            mIsSearchCollapsed = false;
            Utils.expand(mRelativeLayoutSearch, 4);
            mEditTextSearch.requestFocus();
        } else {
            // Collapse
            mIsSearchCollapsed = true;
            Utils.collapse(mRelativeLayoutSearch, 4);
            if (!Utils.isStringEmptyOrNull(searchStr)) {
                mEditTextSearch.setText("");
            }
        }
    }

    private void showHideMenuItem(MenuItem menuItem) {
        if (mBakers.size() > 0 || mBakersAdapter.getItemCount() > 0) {
            Utils.showMenuItem(menuItem);
        } else {
            Utils.hideMenuItem(menuItem);
        }
    }

    private void filterBakers(String searchQuery) {
        ArrayList<Baker> bakers = new ArrayList<>();
        for (Baker baker : mBakers) {
            String bakerName = baker.getFirstName().toLowerCase() + " " + baker.getLastName().toLowerCase();
            String bakerAddress = baker.getAddress().toLowerCase();
            if (bakerName.contains(searchQuery.toLowerCase()) || bakerAddress.contains(searchQuery.toLowerCase())) {
                bakers.add(baker);
            }
        }
        mBakers.clear();
        mBakers.addAll(bakers);
//        mRecyclerViewBakerItems.setAdapter(mBakersAdapter);
        mBakersAdapter.notifyDataSetChanged();

        // No results found
        if (mBakersAdapter.getItemCount() == 0 && !searchQuery.equals("")) {
            showEmptyView(View.GONE, View.GONE, View.GONE, "No bakers found for '" + searchQuery + "'.", getString(R.string.nearby_bakers_change_delivery_location));
        }
        // Some results found
        else if (!searchQuery.equals("") && mBakersAdapter.getItemCount() != 0) {
            mTextViewEmptyView.setText("");
            hideEmptyView();
        }
    }

    private void stopRefreshingSwipeLayout() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void toggleRecyclerView(int recyclerViewVisibility) {
        mRecyclerViewBakerItems.setVisibility(recyclerViewVisibility);
    }

    private void showEmptyView(int imageVisibility, int textButtonVisibility, int progressBarVisibility, String textViewText, String textButtonText) {
        mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        mImageViewEmptyView.setVisibility(imageVisibility);
        mTextViewEmptyViewTextButton.setVisibility(textButtonVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mTextViewEmptyView.setText(textViewText);
        mTextViewEmptyViewTextButton.setText(textButtonText);
    }

    private void hideEmptyView() {
        mRelativeLayoutEmptyView.setVisibility(View.GONE);
    }

    public void executeAsyncTaskBakers(final String paramValue) {
        showHideMenuItem(mSearchMenuItem);
        showHideMenuItem(mFilterMenuItem);
        mBakers.clear();
        toggleRecyclerView(View.GONE);
        showEmptyView(View.GONE, View.GONE, View.VISIBLE, "", getString(R.string.nearby_bakers_change_delivery_location));

        if (!Utils.isNetworkAvailable(getContext())) {
            stopRefreshingSwipeLayout();
            showHideMenuItem(mSearchMenuItem);
            showHideMenuItem(mFilterMenuItem);
            showEmptyView(View.GONE, View.VISIBLE, View.GONE, getString(R.string.error_no_internet_connection), getString(R.string.shared_try_again));
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                String apiName;
                HashMap<String, String> params = new HashMap<>();

                params.put(ApiConstants.PARAM_USER_ID, mLoggedInUserId);
                if (paramValue.equalsIgnoreCase(ApiConstants.PARAM_RELEVANCE)) {
                    apiName = ApiConstants.API_GET_RELEVANT_BAKERS;
                } else {
                    apiName = ApiConstants.API_GET_BAKERS;
                    params.put(ApiConstants.PARAM_ORDER_BY, paramValue);
                }
                getBakersFromServer(apiName, params);
                return null;
            }
        }.execute();
    }

    private void getBakersFromServer(final String apiName, final HashMap<String, String> params) {
        final List<Baker> bakers = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, apiName, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideEmptyView();
                mProgressBar.setVisibility(View.GONE);
                stopRefreshingSwipeLayout();

                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {

                        // Get the json array as a response
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        if (responseArray.length() > 0) {

                            // Iterate through the array
                            for (int i = 0; i < responseArray.length(); i++) {

                                List<String> sliderImageUrls = new ArrayList<>();

                                // Get json object inside the array index wise
                                JSONObject bakerJsonObject = responseArray.getJSONObject(i);

                                // Get baker details of baker object index wise
                                String id = bakerJsonObject.getString(ApiConstants.PARAM_ID);
                                String image = bakerJsonObject.getString(ApiConstants.PARAM_IMAGE);
                                String fn = bakerJsonObject.getString(ApiConstants.PARAM_FIRST_NAME);
                                String ln = bakerJsonObject.getString(ApiConstants.PARAM_LAST_NAME);
                                String address = bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_NAME) + "\n" + bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_ADDRESS);
                                String mobileNumber = bakerJsonObject.getString(ApiConstants.PARAM_MOBILE_NUMBER);
                                String email = bakerJsonObject.getString(ApiConstants.PARAM_EMAIL);
                                sliderImageUrls.add(bakerJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE1));
                                sliderImageUrls.add(bakerJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE2));
                                sliderImageUrls.add(bakerJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE3));
                                double rating = Utils.isStringEmptyOrNull(bakerJsonObject.getString(ApiConstants.PARAM_RATING))
                                        ? 0.0 : Double.parseDouble(bakerJsonObject.getString(ApiConstants.PARAM_RATING));
                                double bakerLatitude = Utils.isStringEmptyOrNull(bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE))
                                        ? 0.0 : Double.parseDouble(bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE));
                                double bakerLongitude = Utils.isStringEmptyOrNull(bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE))
                                        ? 0.0 : Double.parseDouble(bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE));
                                String activeStatus = bakerJsonObject.getString(ApiConstants.PARAM_ACTIVE_STATUS);
                                String favoriteId = bakerJsonObject.getString(ApiConstants.PARAM_FAVORITE_ID);
                                String userIds = bakerJsonObject.getString(ApiConstants.PARAM_USER_IDS);

                                // Calculate the circular distance between user current and baker location
                                double distance = Utils.calculateCircularDistanceInKilometers(
                                        Globals.sUserCurrentLocation.getLatitude(),
                                        Globals.sUserCurrentLocation.getLongitude(),
                                        bakerLatitude,
                                        bakerLongitude
                                );

                                boolean isFavorite = (userIds.contains(mLoggedInUserId)) ? true : false;

                                // If the distance is <= to the Nearby KM filter criteria set, then add baker to list
                                if (distance <= Constants.NEARBY_KM_VALUE) {
                                    Baker baker = new Baker(id, image, fn, ln, address, mobileNumber, email, sliderImageUrls,
                                            rating, Utils.getDoubleValueRoundedOff(distance), isFavorite, favoriteId);
                                    baker.setActiveStatus(activeStatus);
                                    bakers.add(baker);
                                }
                            }
                            hideEmptyView();
                            toggleRecyclerView(View.VISIBLE);
                            mBakers.addAll(bakers);
                            mRecyclerViewBakerItems.setAdapter(mBakersAdapter);
                            mBakersAdapter.notifyDataSetChanged();
                            showHideMenuItem(mSearchMenuItem);
                            showHideMenuItem(mFilterMenuItem);
                            if (mBakersAdapter.getItemCount() == 0) {
                                mImageViewEmptyView.setImageResource(R.drawable.ic_nearby);
                                showEmptyView(View.VISIBLE, View.VISIBLE, View.GONE, getString(R.string.nearby_bakers_couldnt_find_any_bakers), getString(R.string.nearby_bakers_change_delivery_location));
                            }
                        } else {
                            showHideMenuItem(mSearchMenuItem);
                            showHideMenuItem(mFilterMenuItem);
                            toggleRecyclerView(View.GONE);
                            mImageViewEmptyView.setImageResource(R.drawable.ic_bakers_secondary);
                            showEmptyView(View.VISIBLE, View.GONE, View.GONE, getString(R.string.nearby_bakers_no_bakers_available), getString(R.string.nearby_bakers_change_delivery_location));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                showHideMenuItem(mSearchMenuItem);
                showHideMenuItem(mFilterMenuItem);
                stopRefreshingSwipeLayout();
                toggleRecyclerView(View.GONE);
                showEmptyView(View.GONE, View.VISIBLE, View.GONE, "Sorry, something went wrong. Please try again.", getString(R.string.shared_try_again));
                if (error != null) {
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }
}
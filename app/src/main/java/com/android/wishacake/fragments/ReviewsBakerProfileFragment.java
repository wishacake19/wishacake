package com.android.wishacake.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.activities.BakerProfileActivity;
import com.android.wishacake.adapters.ReviewsAdapter;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.models.Review;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReviewsBakerProfileFragment extends Fragment {

    private List<Review> mReviews;

    private ReviewsAdapter mReviewsAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerViewReviewItems;
    private RelativeLayout mRelativeLayoutEmptyView;
    private ProgressBar mProgressBar;
    private ImageView mImageViewEmptyView;
    private TextView mTextViewEmptyView, mTextViewEmptyViewTextButton;

    private static final String LOG_TAG = ReviewsBakerProfileFragment.class.getSimpleName();

    public ReviewsBakerProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reviews_baker_profile, container, false);

        initViews(view);

        mReviews = new ArrayList<>();
        mReviewsAdapter = new ReviewsAdapter(getContext(), mReviews);
        mRecyclerViewReviewItems.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerViewReviewItems.setAdapter(mReviewsAdapter);

        executeAsyncTaskReviews();

        mTextViewEmptyViewTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                executeAsyncTaskReviews();
            }
        });

        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeAsyncTaskReviews();
            }
        });

        return view;
    }

    /*
     *
     * Helper methods
     */

    private void initViews(View view) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mRecyclerViewReviewItems = (RecyclerView) view.findViewById(R.id.rv_review_items);
        mRelativeLayoutEmptyView = (RelativeLayout) view.findViewById(R.id.relative_layout_empty_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        mImageViewEmptyView = (ImageView) view.findViewById(R.id.img_empty_view);
        mTextViewEmptyView = (TextView) view.findViewById(R.id.tv_empty_view);
        mTextViewEmptyViewTextButton = (TextView) view.findViewById(R.id.tv_empty_view_text_button);
    }

    private void stopRefreshingSwipeLayout() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void toggleRecyclerView(int recyclerViewVisibility) {
        mRecyclerViewReviewItems.setVisibility(recyclerViewVisibility);
    }

    private void showEmptyView(int imageVisibility, int textButtonVisibility, int progressBarVisibility, String textViewText) {
        mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        mImageViewEmptyView.setVisibility(imageVisibility);
        mTextViewEmptyViewTextButton.setVisibility(textButtonVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mTextViewEmptyView.setText(textViewText);
    }

    private void hideEmptyView() {
        mRelativeLayoutEmptyView.setVisibility(View.GONE);
    }

    private void executeAsyncTaskReviews() {
        BakerProfileActivity.sTabLayoutBakerProfile.getTabAt(1).setText(getContext().getString(R.string.baker_profile_tab_reviews));
        mReviews.clear();
        toggleRecyclerView(View.GONE);
        showEmptyView(View.GONE, View.GONE, View.VISIBLE, "");

        if (!Utils.isNetworkAvailable(getContext())) {
            stopRefreshingSwipeLayout();
            showEmptyView(View.GONE, View.VISIBLE, View.GONE, getString(R.string.error_no_internet_connection));
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                getReviewsFromServer();
                return null;
            }
        }.execute();
    }

    private void getReviewsFromServer() {
        final List<Review> reviews = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_GET_REVIEWS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideEmptyView();
                mProgressBar.setVisibility(View.GONE);
                stopRefreshingSwipeLayout();

                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {

                        // Get the json array as a response
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        if (responseArray.length() > 0) {

                            // Iterate through the array
                            for (int i = 0; i < responseArray.length(); i++) {
                                // Get json object inside the array index wise
                                JSONObject reviewJsonObject = responseArray.getJSONObject(i);

                                // Get baker details of review object index wise
                                String reviewBy = reviewJsonObject.getString(ApiConstants.PARAM_FIRST_NAME)
                                        + " " + reviewJsonObject.getString(ApiConstants.PARAM_LAST_NAME);
                                String review = reviewJsonObject.getString(ApiConstants.PARAM_REVIEW);
                                double rating = Utils.isStringEmptyOrNull(reviewJsonObject.getString(ApiConstants.PARAM_RATING))
                                        ? 0.0 : Double.parseDouble(reviewJsonObject.getString(ApiConstants.PARAM_RATING));
                                String createdAt = reviewJsonObject.getString(ApiConstants.PARAM_CREATED_AT);

                                reviews.add(new Review(reviewBy, review, rating, DateTimeUtils.getFormattedDateTimeString("yyyy-MM-dd HH:mm:ss", "dd MMM, yyyy", createdAt)));
                            }
                            hideEmptyView();
                            toggleRecyclerView(View.VISIBLE);
                            mReviews.addAll(reviews);
                            mRecyclerViewReviewItems.setAdapter(mReviewsAdapter);
                            mReviewsAdapter.notifyDataSetChanged();
                        } else {
                            toggleRecyclerView(View.GONE);
                            showEmptyView(View.VISIBLE, View.GONE, View.GONE, getString(R.string.reviews_text_empty_view));
                        }
                        BakerProfileActivity.sTabLayoutBakerProfile.getTabAt(1).setText(getContext().getString(R.string.baker_profile_tab_reviews) + " (" + mReviewsAdapter.getItemCount() + ")");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                stopRefreshingSwipeLayout();
                toggleRecyclerView(View.GONE);
                showEmptyView(View.GONE, View.VISIBLE, View.GONE, "Sorry, something went wrong. Please try again.");
                if (error != null) {
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_BAKER_ID, BakerProfileActivity.sBakerId);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }
}

package com.android.wishacake.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.adapters.BakersAdapter;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.models.Baker;
import com.android.wishacake.utilities.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FavoriteBakersFragment extends Fragment {

    private List<Baker> mBakers;

    private BakersAdapter mBakersAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerViewBakerItems;
    private RelativeLayout mRelativeLayoutEmptyView;
    private ProgressBar mProgressBar;
    private ImageView mImageViewEmptyView;
    private TextView mTextViewEmptyView, mTextViewEmptyViewTextButton;

    private static final String LOG_TAG = NearbyBakersFragment.class.getSimpleName();

    public FavoriteBakersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_bakers, container, false);

        Log.d(LOG_TAG, "onCreateView() called...");

        setHasOptionsMenu(true);

        initViews(view);

        mBakers = new ArrayList<>();
        mBakersAdapter = new BakersAdapter(getContext(), mBakers, true);
        mBakersAdapter.setItemClickListener(new BakersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Baker baker = mBakersAdapter.getItem(position);
                Intent bakerIntent = Utils.getBakerIntent(getContext(), baker);
                bakerIntent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, getActivity().getIntent().getExtras().getStringArrayList(Constants.EXTRA_KEY_ORDER_IMAGES));
                startActivity(bakerIntent);
            }
        });

        mRecyclerViewBakerItems.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerViewBakerItems.setAdapter(mBakersAdapter);

        mTextViewEmptyViewTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTextViewEmptyViewTextButton.getText().toString().equalsIgnoreCase(getString(R.string.favorite_bakers_add_a_favorite))) {
                    ViewPager viewPager = (ViewPager) getActivity().findViewById(R.id.vp_bakers);
                    viewPager.setCurrentItem(2);
                } else if (mTextViewEmptyViewTextButton.getText().toString().equalsIgnoreCase(getString(R.string.shared_try_again))) {
                    mTextViewEmptyViewTextButton.setVisibility(View.GONE);
                    executeAsyncTaskFavoriteBakers();
                }
            }
        });

        mSwipeRefreshLayout.setColorSchemeColors(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                executeAsyncTaskFavoriteBakers();
            }
        });

        return view;
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "onStart() called...");
        executeAsyncTaskFavoriteBakers();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "onPause() called...");
    }

    /*
     *
     * Helper methods
     */

    private void initViews(View view) {
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mRecyclerViewBakerItems = (RecyclerView) view.findViewById(R.id.rv_fav_bakers);
        mRelativeLayoutEmptyView = (RelativeLayout) view.findViewById(R.id.relative_layout_empty_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        mImageViewEmptyView = (ImageView) view.findViewById(R.id.img_empty_view);
        mTextViewEmptyView = (TextView) view.findViewById(R.id.tv_empty_view);
        mTextViewEmptyViewTextButton = (TextView) view.findViewById(R.id.tv_empty_view_text_button);
    }

    private void stopRefreshingSwipeLayout() {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    private void toggleRecyclerView(int recyclerViewVisibility) {
        mRecyclerViewBakerItems.setVisibility(recyclerViewVisibility);
    }

    private void showEmptyView(int imageVisibility, int textButtonVisibility, int progressBarVisibility, String textViewText, String textButtonText) {
        mRelativeLayoutEmptyView.setVisibility(View.VISIBLE);
        mImageViewEmptyView.setVisibility(imageVisibility);
        mTextViewEmptyViewTextButton.setVisibility(textButtonVisibility);
        mProgressBar.setVisibility(progressBarVisibility);
        mTextViewEmptyView.setText(textViewText);
        mTextViewEmptyViewTextButton.setText(textButtonText);
    }

    private void hideEmptyView() {
        mRelativeLayoutEmptyView.setVisibility(View.GONE);
    }

    public void executeAsyncTaskFavoriteBakers() {
        mBakers.clear();
        toggleRecyclerView(View.GONE);
        showEmptyView(View.GONE, View.GONE, View.VISIBLE, "", getString(R.string.favorite_bakers_add_a_favorite));

        if (!Utils.isNetworkAvailable(getContext())) {
            stopRefreshingSwipeLayout();
            showEmptyView(View.GONE, View.VISIBLE, View.GONE, getString(R.string.error_no_internet_connection), getString(R.string.shared_try_again));
            return;
        }

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                getFavoriteBakersFromServer();
                return null;
            }
        }.execute();
    }

    private void getFavoriteBakersFromServer() {
        final List<Baker> bakers = new ArrayList<>();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_GET_FAVORITE_BAKERS, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                hideEmptyView();
                mProgressBar.setVisibility(View.GONE);
                stopRefreshingSwipeLayout();

                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {

                        // Get the json array as a response
                        JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                        if (responseArray.length() > 0) {

                            // Iterate through the array
                            for (int i = 0; i < responseArray.length(); i++) {

                                List<String> sliderImageUrls = new ArrayList<>();

                                // Get json object inside the array index wise
                                JSONObject bakerJsonObject = responseArray.getJSONObject(i);

                                // Get baker details of baker object index wise
                                String id = bakerJsonObject.getString(ApiConstants.PARAM_ID);
                                String image = bakerJsonObject.getString(ApiConstants.PARAM_IMAGE);
                                String fn = bakerJsonObject.getString(ApiConstants.PARAM_FIRST_NAME);
                                String ln = bakerJsonObject.getString(ApiConstants.PARAM_LAST_NAME);
                                String address = bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_NAME) + "\n" + bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_ADDRESS);
                                String mobileNumber = bakerJsonObject.getString(ApiConstants.PARAM_MOBILE_NUMBER);
                                String email = bakerJsonObject.getString(ApiConstants.PARAM_EMAIL);
                                sliderImageUrls.add(bakerJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE1));
                                sliderImageUrls.add(bakerJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE2));
                                sliderImageUrls.add(bakerJsonObject.getString(ApiConstants.PARAM_SLIDER_IMAGE3));
                                double rating = Utils.isStringEmptyOrNull(bakerJsonObject.getString(ApiConstants.PARAM_RATING))
                                        ? 0.0 : Double.parseDouble(bakerJsonObject.getString(ApiConstants.PARAM_RATING));
                                double bakerLatitude = Utils.isStringEmptyOrNull(bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE))
                                        ? 0.0 : Double.parseDouble(bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_LATITUDE));
                                double bakerLongitude = Utils.isStringEmptyOrNull(bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE))
                                        ? 0.0 : Double.parseDouble(bakerJsonObject.getString(ApiConstants.PARAM_LOCATION_LONGITUDE));
                                String activeStatus = bakerJsonObject.getString(ApiConstants.PARAM_ACTIVE_STATUS);
                                String favoriteId = bakerJsonObject.getString(ApiConstants.PARAM_FAVORITE_ID);

                                // Calculate the circular distance between user current and baker location
                                double distance = Utils.calculateCircularDistanceInKilometers(
                                        Globals.sUserCurrentLocation.getLatitude(),
                                        Globals.sUserCurrentLocation.getLongitude(),
                                        bakerLatitude,
                                        bakerLongitude
                                );

                                Baker baker = new Baker(id, image, fn, ln, address, mobileNumber, email, sliderImageUrls,
                                        rating, Utils.getDoubleValueRoundedOff(distance), true, favoriteId);
                                baker.setActiveStatus(activeStatus);
                                bakers.add(baker);
                            }
                            hideEmptyView();
                            toggleRecyclerView(View.VISIBLE);
                            mBakers.addAll(bakers);
                            mRecyclerViewBakerItems.setAdapter(mBakersAdapter);
                            mBakersAdapter.notifyDataSetChanged();
                        } else {
                            toggleRecyclerView(View.GONE);
                            showEmptyView(View.VISIBLE, View.VISIBLE, View.GONE, getString(R.string.favorite_bakers_no_fav_bakers), getString(R.string.favorite_bakers_add_a_favorite));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                stopRefreshingSwipeLayout();
                toggleRecyclerView(View.GONE);
                showEmptyView(View.GONE, View.VISIBLE, View.GONE, "Sorry, something went wrong. Please try again.", getString(R.string.shared_try_again));
                if (error != null) {
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_USER_ID, SharedPrefSingleton.getInstance(getContext()).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID));
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }
}

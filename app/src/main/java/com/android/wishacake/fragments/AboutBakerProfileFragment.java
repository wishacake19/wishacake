package com.android.wishacake.fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.wishacake.R;
import com.android.wishacake.activities.BakerProfileActivity;

public class AboutBakerProfileFragment extends Fragment implements View.OnClickListener, View.OnLongClickListener {

    private LinearLayout mLinearLayoutAddress, mLinearLayoutMobileNumber, mLinearLayoutEmail;
    private TextView mTextViewAddress, mTextViewMobileNumber, mTextViewEmail;

    public AboutBakerProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_about_baker_profile, container, false);

        initViews(view);

        mTextViewAddress.setText(BakerProfileActivity.sBakerAddress);
        mTextViewMobileNumber.setText(BakerProfileActivity.sBakerMobileNumber);
        mTextViewEmail.setText(BakerProfileActivity.sBakerEmail);

        mLinearLayoutAddress.setOnClickListener(this);
        mLinearLayoutMobileNumber.setOnClickListener(this);
        mLinearLayoutEmail.setOnClickListener(this);

        mLinearLayoutAddress.setOnLongClickListener(this);
        mLinearLayoutMobileNumber.setOnLongClickListener(this);
        mLinearLayoutEmail.setOnLongClickListener(this);

        return view;
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public void onClick(View view) {
        if (view == mLinearLayoutAddress) {
            if (!BakerProfileActivity.sBakerAddress.equals("")) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("geo:0,0?q= " + BakerProfileActivity.sBakerAddress));
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                }
            }
        }
        if (view == mLinearLayoutMobileNumber) {
            final CharSequence[] items = {"Call", "SMS"};
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme);
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int item) {
                    if (items[item].equals("Call")) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + BakerProfileActivity.sBakerMobileNumber));
                        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivity(intent);
                        }
                    } else if (items[item].equals("SMS")) {
                        Intent intent = new Intent(Intent.ACTION_SENDTO);
                        intent.setData(Uri.parse("smsto:")); // This ensures only SMS apps respond
                        intent.putExtra("address", BakerProfileActivity.sBakerMobileNumber);
                        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                            startActivity(intent);
                        }
                    }
                }
            });

            AlertDialog alertDialog = builder.create();
            alertDialog.setTitle(BakerProfileActivity.sBakerMobileNumber);
            alertDialog.setCancelable(true);

            alertDialog.show();

            TextView textViewTitle = (TextView) alertDialog.findViewById(R.id.alertTitle);
            textViewTitle.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/montserrat_semibold.ttf"));
            textViewTitle.setTextSize(18);
        }
        if (view == mLinearLayoutEmail) {
            String[] emailAddresses = new String[1];
            emailAddresses[0] = BakerProfileActivity.sBakerEmail;

            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:")); // only email apps should handle this
            intent.putExtra(Intent.EXTRA_EMAIL, emailAddresses);
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(intent);
            }
        }
    }

    @Override
    public boolean onLongClick(View view) {
        ClipData clipData = null;
        ClipboardManager clipboardManager = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);

        if (view == mLinearLayoutAddress) {
            clipData = ClipData.newPlainText("address", BakerProfileActivity.sBakerAddress);
            Toast.makeText(getContext(), "Address copied to clipboard.", Toast.LENGTH_SHORT).show();
        }
        if (view == mLinearLayoutMobileNumber) {
            clipData = ClipData.newPlainText("mobile_number", BakerProfileActivity.sBakerMobileNumber);
            Toast.makeText(getContext(), "Mobile number copied to clipboard.", Toast.LENGTH_SHORT).show();
        }
        if (view == mLinearLayoutEmail) {
            clipData = ClipData.newPlainText("email", BakerProfileActivity.sBakerEmail);
            Toast.makeText(getContext(), "Email address copied to clipboard.", Toast.LENGTH_SHORT).show();
        }
        clipboardManager.setPrimaryClip(clipData);
        return true;
    }

    /*
     *
     * Helper methods
     */

    private void initViews(View view) {
        mLinearLayoutAddress = (LinearLayout) view.findViewById(R.id.linear_layout_address);
        mLinearLayoutMobileNumber = (LinearLayout) view.findViewById(R.id.linear_layout_mobile_number);
        mLinearLayoutEmail = (LinearLayout) view.findViewById(R.id.linear_layout_email);
        mTextViewAddress = (TextView) view.findViewById(R.id.tv_baker_address);
        mTextViewMobileNumber = (TextView) view.findViewById(R.id.tv_baker_mobile_number);
        mTextViewEmail = (TextView) view.findViewById(R.id.tv_baker_email);
    }
}

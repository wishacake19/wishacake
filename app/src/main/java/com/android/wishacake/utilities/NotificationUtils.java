package com.android.wishacake.utilities;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioAttributes;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.android.wishacake.R;
import com.android.wishacake.activities.OrderDetailsActivity;
import com.android.wishacake.activities.YourOrdersActivity;
import com.android.wishacake.helpers.Constants;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

public class NotificationUtils {

    private static final int PENDING_INTENT_REQUEST_CODE = 5589;
    private static final String CHANNEL_ID = "channel-id";
    private static final String ACTION_URL = "url";
    private static final String ACTION_ACTIVITY = "activity";

    private Context mContext;
    private HashMap<String, Class> mActivityMap = new HashMap<>();

    public NotificationUtils(Context context) {
        mContext = context;
        mActivityMap.put("YourOrdersActivity", YourOrdersActivity.class);
        mActivityMap.put("OrderDetailsActivity", OrderDetailsActivity.class);
    }

    public void displayNotification(com.android.wishacake.models.Notification notificationObject, Intent notificationIntent) {
        String title = notificationObject.getTitle();
        String message = notificationObject.getMessage();
        String imageUrl = notificationObject.getImageUrl();
        String action = notificationObject.getAction();
        String actionDestination = notificationObject.getActionDestination();
        String orderId = notificationObject.getOrderId();
        long when = notificationObject.getWhen();

        PendingIntent resultPendingIntent;

        if (ACTION_URL.equals(action)) {
            Intent urlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(actionDestination));
            resultPendingIntent = PendingIntent.getActivity(mContext, PENDING_INTENT_REQUEST_CODE,
                    urlIntent, 0);
        } else if (ACTION_ACTIVITY.equals(action) && mActivityMap.containsKey(actionDestination)) {
            notificationIntent = new Intent(mContext, mActivityMap.get(actionDestination));
            if (actionDestination.equals("OrderDetailsActivity")) {
                notificationIntent.putExtra(Constants.EXTRA_KEY_ORDER_ID, orderId);
                notificationIntent.putExtra(Constants.EXTRA_KEY_ORDER_DATA_LOAD_FROM_SERVER, true);
            }
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            resultPendingIntent = PendingIntent.getActivity(mContext, new Random().nextInt(),
                    notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            resultPendingIntent = PendingIntent.getActivity(mContext, PENDING_INTENT_REQUEST_CODE,
                    notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        NotificationManager notificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification;

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mContext, CHANNEL_ID);
        mBuilder.setColor(ContextCompat.getColor(mContext, R.color.colorPrimary));

        mBuilder.setTicker(title);
        mBuilder.setAutoCancel(true);
        mBuilder.setContentTitle(title);
//        if (when != 0) {
//            mBuilder.setWhen(when);
//        }
        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setDefaults(Notification.DEFAULT_VIBRATE);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        mBuilder.setSmallIcon(R.drawable.ic_notification_small_icon);
        mBuilder.setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher));
        mBuilder.setContentText(message);

        Bitmap imageBitMap = null;
        if (imageUrl != null) {
            imageBitMap = getBitmapFromURL(imageUrl);
        }

        if (imageBitMap == null) {
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        } else {
            mBuilder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(imageBitMap));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            mBuilder.setPriority(NotificationCompat.PRIORITY_HIGH);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            mBuilder.setVibrate(new long[0]);
        }
        notification = mBuilder.build();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
                    .build();

            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, "Primary", NotificationManager.IMPORTANCE_HIGH);
            mChannel.enableVibration(true);
            mChannel.enableLights(true);
            mChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), audioAttributes);
            notificationManager.createNotificationChannel(mChannel);
        }

        int uniqueNotificationId = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
        notificationManager.notify(uniqueNotificationId, notification);
    }

    private Bitmap getBitmapFromURL(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            return BitmapFactory.decodeStream(input);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}


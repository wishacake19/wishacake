package com.android.wishacake.utilities;

import android.text.format.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateTimeUtils {

    public static boolean isToday(long time) {
        return DateUtils.isToday(time);
    }

    public static boolean isTomorrow(long time) {
        return DateUtils.isToday(time - DateUtils.DAY_IN_MILLIS);
    }

    public static boolean isYesterday(long time) {
        return DateUtils.isToday(time + DateUtils.DAY_IN_MILLIS);
    }

    public static long convertStringDateToLong(String dateFormat, String dateTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static String getFormattedDateTimeString(String actualFormat, String requiredFormat, String str) {
        SimpleDateFormat actualFormatObj = new SimpleDateFormat(actualFormat);
        String formattedStr = null;
        try {
            formattedStr = new SimpleDateFormat(requiredFormat).format(actualFormatObj.parse(str));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formattedStr;
    }

    public static int getMinutesDifference(long time) {
        long currentTime = Calendar.getInstance().getTime().getTime();
        int minutesDifference = 0;
        try {
            minutesDifference = (int) TimeUnit.MINUTES.convert(currentTime - time, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            if (e != null) {
                e.printStackTrace();
            }
        }
        return minutesDifference;
    }
}

package com.android.wishacake.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

public class PermissionUtils {

    private static int sCurrentAPIVersion = Build.VERSION.SDK_INT;

    public static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 3;
    public static final int REQUEST_CODE_CAMERA_OR_WRITE_EXTERNAL_STORAGE = 12;
    public static final int REQUEST_CODE_LOCATION = 58;

    public static boolean checkGalleryPermission(final Context context) {
        if (sCurrentAPIVersion >= android.os.Build.VERSION_CODES.M) {

            // Permission is not granted
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_WRITE_EXTERNAL_STORAGE);

                return false;
            }
            // Permission has already been granted
            else {
                return true;
            }
        }
        // No need to ask for run-time permission as the device's Android version is below Marshmallow
        else {
            return true;
        }
    }

    public static boolean checkCameraPermission(final Context context) {
        if (sCurrentAPIVersion >= android.os.Build.VERSION_CODES.M) {

            // Permission is not granted
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        == PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{Manifest.permission.CAMERA},
                            REQUEST_CODE_CAMERA_OR_WRITE_EXTERNAL_STORAGE);
                }
                else {
                    ActivityCompat.requestPermissions((Activity) context,
                            new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_CODE_CAMERA_OR_WRITE_EXTERNAL_STORAGE);
                }
                return false;
            }
            // Permission has already been granted
            else {
                return true;
            }
        }
        // No need to ask for run-time permission as the device's Android version is below Marshmallow
        else {
            return true;
        }
    }

    public static boolean checkLocationPermission(final Context context) {
        if (sCurrentAPIVersion >= android.os.Build.VERSION_CODES.M) {

            // Permission is not granted
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions((Activity) context,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_LOCATION);

                return false;
            }
            // Permission has already been granted
            else {
                return true;
            }
        }
        // No need to ask for run-time permission as the device's Android version is below Marshmallow
        else {
            return true;
        }
    }
}

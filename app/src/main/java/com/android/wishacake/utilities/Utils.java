package com.android.wishacake.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.activities.BakerProfileActivity;
import com.android.wishacake.activities.HelpActivity;
import com.android.wishacake.activities.MainActivity;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.models.Baker;
import com.google.firebase.iid.FirebaseInstanceId;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.android.wishacake.helpers.Globals.sFilterSelectedItemIndex;
import static java.lang.Math.acos;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

public class Utils {

    private static final String LOG_TAG = Utils.class.getSimpleName();

    private static String sFirebaseToken;
    private static final String SENDER_ID = "834136674544";

    public static void showError(MaterialEditText materialEditText, String errMessage, TextView errorTextView) {
        errorTextView.setVisibility(View.VISIBLE);
        errorTextView.setText(errMessage);
        materialEditText.requestFocus();
    }

    public static void hideError(TextView errorTextView) {
        errorTextView.setVisibility(View.GONE);
        errorTextView.setText("");
    }

    public static void showCustomOkAlertDialog(Activity activity, String title, String msg) {
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setPositiveButton(activity.getString(R.string.ok_alert_dialog_button), null);

        TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        if (!title.equals("") && title != null && !title.isEmpty()) {
            // Title
            TextView textViewTitle = (TextView) dialogView.findViewById(R.id.tv_title);
            textViewTitle.setVisibility(View.VISIBLE);
            textViewTitle.setText(title);

            // Message
            textViewMsg.setTextColor(ContextCompat.getColor(activity, R.color.colorSecondaryText)); // Default in XML is PrimaryTextColor
        }
        textViewMsg.setText(msg);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    public static void enableDisableUploadImageOptionInHelp(Context context, int bgDrawableId, boolean isEnabled) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            HelpActivity.sFrameLayoutUploadImage.setBackground(ContextCompat.getDrawable(context, bgDrawableId));
        }
        if (isEnabled) {
            HelpActivity.sButtonUploadAnImage.setEnabled(true);
        } else {
            HelpActivity.sButtonUploadAnImage.setEnabled(false);
        }
    }

    public static void shareImage(String url, final Context context) {
        Picasso.get().load(url).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("image/*");
                shareIntent.putExtra(Intent.EXTRA_STREAM, getLocalBitmapUri(bitmap, context));
                context.startActivity(Intent.createChooser(shareIntent, "Share with"));
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
            }
        });
    }

    public static Uri getLocalBitmapUri(Bitmap bmp, Context context) {
        Uri bmpUri = null;
        try {
            File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "IMG_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }

    public static boolean isNameValid(String name) {
        boolean check = false;
        String regexExp = "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$";
        if (name.matches(regexExp)) {
            check = true;
        }
        return check;
    }

    public static boolean isMobileNumberValid(String mobileNumber) {
        boolean check = false;
        if (mobileNumber.startsWith("+92") && mobileNumber.length() == 13) {
            check = true;
        }
        return check;
    }

    public static boolean isPasswordValid(String password) {
        boolean check = false;
        String regexExp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";

        // ^                start-of-string
        // ?=.*[0-9])       a digit must occur at least once
        // (?=.*[a-z])      a lower case letter must occur at least once
        // (?=.*[A-Z])      an upper case letter must occur at least once
        // (?=\S+$)         no whitespace allowed in the entire string
        // .{8,}            anything, at least eight places though
        // $                end-of-string

        if (password.matches(regexExp)) {
            check = true;
        }
        return check;
    }

    public static boolean isEmailValid(String email) {
        boolean check = false;
        String regexExp = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-zA-Z]{2,})$";
        if (email.matches(regexExp)) {
            check = true;
        }
        return check;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    public static void changeStatusBarBackground(Activity activity, int color) {
        try {
            Window window = activity.getWindow();

            // clear FLAG_TRANSLUCENT_STATUS flag:
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

            // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

            // finally change the color to any color with transparency
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(color);
            }
        } catch (Exception e) {
            Log.e(LOG_TAG, e.getMessage());
        }
    }

    public static void setUpOrderStatus(Context context, TextView textView, String text, int colorResId, int drawableResId) {
        textView.setText(text);
        textView.setTextColor(ContextCompat.getColor(context, colorResId));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            textView.setBackground(ContextCompat.getDrawable(context, drawableResId));
        }
    }

    public static void expand(final View view, int speedMultiplierValue) {
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) view.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        view.measure(matchParentMeasureSpec, wrapContentMeasureSpec);

        final int targetHeight = view.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        view.getLayoutParams().height = 1;
        view.setVisibility(View.VISIBLE);

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                view.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                view.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration(((int) (targetHeight / view.getContext().getResources().getDisplayMetrics().density)) * speedMultiplierValue);
        view.startAnimation(animation);
    }

    public static void collapse(final View view, int speedMultiplierValue) {
        final int initialHeight = view.getMeasuredHeight();

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    view.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration(((int) (initialHeight / view.getContext().getResources().getDisplayMetrics().density)) * speedMultiplierValue);
        view.startAnimation(animation);
    }

    public static void updateSearchRightMargin(Context context, EditText editText, int marginValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        // convert the DP into pixel
        int rightPixel = (int) (marginValue * scale + 0.5f);
        ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) editText.getLayoutParams();
        p.setMargins(0, 0, rightPixel, 0);
        editText.setLayoutParams(p);
    }

    public static double getDoubleValueRoundedOff(Double value) {
        DecimalFormat decimalFormat = new DecimalFormat("0.00"); // Up to two decimal places
        String strValue = decimalFormat.format(value);
        return Double.parseDouble(strValue);
    }

    public static boolean isStringEmptyOrNull(String str) {
        if (str.isEmpty() || str.equals("") || str.equalsIgnoreCase("null") || str == null) {
            return true;
        } else {
            return false;
        }
    }

    public static void showReactivateAccountDialog(final Activity activity, String dateStr, final String id) {
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(activity.getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(activity.getString(R.string.reactivate_alert_dialog_button), null);

        final TextView textViewTitle = (TextView) dialogView.findViewById(R.id.tv_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText("Reactivate your account");

        final TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setTextColor(ContextCompat.getColor(activity, R.color.colorSecondaryText));
        textViewMsg.setText("We have found that your account was deactivated on " + dateStr + ". If you wish to use " + activity.getString(R.string.app_name) + " again, tap the Reactivate button below and you're good to go.");

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        final ProgressDialog progressDialog = new ProgressDialog(activity, R.style.ProgressDialogTheme);
        progressDialog.setMessage("Processing...");
        progressDialog.setCancelable(false);

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNetworkAvailable(activity)) {
                    Toast.makeText(activity, activity.getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
                    // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
                    return;
                }
                progressDialog.show();
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        reactivate(alertDialog, id);
                        return null;
                    }
                }.execute();
            }

            private void reactivate(final AlertDialog reactivateAlertDialog, final String id) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_DEACTIVATE_REACTIVATE_ACCOUNT, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                reactivateAlertDialog.dismiss();
                                JSONArray responseArray = rootJsonObject.getJSONArray(ApiConstants.KEY_RESPONSE);

                                String id = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ID);
                                String fn = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_FIRST_NAME);
                                String ln = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_LAST_NAME);
                                String mobileNumber = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_MOBILE_NUMBER);
                                String email = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_EMAIL);
                                String password = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_PASSWORD);
                                String accountType = responseArray.getJSONObject(0).getString(ApiConstants.PARAM_ACCOUNT_TYPE);

                                SharedPrefSingleton.getInstance(activity).saveLoggedInUserData(id, fn, ln, mobileNumber,
                                        email, password, accountType);
                                Globals.sIsGuestUserLoggedIn = false;
                                activity.startActivity(new Intent(activity, MainActivity.class));
                            } else {
                                String errorCode = rootJsonObject.getString(ApiConstants.KEY_ERROR_CODE);
                                if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR) && errorCode.equals("")) {
                                    Toast.makeText(activity, rootJsonObject.getString(ApiConstants.KEY_RESPONSE),
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(activity, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ID, id);
                        params.put(ApiConstants.PARAM_ACCOUNT_STATUS, ApiConstants.PARAM_VALUE_REACTIVATE);
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(activity).addToRequestQueue(stringRequest);
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    public static void enableMenuItem(MenuItem menuItem) {
        if (menuItem != null) {
            menuItem.getIcon().setAlpha(255);
            menuItem.setEnabled(true);
        }
    }

    public static void disableMenuItem(MenuItem menuItem) {
        if (menuItem != null) {
            menuItem.getIcon().setAlpha(130);
            menuItem.setEnabled(false);
        }
    }

    public static void showMenuItem(MenuItem menuItem) {
        if (menuItem != null) {
            menuItem.setVisible(true);
        }
    }

    public static void hideMenuItem(MenuItem menuItem) {
        if (menuItem != null) {
            menuItem.setVisible(false);
        }
    }

    public static String getFormattedCardNumber(String cardNumber) {
        String extractedCardNumber = cardNumber.substring(0, cardNumber.length() - 4); // From 0 - End, neglecting the last 4
        String formattedCardNumber = "**** " + cardNumber.substring(extractedCardNumber.length(), cardNumber.length()); // **** + Last 4 digits
        return formattedCardNumber;
    }

    public static String getCardType(String cardNumber) {
        String cardType = "";
        if (cardNumber.startsWith("51") || cardNumber.startsWith("52") || cardNumber.startsWith("53") || cardNumber.startsWith("54") || cardNumber.startsWith("55")) {
            cardType = Constants.MASTER_CARD;
        } else if (cardNumber.startsWith("4")) {
            cardType = Constants.VISA;
        }
//        else if (cardNumber.startsWith("34") || cardNumber.startsWith("37")) {
//            cardType = Constants.AMERICAN_EXPRESS;
//        }
        return cardType;
    }

    public static String getBase64EncodedFromBitmap(Bitmap imageBitmap, int quality) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, quality, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }

    public static String getBase64EncodedFromBitmap(Bitmap imageBitmap) {
        if (imageBitmap == null) {
            return "";
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        imageBitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
    }

    public static String getPathFromBitmap(Activity activity, Bitmap imageBitmap) {
        return MediaStore.Images.Media.insertImage(activity.getContentResolver(), imageBitmap, "title", null);
    }

    public static Intent getBakerIntent(Context context, Baker baker) {
        Intent bakerIntent = new Intent(context, BakerProfileActivity.class);
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_ID, baker.getId());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_NAME, baker.getFirstName() + " " + baker.getLastName());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_IMAGE, baker.getImageUrl());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_RATING, baker.getRating());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_DISTANCE, baker.getDistance());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_ADDRESS, baker.getAddress());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_MOBILE_NUMBER, baker.getMobileNumber());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_EMAIL, baker.getEmail());
        bakerIntent.putStringArrayListExtra(Constants.EXTRA_KEY_BAKER_SLIDER_IMAGES, (ArrayList<String>) baker.getSliderImageUrls());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_FAVORITE_ID, baker.getFavoriteId());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_ACTIVE_STATUS, baker.getActiveStatus());
        bakerIntent.putExtra(Constants.EXTRA_KEY_BAKER_IS_FAVORITE, baker.isFavorite());
        return bakerIntent;
    }

    public static double calculateCircularDistanceInKilometers(double latitude1, double longitude1, double latitude2, double longitude2) {
        double piRadius = Math.PI / 180.0;
        double phi1 = latitude1 * piRadius;
        double phi2 = latitude2 * piRadius;
        double lam1 = longitude1 * piRadius;
        double lam2 = longitude2 * piRadius;
        return 6371.01 * acos(sin(phi1) * sin(phi2) + cos(phi1) * cos(phi2) * cos(lam2 - lam1));
    }

    public static String getAppliedFilterParamValue(Context context) {
        String paramValue = ApiConstants.PARAM_RATING; // Default
        String[] sortEntries = context.getResources().getStringArray(R.array.array_filter_bakers);

        if (sortEntries[sFilterSelectedItemIndex].equalsIgnoreCase(context.getString(R.string.filter_bakers_dialog_top_rated))) {
            paramValue = ApiConstants.PARAM_RATING;
        }
        else if (sortEntries[sFilterSelectedItemIndex].equalsIgnoreCase(context.getString(R.string.filter_bakers_dialog_relevance))) {
            paramValue = ApiConstants.PARAM_RELEVANCE;
        }
//        else if (sortEntries[sFilterSelectedItemIndex].equalsIgnoreCase(context.getString(R.string.filter_bakers_dialog_nearest_first))) {
//            paramValue = ApiConstants.PARAM_NEAREST_FIRST;
//        }
        else if (sortEntries[sFilterSelectedItemIndex].equalsIgnoreCase(context.getString(R.string.filter_bakers_dialog_new_on_wishacake))) {
            paramValue = ApiConstants.PARAM_CREATED_AT;
        }
        return paramValue;
    }

    public static boolean isBakerFavoriteStatusUpdated(final Context context, final String favoriteId,
                                                       final String bakerName, final String favoriteStatus) {
        if (!Utils.isNetworkAvailable(context)) {
            Toast.makeText(context, context.getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
            return false;
        }

        final boolean[] flag = {false};
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                flag[0] = markFavoriteUnfavoriteBaker(context, favoriteId, bakerName, favoriteStatus);
                return null;
            }
        }.execute();
        return true;
    }

    private static boolean markFavoriteUnfavoriteBaker(final Context context, final String favoriteId, final String bakerName, final String favoriteStatus) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_FAVORITE_UNFAVORITE_BAKER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject rootJsonObject = new JSONObject(response);
                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                        String toastText = null;
                        if (favoriteStatus.equals(ApiConstants.PARAM_VALUE_FAVORITE)) {
                            toastText = bakerName + " has been added to your favorite bakers.";
                        } else if (favoriteStatus.equals(ApiConstants.PARAM_VALUE_UNFAVORITE)) {
                            toastText = bakerName + " has been removed from your favorite bakers.";
                        }
                        Toast.makeText(context, toastText, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, rootJsonObject.getString(ApiConstants.KEY_RESPONSE), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error != null) {
                    String toastMsg = "Sorry, something went wrong. Please try again.";
                    Toast.makeText(context, toastMsg, Toast.LENGTH_SHORT).show();
                    Log.e(LOG_TAG, error.toString());
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put(ApiConstants.PARAM_USER_ID, SharedPrefSingleton.getInstance(context).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID));
                params.put(ApiConstants.PARAM_FAVORITE_ID, favoriteId);
                params.put(ApiConstants.PARAM_FAVORITE_STATUS, favoriteStatus);
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        VolleyRequestHandlerSingleton.getInstance(context).addToRequestQueue(stringRequest);
        return true;
    }

    public static void showResendVerificationEmailDialog(final Activity activity, final String email, String msg) {
        View dialogView = LayoutInflater.from(activity).inflate(R.layout.custom_alert_dialog, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, R.style.AlertDialogTheme);
        builder.setView(dialogView);
        builder.setNegativeButton(activity.getString(R.string.cancel_alert_dialog_button), null);
        builder.setPositiveButton(activity.getString(R.string.resend_alert_dialog_button), null);

        TextView textViewTitle = (TextView) dialogView.findViewById(R.id.tv_title);
        textViewTitle.setVisibility(View.VISIBLE);
        textViewTitle.setText("Verify your account");

        TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
        textViewMsg.setTextColor(ContextCompat.getColor(activity, R.color.colorSecondaryText));
        textViewMsg.setText("To continue using " + activity.getString(R.string.app_name) + ", you'll need to verify your account. " + msg);

        final AlertDialog alertDialog = builder.create();
        alertDialog.setCancelable(true);

        alertDialog.show();

        /*
         *
         * Following code must be called after show() method of alert dialog
         */

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });


        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Utils.isNetworkAvailable(activity)) {
                    Toast.makeText(activity, activity.getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
                    // Utils.showCustomOkAlertDialog(LoginActivity.this, "No internet connection", getString(R.string.error_no_internet_connection));
                    return;
                }
                final ProgressDialog progressDialog = new ProgressDialog(activity, R.style.ProgressDialogTheme);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Requesting account verification email...");
                progressDialog.show();

                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                                ApiConstants.API_RESEND_VERIFICATION_EMAIL, new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                progressDialog.dismiss();
                                try {
                                    JSONObject rootJsonObject = new JSONObject(response);
                                    String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                                    if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                        alertDialog.dismiss();
                                        Utils.showCustomOkAlertDialog(activity, "",
                                                "We have sent an email to \"" + email + "\" containing instructions on how to verify your account.");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                                if (error != null) {
                                    String toastMsg = "Sorry, something went wrong. Please try again.";
                                    Toast.makeText(activity, toastMsg, Toast.LENGTH_SHORT).show();
                                    Log.e(LOG_TAG, error.toString());
                                }
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() {
                                Map<String, String> params = new HashMap<>();
                                params.put(ApiConstants.PARAM_EMAIL, email);
                                return params;
                            }
                        };

                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(10000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                        VolleyRequestHandlerSingleton.getInstance(activity).addToRequestQueue(stringRequest);
                        return null;
                    }
                }.execute();
            }
        });

        alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorSecondaryText));
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    public static String getFirebaseToken() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    sFirebaseToken = FirebaseInstanceId.getInstance().getToken(SENDER_ID, "FCM");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
        return sFirebaseToken;
    }

    public static String getOrderStatusByOrderStatusCode(String status) {
        String orderStatus = ApiConstants.PARAM_VALUE_PENDING;
        if (status.equals("1")) {
            orderStatus = ApiConstants.PARAM_VALUE_PENDING;
        } else if (status.equals("2")) {
            orderStatus = ApiConstants.PARAM_VALUE_ACCEPTED;
        } else if (status.equals("3")) {
            orderStatus = ApiConstants.PARAM_VALUE_REJECTED;
        } else if (status.equals("4")) {
            orderStatus = ApiConstants.PARAM_VALUE_CONFIRMED;
        } else if (status.equals("5")) {
            orderStatus = ApiConstants.PARAM_VALUE_CANCELLED;
        } else if (status.equals("6")) {
            orderStatus = ApiConstants.PARAM_VALUE_ONGOING;
        } else if (status.equals("7")) {
            orderStatus = ApiConstants.PARAM_VALUE_COMPLETED;
        }
        return orderStatus;
    }
}
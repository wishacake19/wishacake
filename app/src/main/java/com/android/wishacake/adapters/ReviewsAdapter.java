package com.android.wishacake.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.wishacake.R;
import com.android.wishacake.models.Review;

import java.util.ArrayList;
import java.util.List;

import me.zhanghai.android.materialratingbar.MaterialRatingBar;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ReviewsViewHolder> {

    private Context mContext;
    private List<Review> mReviews = new ArrayList<>();

    /*
     *
     * Constructor
     */

    public ReviewsAdapter(Context context, List<Review> reviews) {
        mContext = context;
        mReviews = reviews;
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public ReviewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_reviews_recyclerview_item, parent, false);
        return new ReviewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ReviewsViewHolder holder, final int position) {
        Review review = mReviews.get(position);
        holder.mTextViewReviewBy.setText(review.getReviewBy());
        holder.mTextViewReview.setText(review.getReview());
        holder.mMaterialRatingBarRating.setRating((float) review.getRating());
        holder.mTextViewReviewDate.setText("Posted on " + review.getDate());

        // Hides the divider from last item
        if (position == getItemCount() - 1) {
            holder.mViewDivider.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mReviews.size();
    }

    /*
     *
     * ViewHolder class
     */

    public class ReviewsViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextViewReviewBy, mTextViewReview, mTextViewReviewDate;
        private MaterialRatingBar mMaterialRatingBarRating;
        private View mViewDivider;

        public ReviewsViewHolder(View view) {
            super(view);
            mTextViewReviewBy = (TextView) view.findViewById(R.id.tv_review_by);
            mTextViewReview = (TextView) view.findViewById(R.id.tv_review);
            mTextViewReviewDate = (TextView) view.findViewById(R.id.tv_review_date);
            mMaterialRatingBarRating = (MaterialRatingBar) view.findViewById(R.id.mrb_review_rating);
            mViewDivider = (View) view.findViewById(R.id.view_divider);
        }
    }
}

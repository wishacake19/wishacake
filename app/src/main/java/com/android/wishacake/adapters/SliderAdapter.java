package com.android.wishacake.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.wishacake.R;
import com.android.wishacake.activities.FullscreenImageActivity;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.utilities.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SliderAdapter extends PagerAdapter {

    private Context mContext;
    private List<Integer> mImageResIds = new ArrayList<>();
    private List<String> mImageUrls;

    private final String LOG_TAG = SliderAdapter.class.getSimpleName();

    public SliderAdapter(Context context, List<String> imageUrls, List<Integer> imageResIds) {
        mContext = context;
        mImageUrls = imageUrls;
        mImageResIds = imageResIds;
    }

    public SliderAdapter(Context context, List<String> imageUrls) {
        mContext = context;
        mImageUrls = imageUrls;
    }

    @Override
    public int getCount() {
        if (mImageUrls.size() > 0) {
            return mImageUrls.size();
        } else {
            return mImageResIds.size();
        }
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup parent, final int position) {
        View view;
        if (Globals.sIsSliderFromMainActivity) {
            view = LayoutInflater.from(mContext).inflate(R.layout.custom_slider_item, parent, false);
            ImageView imageViewSlider = (ImageView) view.findViewById(R.id.img_slider);
            Picasso.get().load(mImageResIds.get(position)).fit().placeholder(R.color.colorDivider).into(imageViewSlider);
//            isSliderImageLoadedOrNotNull(mImageUrls.get(position), imageViewSlider);
        } else {
            view = LayoutInflater.from(mContext).inflate(R.layout.custom_slider_item_baker_profile, parent, false);
            ImageView imageViewSlider = (ImageView) view.findViewById(R.id.img_slider_baker_profile);
            try {
                if (isSliderImageLoadedOrNotNull(mImageUrls.get(position), imageViewSlider)) {
                    imageViewSlider.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(mContext, FullscreenImageActivity.class);
                            intent.putExtra(Constants.EXTRA_KEY_FULLSCREEN_IMAGE, mImageUrls.get(position));
                            mContext.startActivity(intent);
                        }
                    });
                }
            } catch (Exception e) {
            }
        }

        ViewPager viewPager = (ViewPager) parent;
        viewPager.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup parent, int position, @NonNull Object object) {
        ViewPager viewPager = (ViewPager) parent;
        View view = (View) object;
        viewPager.removeView(view);
    }

    /*
     *
     * Helper methods
     */

    private boolean isSliderImageLoadedOrNotNull(String url, ImageView imageViewSlider) {
        if (!Utils.isStringEmptyOrNull(url)) {
            Picasso.get().load(url).fit().placeholder(R.color.colorDivider).into(imageViewSlider);
            return true;
        } else {
            Picasso.get().load(R.drawable.ic_no_image_available).fit().placeholder(R.color.colorDivider).into(imageViewSlider);
            return false;
        }
    }
}

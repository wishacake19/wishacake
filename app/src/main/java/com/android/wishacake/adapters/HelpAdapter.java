package com.android.wishacake.adapters;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.wishacake.R;
import com.android.wishacake.activities.HelpActivity;
import com.android.wishacake.helpers.HelpItem;
import com.android.wishacake.utilities.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.HelpViewHolder> {

    private Context mContext;
    private List<HelpItem> mHelpItems = new ArrayList<>();

    private final int UPLOAD_LIMIT = 3;

    /*
     *
     * Constructor
     */

    public HelpAdapter(Context context, List<HelpItem> helpItems) {
        mContext = context;
        mHelpItems = helpItems;
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public HelpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_help_recyclerview_item, parent, false);
        return new HelpViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HelpViewHolder holder, final int position) {
        // Disable the option to upload more images if count reaches 3
        if (HelpActivity.sHelpImageCount == this.UPLOAD_LIMIT) {
            Utils.enableDisableUploadImageOptionInHelp(mContext, R.drawable.contained_normal_button_upload_image_disabled,
                    false);
        }

        final HelpItem helpItem = mHelpItems.get(position);

        Picasso.get().load(helpItem.getImageUri()).fit().placeholder(R.color.colorDivider).into(holder.mImageViewHelpImage);
        holder.mTextViewFileName.setText(helpItem.getFileName());

        holder.mImageViewClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHelpItems.size() > 0) {
                    mHelpItems.remove(position);
                    notifyItemRemoved(position);
                    notifyItemRangeChanged(position, mHelpItems.size());

                    HelpActivity.sHelpImageCount--;
                }

                // Enable the option to upload more images if count is != 3
                if (HelpActivity.sHelpImageCount != UPLOAD_LIMIT) {
                    Utils.enableDisableUploadImageOptionInHelp(mContext, R.drawable.contained_normal_button_upload_image_enabled,
                            true);
                }

                // Hides the extra 8dp bottom space
                if (HelpActivity.sHelpImageCount == 0) {
                    HelpActivity.sRecyclerViewHelpItems.setVisibility(View.GONE);
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mHelpItems.size();
    }

    /*
     *
     * Helper methods
     */

    public HelpItem getItem(int position) {
        return mHelpItems.get(position);
    }

    /*
     *
     * ViewHolder class
     */

    public class HelpViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageViewHelpImage, mImageViewClear;
        private TextView mTextViewFileName;

        public HelpViewHolder(View view) {
            super(view);
            mImageViewHelpImage = (ImageView) view.findViewById(R.id.img_help);
            mTextViewFileName = (TextView) view.findViewById(R.id.tv_file_name);
            mImageViewClear = (ImageView) view.findViewById(R.id.img_clear);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mImageViewHelpImage.setClipToOutline(true);
            }
        }
    }
}

package com.android.wishacake.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.wishacake.R;
import com.android.wishacake.activities.PreviewImagesActivity;
import com.android.wishacake.activities.SignUpActivity;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.Globals;
import com.android.wishacake.models.FeaturedCake;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class FeaturedCakesAdapter extends RecyclerView.Adapter<FeaturedCakesAdapter.FeaturedCakesViewHolder> {

    private Context mContext;
    private List<FeaturedCake> mFeaturedCakes = new ArrayList<>();
    private OnItemClickListener mItemClickListener;

    /*
     *
     * Constructor
     */

    public FeaturedCakesAdapter(Context context, List<FeaturedCake> featuredCakes) {
        mContext = context;
        mFeaturedCakes = featuredCakes;
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public FeaturedCakesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_featured_cakes_recyclerview_item, parent, false);
        return new FeaturedCakesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FeaturedCakesViewHolder holder, final int position) {
        final FeaturedCake featuredCake = mFeaturedCakes.get(position);
        if (featuredCake.getImageUrls() != null && featuredCake.getImageUrls().get(0) != null) {
            Picasso.get().load(featuredCake.getImageUrls().get(0)).fit().placeholder(R.color.colorDivider).into(holder.mImageViewCake);
        } else {
            Picasso.get().load(R.drawable.ic_no_image_available).fit().placeholder(R.color.colorDivider).into(holder.mImageViewCake);
        }
        holder.mTextViewTitle.setText(featuredCake.getTitle());
        holder.mTextViewDescription.setText(featuredCake.getDescription());

        holder.mTextViewOrderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Globals.sIsGuestUserLoggedIn) {
                    Globals.sIsSignUpOpenedFromLogin = false;
                    Intent intent = new Intent(mContext, SignUpActivity.class);
                    intent.putExtra(Constants.EXTRA_KEY_IS_SIGN_UP_FOR_GUEST_USER, true);
                    mContext.startActivity(intent);
                    return;
                }

                Intent intent = new Intent(mContext, PreviewImagesActivity.class);
                intent.putExtra(Constants.EXTRA_KEY_TITLE_PREVIEW_IMAGES_ACTIVITY, mContext.getString(R.string.activity_title_preview_images));
                intent.putExtra(Constants.EXTRA_KEY_IS_PREVIEW_IMAGES_OPEN_FROM_UNITY, false);
                intent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, (ArrayList<String>) featuredCake.getImageUrls());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFeaturedCakes.size();
    }

    /*
     *
     * Setters
     */

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    /*
     *
     * Helper methods
     */

    public FeaturedCake getItem(int position) {
        return mFeaturedCakes.get(position);
    }

    /*
     *
     * ViewHolder class
     */

    public class FeaturedCakesViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageViewCake;
        private TextView mTextViewTitle, mTextViewDescription, mTextViewOrderNow;

        public FeaturedCakesViewHolder(View view) {
            super(view);
            mImageViewCake = (ImageView) view.findViewById(R.id.img_cake);
            mTextViewTitle = (TextView) view.findViewById(R.id.tv_cake_title);
            mTextViewDescription = (TextView) view.findViewById(R.id.tv_cake_desc);
            mTextViewOrderNow = (TextView) view.findViewById(R.id.tv_order_now);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(v, getAdapterPosition());
                    }
                }
            });
        }
    }

    /*
     *
     * Item click interface
     */

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}

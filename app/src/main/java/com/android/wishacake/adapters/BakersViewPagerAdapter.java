package com.android.wishacake.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.android.wishacake.fragments.FavoriteBakersFragment;
import com.android.wishacake.fragments.NearbyBakersFragment;
import com.android.wishacake.fragments.RecentBakersFragment;

public class BakersViewPagerAdapter extends FragmentPagerAdapter {

    public BakersViewPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new FavoriteBakersFragment();
        } else if (position == 1) {
            fragment = new RecentBakersFragment();
        } else if (position == 2) {
            fragment = new NearbyBakersFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }
}

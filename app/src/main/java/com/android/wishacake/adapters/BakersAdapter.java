package com.android.wishacake.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.wishacake.R;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.models.Baker;
import com.android.wishacake.utilities.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class BakersAdapter extends RecyclerView.Adapter<BakersAdapter.BakerViewHolder> {

    private Context mContext;
    private boolean mIsFromFavoriteBakersFragment;
    private List<Baker> mBakers = new ArrayList<>();
    private OnItemClickListener mItemClickListener;

    private final String LOG_TAG = BakersAdapter.class.getSimpleName();

    /*
     *
     * Constructor
     */

    public BakersAdapter(Context context, List<Baker> bakers, boolean isFromFavoriteBakersFragment) {
        mContext = context;
        mBakers = bakers;
        mIsFromFavoriteBakersFragment = isFromFavoriteBakersFragment;
    }

    /*
     *
     * Overridden methods
     */

    @Override
    public BakerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_bakers_recyclerview_item, parent, false);
        return new BakerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BakerViewHolder holder, final int position) {
        final Baker baker = mBakers.get(position);
        try {
            if (baker.getImageUrl() != null) {
                if (baker.getImageUrl().trim().length() != 0) {
                    Log.v(LOG_TAG, baker.getImageUrl());
                    Picasso.get().load(baker.getImageUrl()).fit().placeholder(R.drawable.ic_default_profile_pic).into(holder.mCircleImageViewBakerImg, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.v(LOG_TAG, "onSuccess() called of Picasso");
                        }

                        @Override
                        public void onError(Exception e) {
                            if (e != null) {
                                Log.e(LOG_TAG, "onError() called of Picasso, Exception: " + e.getMessage());
                            } else {
                                Log.e(LOG_TAG, "onError() called of Picasso");
                            }
                        }
                    });
                } else {
                    Picasso.get().load(R.drawable.ic_default_profile_pic)
                            .placeholder(R.drawable.ic_default_profile_pic).into(holder.mCircleImageViewBakerImg);
                }
            } else {
                Picasso.get().load(R.drawable.ic_default_profile_pic)
                        .placeholder(R.drawable.ic_default_profile_pic).into(holder.mCircleImageViewBakerImg);
            }
        } catch (Exception e) {
            if (e != null) {
                e.printStackTrace();
            }
        }
        if (baker.getActiveStatus().equals("1") || baker.getActiveStatus().equalsIgnoreCase("Online")) {
            holder.mImageViewOnline.setVisibility(View.VISIBLE);
        }
        holder.mTextViewName.setText(baker.getFirstName() + " " + baker.getLastName());
        holder.mTextViewAddress.setText(baker.getAddress());
        if (baker.getRating() <= 0) {
            holder.mTextViewRating.setText("N/A");
        } else {
            holder.mTextViewRating.setText(baker.getRating() + "");
        }
        holder.mTextViewDistance.setText(baker.getDistance() + " KM away");

        if (baker.isFavorite()) {
            holder.mImageViewFav.setImageResource(R.drawable.ic_favorite_filled);
        } else {
            baker.setFavorite(false);
            holder.mImageViewFav.setImageResource(R.drawable.ic_favorite_unfilled);
        }

        hideDividerForLastItem(holder, position);

//        if (baker.getImageUrl() != null) {
//            holder.mCircleImageViewBakerImg.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    View dialogView = LayoutInflater.from(mContext).inflate(R.layout.custom_image_popup_dialog, null);
//                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.ImagePopupDialogTheme);
//                    builder.setView(dialogView);
//
//                    TextView textViewBakerName = (TextView) dialogView.findViewById(R.id.tv_baker_name);
//                    textViewBakerName.setText(baker.getFirstName() + " " + baker.getLastName());
//
//                    ImageView imageViewBaker = (ImageView) dialogView.findViewById(R.id.img_baker);
//                    Picasso.get().load(Uri.parse(baker.getImageUrl())).fit().placeholder(R.drawable.ic_default_profile_pic).into(imageViewBaker);
//
//                    final AlertDialog alertDialog = builder.create();
//                    alertDialog.setCancelable(true);
//                    alertDialog.show();
//
//                    RelativeLayout relativeLayoutImgPopupDialog = (RelativeLayout) dialogView.findViewById(R.id.relative_layout_img_popup_dialog);
//                    relativeLayoutImgPopupDialog.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            Intent intent = new Intent(mContext, FullscreenImageActivity.class);
//                            intent.putExtra(Constants.KEY_BAKER_NAME, baker.getFirstName() + " " + baker.getLastName());
//                            intent.putExtra(Constants.KEY_BAKER_IMAGE, baker.getImageUrl());
//                            mContext.startActivity(intent);
//                            alertDialog.dismiss();
//                        }
//                    });
//                }
//            });
//        }

        holder.mImageViewFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!baker.isFavorite()) {
                    if (Utils.isBakerFavoriteStatusUpdated(mContext, baker.getFavoriteId(), baker.getFirstName() + " " + baker.getLastName(), ApiConstants.PARAM_VALUE_FAVORITE)) {
                        baker.setFavorite(true);
                        holder.mImageViewFav.setImageResource(R.drawable.ic_favorite_filled);
                    }
                } else {
                    View dialogView = LayoutInflater.from(mContext).inflate(R.layout.custom_alert_dialog, null);
                    final AlertDialog.Builder builder = new AlertDialog.Builder(mContext, R.style.AlertDialogTheme);
                    builder.setView(dialogView);
                    builder.setNegativeButton(mContext.getString(R.string.cancel_alert_dialog_button), null);
                    builder.setPositiveButton(mContext.getString(R.string.bakers_remove_alert_dialog_button), null);

                    TextView textViewMsg = (TextView) dialogView.findViewById(R.id.tv_msg);
                    textViewMsg.setText("Are you sure you want to remove \"" + baker.getFirstName() + " " + baker.getLastName() + "\" from your favorite bakers?");

                    final AlertDialog alertDialog = builder.create();
                    alertDialog.setCancelable(true);

                    alertDialog.show();

                    /*
                     *
                     * Following code must be called after show() method of alert dialog
                     */

                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Utils.isBakerFavoriteStatusUpdated(mContext, baker.getFavoriteId(), baker.getFirstName() + " " + baker.getLastName(), ApiConstants.PARAM_VALUE_UNFAVORITE)) {
                                alertDialog.dismiss();
                                baker.setFavorite(false);
                                holder.mImageViewFav.setImageResource(R.drawable.ic_favorite_unfilled);
                                if (mIsFromFavoriteBakersFragment) {
                                    mBakers.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, getItemCount());
                                    if (position == getItemCount()) {
                                        holder.mViewDivider.setVisibility(View.INVISIBLE); // Gone kia tw heart top pe move hojaega
                                        notifyDataSetChanged();
                                    }
                                }
                            }
                        }
                    });

                    alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(mContext, R.color.colorSecondaryText));
                    alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBakers.size();
    }

    /*
     *
     * Setters
     */

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    /*
     *
     * Helper methods
     */

    public Baker getItem(int position) {
        return mBakers.get(position);
    }

    private void hideDividerForLastItem(BakerViewHolder holder, int position) {
        // Hides the divider from last item
        if (position == getItemCount() - 1) {
            holder.mViewDivider.setVisibility(View.INVISIBLE); // Gone kia tw heart top pe move hojaega
        }
    }

    private void refreshAllFragments() {
//        BakersViewPagerAdapter bakersViewPagerAdapter = new BakersViewPagerAdapter(((FragmentActivity) mContext).getSupportFragmentManager());
//        sViewPagerBakers.setAdapter(bakersViewPagerAdapter);
//        sViewPagerBakers.setOffscreenPageLimit(3);
//        sViewPagerBakers.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(sTabLayoutBakers));
//        sTabLayoutBakers.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(sViewPagerBakers));
    }

    /*
     *
     * ViewHolder class
     */

    public class BakerViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView mCircleImageViewBakerImg;
        private TextView mTextViewName, mTextViewAddress, mTextViewRating, mTextViewDistance;
        private ImageView mImageViewFav, mImageViewOnline;
        private View mViewDivider;

        public BakerViewHolder(View view) {
            super(view);
            mCircleImageViewBakerImg = (CircleImageView) view.findViewById(R.id.civ_baker_image);
            mTextViewName = (TextView) view.findViewById(R.id.tv_baker_name);
            mTextViewAddress = (TextView) view.findViewById(R.id.tv_baker_address);
            mTextViewRating = (TextView) view.findViewById(R.id.tv_baker_rating);
            mImageViewFav = (ImageView) view.findViewById(R.id.img_fav);
            mImageViewOnline = (ImageView) view.findViewById(R.id.img_online);
            mTextViewDistance = (TextView) view.findViewById(R.id.tv_baker_distance);
            mViewDivider = (View) view.findViewById(R.id.view_divider);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(v, getAdapterPosition());
                    }
                }
            });
        }
    }

    /*
     *
     * Item click interface
     */

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}

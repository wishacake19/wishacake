package com.android.wishacake.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.wishacake.R;
import com.android.wishacake.activities.MainActivity;
import com.android.wishacake.activities.PreviewImagesActivity;
import com.android.wishacake.helpers.ApiConstants;
import com.android.wishacake.helpers.Constants;
import com.android.wishacake.helpers.SharedPrefSingleton;
import com.android.wishacake.helpers.VolleyRequestHandlerSingleton;
import com.android.wishacake.models.Order;
import com.android.wishacake.utilities.DateTimeUtils;
import com.android.wishacake.utilities.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YourOrdersAdapter extends RecyclerView.Adapter<YourOrdersAdapter.YourOrdersViewHolder> {

    private Context mContext;
    private Activity mActivity;
    private List<Order> mOrders = new ArrayList<>();
    private OnItemClickListener mItemClickListener;
    private ProgressDialog mProgressDialog;

    private static final String LOG_TAG = YourOrdersAdapter.class.getSimpleName();

    /*
     *
     * Constructor
     */

    public YourOrdersAdapter(Context context, Activity activity, List<Order> orders) {
        mContext = context;
        mActivity = activity;
        mOrders = orders;
        mProgressDialog = new ProgressDialog(context, R.style.ProgressDialogTheme);
        mProgressDialog.setCancelable(false);
    }

    /*
     *
     * Overridden methods
     */

    @NonNull
    @Override
    public YourOrdersViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_your_orders_recyclerview_item, parent, false);
        return new YourOrdersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull YourOrdersViewHolder holder, int position) {
        final Order order = mOrders.get(position);

        String formattedDateStr = getFormattedDateString(order);
        holder.mTextViewOrderDate.setText(formattedDateStr);
        holder.mTextViewOrderAmount.setText("PKR " + String.format("%.0f", order.getTotalAmount()) + "");
        holder.mTextViewOrderBakerName.setText(order.getBaker().getFirstName() + " " + order.getBaker().getLastName());

        String status = order.getStatus();

        if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_PENDING)) {
            holder.mLinearLayoutButtonReOrder.setVisibility(View.GONE);
            if (DateTimeUtils.getMinutesDifference(order.getDate()) <= 60) {
                holder.mLinearLayoutButtonsCancelConfirm.setVisibility(View.VISIBLE);
            }
            holder.mFrameLayoutButtonConfirm.setVisibility(View.GONE);
            holder.mTextViewOrderAmount.setVisibility(View.GONE);
            Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_PENDING,
                    R.color.colorOrderStatusPending, R.drawable.order_status_pending_drawable);
        } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_REJECTED)) {
            holder.mLinearLayoutButtonsCancelConfirm.setVisibility(View.GONE);
            holder.mLinearLayoutButtonReOrder.setVisibility(View.VISIBLE);
            holder.mTextViewOrderAmount.setText("PKR 0");
            holder.mTextViewOrderAmount.setTextColor(ContextCompat.getColor(mContext, R.color.colorOrderStatusRejected));
            Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_REJECTED,
                    R.color.colorOrderStatusRejected, R.drawable.order_status_rejected_drawable);
        } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ACCEPTED)) {
            holder.mLinearLayoutButtonReOrder.setVisibility(View.GONE);
            holder.mLinearLayoutButtonsCancelConfirm.setVisibility(View.VISIBLE);
            Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_ACCEPTED,
                    R.color.colorOrderStatusAccepted, R.drawable.order_status_accepted_drawable);
        } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CANCELLED)) {
            holder.mLinearLayoutButtonsCancelConfirm.setVisibility(View.GONE);
            holder.mLinearLayoutButtonReOrder.setVisibility(View.VISIBLE);
            Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_CANCELLED,
                    R.color.colorOrderStatusCancelled, R.drawable.order_status_cancelled_drawable);
        } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_CONFIRMED)) {
            holder.mLinearLayoutButtonsCancelConfirm.setVisibility(View.GONE);
            holder.mLinearLayoutButtonReOrder.setVisibility(View.GONE);
            Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_CONFIRMED,
                    R.color.colorOrderStatusConfirmed, R.drawable.order_status_confirmed_drawable);
        } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_ONGOING)) {
            holder.mLinearLayoutButtonsCancelConfirm.setVisibility(View.GONE);
            holder.mLinearLayoutButtonReOrder.setVisibility(View.VISIBLE);
            Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_ONGOING,
                    R.color.colorOrderStatusOngoing, R.drawable.order_status_ongoing_drawable);
        } else if (status.equalsIgnoreCase(ApiConstants.PARAM_VALUE_COMPLETED)) {
            holder.mLinearLayoutButtonsCancelConfirm.setVisibility(View.GONE);
            holder.mLinearLayoutButtonReOrder.setVisibility(View.VISIBLE);
            Utils.setUpOrderStatus(mContext, holder.mTextViewOrderStatus, ApiConstants.PARAM_VALUE_COMPLETED,
                    R.color.colorOrderStatusCompleted, R.drawable.order_status_completed_drawable);
        }

        // Hides the divider from last item
        if (position == getItemCount() - 1) {
            holder.mViewDivider.setVisibility(View.GONE);
        }

        holder.mButtonReOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, PreviewImagesActivity.class);
                intent.putExtra(Constants.EXTRA_KEY_TITLE_PREVIEW_IMAGES_ACTIVITY, mContext.getString(R.string.activity_title_preview_images));
                intent.putExtra(Constants.EXTRA_KEY_IS_PREVIEW_IMAGES_OPEN_FROM_UNITY, false);
                intent.putStringArrayListExtra(Constants.EXTRA_KEY_ORDER_IMAGES, (ArrayList<String>) order.getImageUrls());
                mContext.startActivity(intent);
            }
        });

        holder.mButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateOrderStatus(order, ApiConstants.PARAM_VALUE_CANCELLED);
            }
        });

        holder.mButtonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateOrderStatus(order, ApiConstants.PARAM_VALUE_CONFIRMED);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mOrders.size();
    }

    /*
     *
     * Setters
     */

    public void setItemClickListener(OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    /*
     *
     * Helper methods
     */

    @NonNull
    private String getFormattedDateString(Order order) {
        String formattedDateStr = "";
        if (DateTimeUtils.isToday(order.getDate())) {
            formattedDateStr = "Today";
        } else if (DateTimeUtils.isYesterday(order.getDate())) {
            formattedDateStr = "Yesterday";
        } else {
            formattedDateStr = new SimpleDateFormat("dd MMM, yyyy").format(order.getDate());
        }
        formattedDateStr += " • " + new SimpleDateFormat("hh:mm a").format(order.getDate());
        return formattedDateStr;
    }

    public Order getItem(int position) {
        return mOrders.get(position);
    }

    private void updateOrderStatus(final Order order, final String statusAction) {
        if (!Utils.isNetworkAvailable(mContext)) {
            Toast.makeText(mContext, mContext.getString(R.string.error_no_internet_connection), Toast.LENGTH_SHORT).show();
            // Utils.showCustomOkAlertDialog(mContext, "No internet connection", getString(R.string.error_no_internet_connection));
            return;
        }
        mProgressDialog.setMessage("Processing...");
        mProgressDialog.show();

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiConstants.API_UPDATE_ORDER_STATUS, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgressDialog.dismiss();
                        try {
                            JSONObject rootJsonObject = new JSONObject(response);
                            String responseMsg = rootJsonObject.getString(ApiConstants.KEY_RESPONSE);
                            String status = rootJsonObject.getString(ApiConstants.KEY_STATUS);

                            if (status.equalsIgnoreCase(ApiConstants.STATUS_SUCCESS)) {
                                String toastMsg = "[" + getFormattedDateString(order) + "] ";
                                if (statusAction.equals(ApiConstants.PARAM_VALUE_CONFIRMED)) {
                                    toastMsg += "Order confirmed successfully.";
                                }
                                else if (statusAction.equals(ApiConstants.PARAM_VALUE_CANCELLED)) {
                                    toastMsg += "Order cancelled successfully.";
                                }
                                Toast.makeText(mContext, toastMsg, Toast.LENGTH_SHORT).show();
                                ActivityCompat.finishAffinity(mActivity);
                                mContext.startActivity(new Intent(mContext, MainActivity.class));
                            }
                            else if (status.equalsIgnoreCase(ApiConstants.STATUS_ERROR)) {
                                Toast.makeText(mContext, responseMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgressDialog.dismiss();
                        if (error != null) {
                            String toastMsg = "Sorry, something went wrong. Please try again.";
                            Toast.makeText(mContext, toastMsg, Toast.LENGTH_SHORT).show();
                            Log.e(LOG_TAG, error.toString());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<>();
                        params.put(ApiConstants.PARAM_ORDER_ID, order.getId());
                        params.put(ApiConstants.PARAM_USER_ID, SharedPrefSingleton.getInstance(mContext).getLoggedInUserData().get(Constants.SHARED_PREF_KEY_USER_ID));
                        params.put(ApiConstants.PARAM_BAKER_ID, order.getBaker().getId());
                        params.put(ApiConstants.PARAM_SUBTOTAL, String.format("%.0f", order.getSubTotal()));
                        params.put(ApiConstants.PARAM_DELIVERY_CHARGES, String.format("%.0f", order.getDeliveryCharges()));
                        params.put(ApiConstants.PARAM_TOTAL_AMOUNT, String.format("%.0f", order.getTotalAmount()));
                        params.put(ApiConstants.PARAM_REJECTED_REASON, "null");
                        params.put(ApiConstants.PARAM_STATUS, statusAction);
                        return params;
                    }
                };

                stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                VolleyRequestHandlerSingleton.getInstance(mContext).addToRequestQueue(stringRequest);
                return null;
            }
        }.execute();
    }

    /*
     *
     * ViewHolder class
     */

    public class YourOrdersViewHolder extends RecyclerView.ViewHolder {

        private TextView mTextViewOrderDate, mTextViewOrderAmount, mTextViewOrderBakerName, mTextViewOrderStatus;
        private LinearLayout mLinearLayoutButtonReOrder, mLinearLayoutButtonsCancelConfirm;
        private Button mButtonReOrder, mButtonCancel, mButtonConfirm;
        private FrameLayout mFrameLayoutButtonConfirm;
        private View mViewDivider;

        public YourOrdersViewHolder(View view) {
            super(view);
            mTextViewOrderDate = (TextView) view.findViewById(R.id.tv_order_date);
            mTextViewOrderAmount = (TextView) view.findViewById(R.id.tv_order_amount);
            mTextViewOrderBakerName = (TextView) view.findViewById(R.id.tv_order_baker_name);
            mTextViewOrderStatus = (TextView) view.findViewById(R.id.tv_order_status);
            mViewDivider = (View) view.findViewById(R.id.view_divider);
            mLinearLayoutButtonReOrder = (LinearLayout) view.findViewById(R.id.linear_layout_button_re_order);
            mLinearLayoutButtonsCancelConfirm = (LinearLayout) view.findViewById(R.id.linear_layout_buttons_cancel_confirm);
            mButtonReOrder = (Button) view.findViewById(R.id.btn_re_order);
            mButtonCancel = (Button) view.findViewById(R.id.btn_cancel);
            mButtonConfirm = (Button) view.findViewById(R.id.btn_confirm);
            mFrameLayoutButtonConfirm = (FrameLayout) view.findViewById(R.id.frame_layout_btn_confirm);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(v, getAdapterPosition());
                    }
                }
            });
        }
    }

    /*
     *
     * Item click interface
     */

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }
}

package com.android.wishacake.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.wishacake.R;
import com.android.wishacake.activities.FullscreenImageActivity;
import com.android.wishacake.helpers.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CakeImageAdapter extends RecyclerView.Adapter<CakeImageAdapter.CakeImageViewHolder> {

    private Context mContext;
    private List<String> mImages = new ArrayList<>();

    /*
     *
     * Constructor
     */

    public CakeImageAdapter(Context context, List<String> images) {
        mContext = context;
        mImages = images;
    }

    /*
     *
     * Overridden methods
     */

    @NonNull
    @Override
    public CakeImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_cake_image_recyclerview_item, parent, false);
        return new CakeImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CakeImageViewHolder holder, final int position) {
        final String image = mImages.get(position);
        if (image != null) {
            Picasso.get().load(image).fit().placeholder(R.color.colorDivider).into(holder.mImageViewCake);
            holder.mImageViewCake.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, FullscreenImageActivity.class);
                    intent.putExtra(Constants.EXTRA_KEY_FULLSCREEN_IMAGE, image);
                    mContext.startActivity(intent);
                }
            });
        } else {
            Picasso.get().load(R.drawable.ic_no_image_available).fit().placeholder(R.color.colorDivider).into(holder.mImageViewCake);
        }
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    /*
     *
     * ViewHolder class
     */

    public class CakeImageViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageViewCake;

        public CakeImageViewHolder(View view) {
            super(view);
            mImageViewCake = (ImageView) view.findViewById(R.id.img_cake);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mImageViewCake.setClipToOutline(true);
            }
        }
    }
}

package com.android.wishacake.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.android.wishacake.R;
import com.android.wishacake.activities.PreviewImagesActivity;
import com.android.wishacake.helpers.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PreviewImagesAdapter extends RecyclerView.Adapter<PreviewImagesAdapter.PreviewImagesViewHolder> {

    private int mIndex = 0;
    private Context mContext;
    private List<String> mImages = new ArrayList<>();

    /*
     *
     * Constructor
     */

    public PreviewImagesAdapter(Context context) {
        mContext = context;
    }

    /*
     *
     * Overridden methods
     */

    @NonNull
    @Override
    public PreviewImagesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_preview_image_recyclerview_item, parent, false);
        return new PreviewImagesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final PreviewImagesViewHolder holder, final int position) {
        final String image = mImages.get(position);
        if (image != null) {
            setCurrentImageSelected(holder, position, image);
            Picasso.get().load(image).fit().placeholder(R.color.colorDivider).into(holder.mImageViewPhoto);

            holder.mImageViewPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setCurrentImageSelected(holder, position, image);
                    mIndex = position;
                    notifyDataSetChanged();
                }
            });

            holder.mImageButtonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        removeItem(position);
                        enableDisableAddFabButton();
                        enableDisableContinueButton();
                        mIndex = position;
                        if (position == getItemCount() && getItemCount() == 0) {
                            PreviewImagesActivity.sPhotoDraweeViewImage.setPhotoUri(Uri.parse(""));
                        } else if (position == getItemCount() && getItemCount() != 0) {
                            mIndex = mIndex - 1;
                            int newPosition = position - 1;
                            setCurrentImageSelected(holder, newPosition, mImages.get(newPosition));
                        } else {
                            setCurrentImageSelected(holder, position, image);
                        }
                    } catch (Exception e) {
                        if (e != null) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        } else {
            Picasso.get().load(R.drawable.ic_no_image_available).fit().placeholder(R.color.colorDivider).into(holder.mImageViewPhoto);
        }

        enableDisableAddFabButton();
        enableDisableContinueButton();
    }

    @Override
    public int getItemCount() {
        return mImages.size();
    }

    /*
     *
     * Setters
     */

    public void setImages(List<String> images) {
        mImages = images;
        notifyDataSetChanged();
    }

    /*
     *
     * Helper methods
     */

    private void setCurrentImageSelected(@NonNull PreviewImagesViewHolder holder, int position, String image) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            if (mIndex == position) {
                holder.mImageViewPhoto.setPadding(3,3,3,3);
                holder.mImageViewPhoto.setBackground(ContextCompat.getDrawable(mContext, R.drawable.selected_preview_image_border));
                PreviewImagesActivity.sPhotoDraweeViewImage.setPhotoUri(Uri.parse(image));
            } else {
                holder.mImageViewPhoto.setPadding(0,0,0,0);
                holder.mImageViewPhoto.setBackground(ContextCompat.getDrawable(mContext, R.drawable.unselected_preview_image_border));
            }
        }
    }

    private void enableDisableContinueButton() {
        if (getItemCount() == 0) {
            PreviewImagesActivity.sButtonContinue.setEnabled(false);
            PreviewImagesActivity.sFrameLayoutContinue.setBackgroundColor(
                    ContextCompat.getColor(mContext, R.color.colorPrimaryLight));
        } else {
            PreviewImagesActivity.sButtonContinue.setEnabled(true);
            PreviewImagesActivity.sFrameLayoutContinue.setBackgroundColor(
                    ContextCompat.getColor(mContext, R.color.colorPrimary));
        }
    }

    private void enableDisableAddFabButton() {
        if (getItemCount() >= Constants.ORDER_IMAGES_SIZE) {
            PreviewImagesActivity.sFabAdd.setEnabled(false);
            PreviewImagesActivity.sFabAdd.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.colorPrimaryLight)));
        } else {
            PreviewImagesActivity.sFabAdd.setEnabled(true);
            PreviewImagesActivity.sFabAdd.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.colorPrimary)));
        }
    }

    private void removeItem(int position) {
        mImages.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, getItemCount());
    }

    public List<String> getImages() {
        return mImages;
    }

    /*
     *
     * ViewHolder class
     */

    public class PreviewImagesViewHolder extends RecyclerView.ViewHolder {

        private ImageView mImageViewPhoto;
        private ImageButton mImageButtonDelete;

        public PreviewImagesViewHolder(View view) {
            super(view);
            mImageViewPhoto = (ImageView) view.findViewById(R.id.img_photo);
            mImageButtonDelete = (ImageButton) view.findViewById(R.id.img_btn_delete);
        }
    }
}
